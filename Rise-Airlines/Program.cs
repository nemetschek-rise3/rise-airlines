﻿using Airlines.Bussines;
using Airlines.Bussines.Mapper;
using Airlines.Bussines.Service;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Entities;
using Airlines.Persistance.Basic.Mapper;
using Airlines.Persistance.Basic.Models;
using RiseAirlines.Controllers;
using Airlines.Persistance.Basic.Models.Aircrafts;
using Airlines.Persistance.Basic.Repository;
using AutoMapper;
using RiseAirlines.Persistance.Basic.EntitiesService;
using Airlines.Bussines.Dto;
using Microsoft.EntityFrameworkCore;

namespace Airlines;
public class Program
{
    public static void Main(string[] args)
    {
        ArgumentNullException.ThrowIfNull(args);

        DB_Context dbContext = new DB_Context();
        AirportRepository airportRepo = new AirportRepository(dbContext);
        AirlineRepository airlineRepo = new AirlineRepository(dbContext);
        FlightRepository flightRepo = new FlightRepository(dbContext);
        IAirportMapper airportMapper = new AirportMapper();
        IAirlineMapper airlineMapper = new AirlineMapper();
        IFlightMapper flightMapper = new FlightMapper();
        AirportService airportService = new AirportService(airportRepo, airportMapper);
        AirlineService airlineService = new AirlineService(airlineRepo, airlineMapper);
        FlightService flightService = new FlightService(flightRepo, flightMapper);

        //HashSet<AirportModel> airports = AirportsService.CreateAirports("Airports.csv");
        //HashSet<AirlineModel> airlines = AirlineService.CreateAirlines("Airlines.csv");
        //HashSet<FlightModel> flights = FlightService.CreateFlights("Flights.csv");
        //AircraftDto aircraftDto = AircraftService.CreateAircrafts("Aircrafts.csv");
        //EntitiesDto entityDto = new EntitiesDto(airports, airlines, flights, aircraftDto);

        //ProgramExecuter.Start(entityDto);

        //IAirportMapper airportMapper = new AirportMapper();
       
        //List<AirportModel> JFKAirports = airportService.GetAirportFilter("Code", "JFK");
        //AirportModel airport1 = JFKAirports.FirstOrDefault()!;
        //Console.WriteLine("Found airport is:");
        //Console.WriteLine(airport1.Name);

        DateTime dateTime = DateTime.Now;
        DateOnly founded = new DateOnly(1967, 4, 29);

        //IFlightMapper flightMapper = new FlightMapper();
        //FlightModel flightDto = new FlightModel(1, "F1", 1, 2, "Boing747", 700, 7, dateTime.AddHours(1), dateTime.AddHours(8));
        //Flight flight = flightMapper.MapToFlight(flightDto);
        //Console.WriteLine(flight.Code);

        //List<AirportModel> airports = airportService.GetAirports();
        //foreach (AirportModel airport1 in airports)
        //{
        //    Console.WriteLine(airport1.Id + " " + airport1.Name);
        //}
        IQueryable<Airline> deleteAirlines = dbContext.Airlines.Where(airline => EF.Property<string>(airline, "Name").Equals("dasdasd"));
        foreach (Airline airline in deleteAirlines)
        {
            Console.WriteLine(airline.Name);
            dbContext.Airlines.Remove(airline);
            Console.WriteLine(airline.Name);
            //dbContext.SaveChanges();
        }
        //airlineService.DeleteAirlineByFilter("Name", "dasdasd");

        //Airport airport = flights.FirstOrDefault().DepartureAirport;
        //Console.WriteLine(airport.Id);   
    }
}