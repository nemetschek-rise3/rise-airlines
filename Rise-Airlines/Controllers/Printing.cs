﻿using Airlines.Bussines.DataStructures;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic;
using Airlines.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Models.Aircrafts;
using Airlines.Persistance.Basic.Models.Reservations;
using RiseAirlines.Persistance.Basic.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RiseAirlines.Controllers;
public static class Printing
{

    public static void PrintAirports(HashSet<AirportModel> airports)
    {
        Console.WriteLine("Names of airports are: ");
        foreach (AirportModel airport in airports)
        {

            Console.WriteLine(airport.Name);
        }
    }

    public static void PrintAirlines(HashSet<AirlineModel> airlines)
    {
        Console.WriteLine("Names of airlines are: ");
        foreach (AirlineModel airline in airlines)
            Console.WriteLine(airline.Name);
    }

    public static void PrintFlights(HashSet<FlightModel> flights)
    {
        Console.WriteLine("Names of flights are: ");
        foreach (FlightModel flight in flights)
            Console.Write($"{flight.Id} {flight.DepartureAirport} {flight.ArrivalAirport}\n");
    }

    public static void PrintRoute(RouteLinkedList route)
    {
        LinkedListNode current = route.head!;

        if (route.head == null)
            throw new InvalidCommandException("The route is empty");
        else
        {
            Console.WriteLine("Route airports are: ");
            while (current != null)
            {
                Console.WriteLine($"{current.Flight!.DepartureAirport} - {current.Flight.ArrivalAirport}");
                current = current.Next!;
            }
        }
    }
    public static void PrintAircrafts(AircraftDto aircraftDto)
    {
        HashSet<CargoAircraft> cargoAircrafts = aircraftDto.CargoAircrafts!;
        HashSet<PassengerAircraft> passengerAircrafts = aircraftDto.PassengerAircrafts!;
        foreach (var cargoAircraft in cargoAircrafts!)
            Console.WriteLine(cargoAircraft.Model);
        foreach (var passengerAircraft in passengerAircrafts)
            Console.WriteLine(passengerAircraft.Model);
    }

    public static void PrintReservations(List<Reservation> reservations)
    {

        foreach (Reservation reservation in reservations)
            Console.WriteLine(reservation.GetInfo());
    }

    public static void PrintOutput(HashSet<AirportModel> airports, HashSet<AirlineModel> airlines, HashSet<FlightModel> flights)
    {
        PrintAirports(airports);
        PrintAirlines(airlines);
        PrintFlights(flights);
    }

    public static void PrintEntities(string[] inputs, EntitiesDto dto, RouteLinkedList route, List<Reservation> reservations)
    {
        if (inputs[1].Equals("airports"))
            PrintAirports(dto.Airports);
        else if (inputs[1].Equals("airlines"))
            PrintAirlines(dto.Airlines);
        else if (inputs[1].Equals("flights"))
            PrintFlights(dto.Flights);
        else if (inputs[1].Equals("aircrafts"))
            PrintAircrafts(dto.AircraftDto!);
        else if (inputs[1].Equals("reservations"))
            PrintReservations(reservations);
        else if (inputs[1].Equals("route"))
            PrintRoute(route);
        else
            throw new InvalidCommandException("Invalid input, enter only airports, airlines, flights, aircrafts!");
    }

    public static void PrintHelpCommands(List<string> commands)
    {
        foreach (string command in commands)
            Console.WriteLine(command);
    }
    public static void PrintTreeRoute(List<int> route, EntitiesDto entities)
    {
        HashSet<AirportModel> airports = entities.Airports;
        int count = 1;
        Console.Write("Found route is: ");
        foreach (int airportId in route)
        {
            AirportModel airport = Search.AirportIdSearch(airportId, airports);

            if (count < route.Count)
            {
                Console.Write($"{airport.Code} - ");
                count++;
            }
            else
            {
                Console.Write($"{airport.Code}");
            }

            Console.WriteLine();
        }
    }
}
