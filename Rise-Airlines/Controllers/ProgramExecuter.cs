﻿using Airlines.Bussines.DataStructures;
using Airlines.Commands;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Models.Reservations;
using RiseAirlines.Commands;
using RiseAirlines.Persistance.Basic.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Controllers;
public class ProgramExecuter
{
    public static void Start(EntitiesDto dto)
    {
        bool continuing = true;
        RouteLinkedList route = new RouteLinkedList();
        List<Reservation> reservations = [];
        List<TreeNode> roots = new List<TreeNode>();
        while (continuing)
        {
            Console.WriteLine("list a command (help)");
            string inputLine = Console.ReadLine()!;
            
            string[] inputs = inputLine.Split(' ');

            try
            {
                switch (inputs[0])
                {
                    case "close":
                        continuing = false;
                        break;

                    case "batch":
                        BatchCommand batchCommand = BatchCommand.CreateBatchCommand(inputs, dto, route, reservations);
                        batchCommand.Execute();
                        break;

                    case "help":

                        HelpCommand helpCommand = HelpCommand.CreateHelpCommand(inputs);
                        helpCommand.Execute();
                        break;

                    case "list":
                        ListCommand listCommand = ListCommand.CreateListCommand(inputs, dto);
                        listCommand.Execute();
                        break;

                    case "exist":

                        ExistCommand existCommand = ExistCommand.CreateExistCommand(inputs, dto);
                        existCommand.Execute();
                        break;

                    case "sort":
                        SortCommand sortCommand = SortCommand.CreateSortCommand(inputs, dto);
                        sortCommand.Execute();
                        break;

                    case "route":
                        RouteCommand routeCommand = RouteCommand.CreateRouteCommand(inputs, route, dto);
                        routeCommand.Execute();
                        break;

                    case "print":

                        PrintCommand printCommand = PrintCommand.CreatePrintCommand(inputs, dto, route, reservations);
                        printCommand.Execute();
                        break;

                    case "reserve":
                        ReserveCommand reserveCommand = ReserveCommand.CreateReserveCommand(inputs, dto, reservations);
                        reserveCommand.Execute();
                        break;

                    default:
                        throw new InvalidCommandException("Invalid command");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }


    public static Command FindCommand(string inputLine, EntitiesDto dto, RouteLinkedList route, List<Reservation> reservations)
    {
        string[] inputs = inputLine.Split(' ');

        switch (inputs[0])
        {
            case "help":
                    
                HelpCommand helpCommand = HelpCommand.CreateHelpCommand(inputs);
                return helpCommand;

            case "list":
                ListCommand listCommand = ListCommand.CreateListCommand(inputs, dto);
                return listCommand;

            case "exist":

                ExistCommand existCommand = ExistCommand.CreateExistCommand(inputs, dto);
                return existCommand;

            case "sort":
                SortCommand sortCommand = SortCommand.CreateSortCommand(inputs, dto);
                return sortCommand;

            case "route":
                RouteCommand routeCommand = RouteCommand.CreateRouteCommand(inputs, route, dto);
                return routeCommand;

            case "print":

                PrintCommand printCommand = PrintCommand.CreatePrintCommand(inputs, dto, route, reservations);
                return printCommand;

            case "reserve":
                ReserveCommand reserveCommand = ReserveCommand.CreateReserveCommand(inputs, dto, reservations);
                return reserveCommand;

            default:
                Console.WriteLine("Invalid command");
                Command emptyCommand = Command.CreateCommand();
                return emptyCommand;
        }
    }
}
