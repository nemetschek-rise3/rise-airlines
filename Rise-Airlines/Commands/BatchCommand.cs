﻿using Airlines.Bussines.BussinesValidation;
using Airlines.Bussines.DataStructures;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Models.Reservations;
using RiseAirlines.Commands;
using RiseAirlines.Controllers;
using RiseAirlines.Persistance.Basic.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Commands;
public class BatchCommand : Command
{

    public EntitiesDto Entities { get; set; }
    public RouteLinkedList Route { get; set; }
    public List<Reservation> Reservations { get; set; }
    private BatchCommand(string[] commands, EntitiesDto entities, RouteLinkedList route, List<Reservation> reservations) : base(commands)
    {
        Entities = entities;
        Route = route;
        Reservations = reservations;

    }

    public static BatchCommand CreateBatchCommand(string[] commands, EntitiesDto entities, RouteLinkedList route, List<Reservation> reservations)
    {
        return new BatchCommand(commands, entities, route, reservations);
    }


    public override void Execute()
    {
        CommandValidation validation = new CommandValidation();
        validation.ValidateBatchCommand(Commands!);

        string inputLine;
        Queue<Command> batchCommands = new Queue<Command>();

        string batchCommand;
        while (true)
        {
            Console.WriteLine("Batch mode: list a command to add to the batch: (help)");
            inputLine = Console.ReadLine()!;
            if (inputLine.Equals("batch run") || inputLine.Equals("batch close"))
            {
                batchCommand = inputLine;
                break;
            }
            if (inputLine.Equals("help"))
            {
                HelpCommand.HelpReader("DefaultHelpCommands.csv");
            }
            else
            {
                Command currentCommand = ProgramExecuter.FindCommand(inputLine, Entities, Route, Reservations);

                if (currentCommand.Commands is not null)
                {
                    batchCommands.Enqueue(currentCommand);
                }
            }
        }
        if (batchCommand.Equals("batch run"))
        {
            foreach (Command command in batchCommands)
            {
                command.Execute();

            }
        }
    }
}
