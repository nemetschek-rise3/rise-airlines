﻿using Airlines.Bussines.BussinesValidation;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic;
using Airlines.Persistance.Basic.Models;
using RiseAirlines.Persistance.Basic.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Commands;
public class ExistCommand : Command
{
    private ExistCommand(string[] commands, EntitiesDto entitiesDto) : base(commands)
    {
        EntitiesDto = entitiesDto;
    }

    public EntitiesDto EntitiesDto { get; set; }

    public static ExistCommand CreateExistCommand(string[] inputs, EntitiesDto entitiesDto)
    {
        return new ExistCommand(inputs, entitiesDto);
    }

    public override void Execute()
    {
        CommandValidation validation = new CommandValidation();
        validation.ValidateExistCommand(Commands!, EntitiesDto);

        Console.WriteLine(Search.AirlineSearch(Commands![1], EntitiesDto.Airlines));
    }
}
