﻿using Airlines.Bussines.BussinesValidation;
using Airlines.Commands;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Models.Reservations;
using RiseAirlines.Persistance.Basic.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Commands;

public class ReserveCommand : Command
{
    public List<Reservation> Reservations { get; set; }
    public EntitiesDto Entities { get; set; }

    private ReserveCommand(string[] commands, EntitiesDto entities, List<Reservation> reservations) : base(commands)
    {
        Entities = entities;
        Reservations = reservations;
    }

    public static ReserveCommand CreateReserveCommand(string[] inputs, EntitiesDto entities, List<Reservation> reservations)
    {
        return new ReserveCommand(inputs, entities, reservations);
    }


    public override void Execute()
    {
        CommandValidation validation = new CommandValidation();
        validation.ValidateReserveTicketCommand(Commands!, Reservations);

        CommandValidation commandValidation = new CommandValidation();
        if (Commands![1].Equals("ticket"))
        {
            commandValidation.ValidatePassengerTicketCommand(Commands, Entities.Flights);

            Reservation reservation = PassengerReservation.CreatePassengerReservation(Commands[2], int.Parse(Commands[3]), int.Parse(Commands[4]), int.Parse(Commands[5]));
            Reservations.Add(reservation);
            Console.WriteLine(reservation.GetInfo());
        }

        else if (Commands[1].Equals("cargo"))
        {
            commandValidation.ValidateCargoTicketCommand(Commands, Entities.Flights);

            CargoReservation reservation = new CargoReservation(Commands[2], int.Parse(Commands[3]), int.Parse(Commands[4]));
            Reservations.Add(reservation);
            Console.WriteLine("You successfully created a reservation!");
            Console.WriteLine(reservation.GetInfo());
        }
        else
        {
            throw new InvalidCommandException("Invalid input - you can only reserve ticket or cargo!");
        }
    }
}