﻿using Airlines.Bussines.BussinesValidation;
using Airlines.Bussines.DataStructures;
using Airlines.Commands;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Models.Reservations;
using RiseAirlines.Controllers;
using RiseAirlines.Persistance.Basic.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Commands;
public class PrintCommand : Command
{
    public EntitiesDto Entities { get; set; }
    public RouteLinkedList Route { get; set; }
    public List<Reservation> Reservations { get; set; }

    private PrintCommand(string[] commands, EntitiesDto entities, RouteLinkedList route, List<Reservation> reservations) : base(commands)
    {
        Entities = entities;
        Route = route;
        Reservations = reservations;
    }

    public static PrintCommand CreatePrintCommand(string[] inputs, EntitiesDto entities, RouteLinkedList route, List<Reservation> reservations)
    {
        return new PrintCommand(inputs, entities, route, reservations);
    }


    public override void Execute()
    {
        CommandValidation validation = new CommandValidation();
        validation.ValidatePrintCommand(Commands!);

        Printing.PrintEntities(Commands!, Entities, Route, Reservations);
    }
}
