﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Commands
{
    public interface ICommand
    {

        void Execute();
    }
}
