﻿using Airlines.Bussines.BussinesValidation;
using Airlines.Commands;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic;
using Airlines.Persistance.Basic.Models;
using RiseAirlines.Commands;
using RiseAirlines.Persistance.Basic.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Commands;
public class ListCommand : Command
{
    private ListCommand(string[] commands, EntitiesDto entitiesDto) : base(commands)
    {
        EntitiesDto = entitiesDto;
    }

    public EntitiesDto? EntitiesDto { get; set; }

    public static ListCommand CreateListCommand(string[] inputs, EntitiesDto entitiesDto)
    {
        return new ListCommand(inputs, entitiesDto);
    }

    public override void Execute()
    {
        CommandValidation validation = new CommandValidation();
        validation.ValidateListCommand(Commands!, EntitiesDto!);

        if (Commands![1].Equals("city"))
        {
            IEnumerable<AirportModel> cities = Search.AirportCitySearch(Commands[2], EntitiesDto!.Airports);
            foreach (AirportModel c in cities)
            {
                Console.WriteLine(c.Name);
            }
        }
        else
        {
            IEnumerable<AirportModel> countries = Search.AirportCountrySearch(Commands[2], EntitiesDto!.Airports);
            foreach (AirportModel c in countries)
            {
                Console.WriteLine(c.Name);
            }
        }
    }
}
