﻿using Airlines.Bussines.DataStructures;
using Airlines.Bussines.StrategyAlgorithms;
using Airlines.Commands;
using Airlines.Persistance.Basic;
using Airlines.old.Persistance.Basic.Models;
using RiseAirlines.Controllers;
using RiseAirlines.Persistance.Basic.EntitiesService;
using RiseAirlines.Persistance.Basic.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Airlines.Bussines.BussinesValidation;

namespace Airlines.Commands;
public class RouteCommand : Command
{
    public const string TIME = "time";
    public const string CHEAP = "cheap";
    public const string STOP = "stop";

    public RouteLinkedList Route { get; set; }
    public EntitiesDto Entities { get; set; }


    public RouteCommand(string[] commands, RouteLinkedList route, EntitiesDto entities) : base(commands)
    {
        Route = route;
        Entities = entities;

    }

    public static RouteCommand CreateRouteCommand(string[] commands, RouteLinkedList route, EntitiesDto entities)
    {
        return new RouteCommand(commands, route, entities);
    }


    public override void Execute()
    {
        CommandValidation validation = new CommandValidation();

        switch (Commands![1])
        {
            case "new":

                validation.ValidateNewRouteCommand(Commands);
                if (Commands.Length == 2)
                {
                    Console.WriteLine("You created a new route! Select from the commands below!");
                    Route.Created = true;
                    Route.Clear();
                }
                break;

            case "add":

                validation.ValidateAddRouteCommand(Commands,Route);
                Route.AddFlight(Search.FlightSearch(Commands[2], Entities.Flights));
                Console.WriteLine($"You successfully added {Commands[2]} to the route!");

                break;

            case "remove":
               
                validation.ValidateRemoveRouteCommand(Commands,Route);
                Route.RemoveFlight();
                Console.WriteLine("You successfully removed last flight from the route!");
                break;

            case "find":

                validation.ValidateFindRouteCommand(Commands);
                string rootAirport = Commands[2];
                switch (rootAirport)
                {
                    case "CPT":
                        FindAndPrintRoute(".\\Routes\\CPTRoute.csv");
                        break;
                    case "DXB":
                        FindAndPrintRoute(".\\Routes\\DXBRoute.csv");
                        break;
                    case "HND":
                        FindAndPrintRoute(".\\Routes\\HNDRoute.csv");
                        break;
                    case "LAX":
                        FindAndPrintRoute(".\\Routes\\LAXRoute.csv");
                        break;
                    case "LHR":
                        FindAndPrintRoute(".\\Routes\\LHRRoute.csv");
                        break;
                    case "SYD":
                        FindAndPrintRoute(".\\Routes\\SYDRoute.csv");
                        break;
                    default:
                        throw new InvalidCommandException("There is no such root airport!");
                }
                break;

            case "check":
                  
                validation.ValidateCheckRouteCommand(Commands);
                GraphAirport graph = new GraphAirport();
                HashSet<FlightModel> flights = FlightManager.CreateFlights("PassengerGraphFlights.csv");
                graph.LoadGraph(flights);
                bool isConnected = graph.IsConnected(int.Parse(Commands[2]), int.Parse(Commands[3]));
                Console.WriteLine(isConnected);
                break;

            case "search":

                validation.ValidateSearchRouteCommand(Commands);
                FindAndPrintGraphRoute("PassengerGraphFlights.csv");
                break;

            default:
                throw new InvalidCommandException("Invalid route command!");
        }
    }
    public List<int> FindRoute(string path, int destinationAirport)
    {

        ArrayList treeFlights = FlightManager.ReadFlightsFromCsv(path, Entities.Flights);
        FlightRouteTree flightRouteTree = new FlightRouteTree();
        TreeNode root = flightRouteTree.BuildTreeFromCSV(treeFlights);
        TreeNode destinationNode = flightRouteTree.DFS(root, destinationAirport);
        List<int> route = flightRouteTree.FindDestinationRoute(destinationNode, root);
        return route;
    }

    public void FindAndPrintRoute(string path)
    {
        List<int> route = FindRoute(path, int.Parse(Commands![3]));
        Printing.PrintTreeRoute(route, Entities);
    }

    public void FindAndPrintGraphRoute(string path)
    {
        HashSet<FlightModel> flights = FlightManager.CreateFlights(path);
        GraphAirport graph = new GraphAirport();
        graph.LoadGraph(flights);
        Algorithm algorithm = FindAlgorithm(Commands![4], int.Parse(Commands[2]), int.Parse(Commands[3]));
        List<int> nearestPath = graph.FindNearestPath(int.Parse(Commands![2]), int.Parse(Commands[3]), algorithm);
        int counter = 1;
        foreach (int vertex in nearestPath)
        {
            if (nearestPath.Count > counter)
            {

                Console.Write($"{vertex} - ");
                counter++;
            }
            else
            {
                Console.Write(vertex);
            }
        }
        Console.WriteLine();
    }

    public static Algorithm FindAlgorithm(string algorithm, int startAirport, int destinationAirport)
    {
        if (algorithm.Equals(TIME))
        {
            return new TimeAlgorithm(startAirport, destinationAirport);
        }
        else if (algorithm.Equals(CHEAP))
        {
            return new CheapAlgorithm(startAirport, destinationAirport);
        }
        else if (algorithm.Equals(STOP))
        {
            return new StopAlgorithm(startAirport, destinationAirport);
        }
        else
        {
            throw new InvalidCommandException("There is no such strategy - available are (cheap, time, stop)");
        }
    }
}