﻿using Airlines.Bussines.Sort;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Models;
using RiseAirlines.Persistance.Basic.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Commands;
public class SortCommand : Command
{
    private SortCommand(string[] commands, EntitiesDto entitiesDto) : base(commands)
    {
        EntitiesDto = entitiesDto;
    }

    public EntitiesDto EntitiesDto { get; set; }

    public static SortCommand CreateSortCommand(string[] inputs, EntitiesDto entitiesDto)
    {
        return new SortCommand(inputs, entitiesDto);
    }

    public override void Execute()
    {
        try
        {
            if (Commands![1].Equals("airports"))
            {
                if (Commands[2].Equals("id"))
                {
                    Console.WriteLine("How do you want to sort airports - ascending or descending?");
                    string answer = Console.ReadLine()!;
                    EntitiesDto.Airports = Sort.SortingById(EntitiesDto.Airports, answer);
                }
                else if (Commands[2].Equals("name"))
                {
                    Console.WriteLine("How do you want to sort airports - ascending or descending?");
                    string answer = Console.ReadLine()!;
                    EntitiesDto.Airports = Sort.SortingByName(EntitiesDto.Airports, answer);
                }
                else
                {
                    throw new InvalidCommandException("Invalid input, airports can be sorted only by id or name!");
                }
            }
            else if (Commands[1].Equals("airlines"))
            {
                if (Commands[2].Equals("name"))
                {
                    Console.WriteLine("How do you want to sort airlines - ascending or descending?");
                    string answer = Console.ReadLine()!;
                    EntitiesDto.Airlines = Sort.SortingByName(EntitiesDto.Airlines, answer);
                }
                else
                {
                    throw new InvalidCommandException("Invalid input, airlines can be sorted only by name!");
                }
            }
            else if (Commands[2].Equals("flights"))
            {
                if (Commands[2].Equals("id"))
                {
                    Console.WriteLine("How do you want to sort flights - ascending or descending?");
                    string answer = Console.ReadLine()!;
                    EntitiesDto.Flights = Sort.SortingById(EntitiesDto.Flights, answer);
                }
                else
                {
                    throw new InvalidCommandException("Invalid input, flights can be sorted only by id!");
                }
            }
            else
            {
                throw new InvalidCommandException("Invalid input, only airports, airlines or flights can be sorted!");
            }
        }
        catch (InvalidCommandException ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
}
