﻿using Airlines.Bussines.Interfaces;
using BussinesLogic.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RiseAirlines.Entities;
public class Flight : IdItem
{
    public Flight() { }
    public Flight(string id, string departureAirport, string arrivalAirport, string aircraftModel)
    {
        this.Id = id;
        this.ArrivalAirport = arrivalAirport;
        this.DepartureAirport = departureAirport;
        this.AircraftModel = aircraftModel;
    }


    private string? id;
    private string? arrivalAirport;
    private string? departureAirport;
    private string? aircraft;

    public string Id
    {
        get { return id!; }
        set
        {
            if (!Validation.ValidateFlight(value))
            {
                Console.WriteLine("Error: Flight Id must contain only letters or digits! The flight is not going to be saved!");
            }
            else
            {
                id = value;
            }
        }
    }
    public string? DepartureAirport
    {
        get { return departureAirport; }
        set
        {
            if (!Validation.ValidateAirport(value!))
            {
                Console.WriteLine("Error: Departure Airport must contain only 3 letters! The flight is not going to be saved!");
            }
            else
            {
                departureAirport = value!;
            }
        }
    }
    public string? ArrivalAirport
    {
        get { return arrivalAirport; }
        set
        {
            if (!Validation.ValidateAirport(value!))
            {
                Console.WriteLine("Error: Airport Airport must contain only 3 letters! The flight is not going to be saved!");
            }
            else
            {
                arrivalAirport = value!;
            }
        }
    }

    public string AircraftModel
    {
        get => aircraft!;
        set
        {
            aircraft = value;
        }
    }

    public override bool Equals(object? obj)
    {
        if (obj == null || this.GetType() != obj.GetType())
        {
            return false;
        }
        Flight flight = (Flight)obj;
        return this.Id!.Equals(flight.Id) && this.ArrivalAirport!.Equals(flight.ArrivalAirport) && this.DepartureAirport!.Equals(flight.DepartureAirport);
    }
    public override int GetHashCode()
    {
        return HashCode.Combine(Id, DepartureAirport, ArrivalAirport);
    }
}
