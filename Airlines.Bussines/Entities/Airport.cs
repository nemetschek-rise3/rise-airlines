﻿using Airlines.Bussines.Interfaces;
using BussinesLogic.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Entities;
public class Airport : INameItem, IdItem
{
    public Airport() { }
    public Airport(string id, string name, string city, string country)
    {
         this.Id = id;
        this.Name = name;
        this.City = city;
        this.Country = country;
    }
    private string? id;
    private string? name;
    private string? city;
    private string? country;

    public string Id
    {
        get { return id!; }
        set
        {
            if (!Validation.ValidateAirport(value))
            {
                Console.WriteLine("Error: Id must have only 3 letters. The airport is not going to be saved!");
            }
            else
            {
                id = value;
            }
        }
    }

    public string? Name
    {
        get{ return name; }
        set
        {
            if (!Validation.HasOnlyLetters(value!)){
                Console.WriteLine($"Error: Name must contain only letters! {value}. The airport is not going to be saved!");
            }
            else
            {
                name = value!;
            }

        }
    }
    public string? City {
        get { return city; }
        set
        {
            if (!Validation.HasOnlyLetters(value!))
            {
                Console.WriteLine($"Error: Name must contain only letters! {value}. The airport is not going to be saved!");
            }
            else
            {
                city = value!;
            }

        }
    }
    public string? Country {
        get { return country; }
        set
        {
            if (!Validation.HasOnlyLetters(value!))
            {
                Console.WriteLine($"Error: Name must contain only letters! {value}. The airport is not going to be saved!");
            }
            else
            {
                country = value!;
            }

        }
    }


    public override bool Equals(object? obj)
    {
        if (obj == null || this.GetType() != obj.GetType())
        {
            return false;
        }
        Airport airport = (Airport)obj;
        return this.Name!.Equals(airport.Name) && this.Id.Equals(airport.Id) && this.City!.Equals(airport.City) && this.Country!.Equals(airport.Country);
    }
    public override int GetHashCode()
    {
        return HashCode.Combine(Id, Name, City, Country);
    }
}
