﻿using Airlines.Bussines.Aircrafts;
using RiseAirlines.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.Entities
{
    public class EntitiesDto
    {
        public EntitiesDto(HashSet<Airport> airports, HashSet<Airline> airlines, HashSet<Flight> flights, AircraftDto aircraftDto) 
        {
            this.Airports = airports;
            this.Airlines = airlines;
            this.Flights = flights;
            this.AircraftDto = aircraftDto;
        }

        private EntitiesDto(HashSet<Airport> airports, HashSet<Airline> airlines, HashSet<Flight> flights)
        {
            this.Airports = airports;
            this.Airlines = airlines;
            this.Flights = flights;
        }

        public AircraftDto? AircraftDto { get; set; }
        public HashSet<Airport> Airports {  get; set; }
        public HashSet<Airline> Airlines { get; set; }
        public HashSet<Flight> Flights {  get; set; }

    }
}
