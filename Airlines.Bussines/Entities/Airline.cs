﻿using Airlines.Bussines.Interfaces;
using BussinesLogic.Validation;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Entities;
public class Airline : INameItem, IdItem
{
    public Airline() { }
    public Airline(string id, string name)
    {
        this.Id = id;
        this.Name = name;
    }
    private string? id;
    private string? name;
    public string Id {
        get { return id!; }
        set
        {
            if (!Validation.ValidateAirline(value!))
            {
                Console.WriteLine($"Error: Name must contain only letters! {value}. The airline is not going to be saved!");
            }
            else
            {
                id = value;
            }
        }
    }
    public string? Name
    {
        get { return name; }
        set
        {
            if (!Validation.HasOnlyLetters(value!))
            {
                Console.WriteLine($"Error: Name must contain only letters! {value}. The airline is not going to be saved!");
            }
            else
            {
                name = value!;
            }

        }
    }

    public override bool Equals(object? obj)
    {
        if (obj == null || this.GetType() != obj.GetType())
        {
            return false;
        }
        Airline airline = (Airline)obj;
        return this.Name!.Equals(airline.Name) && this.Id.Equals(airline.Id);
    }
    public override int GetHashCode()
    {
        return HashCode.Combine(Id, Name);
    }
}
