﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Exceptions;
public class InvalidAirportException : Exception
{
    public InvalidAirportException(string message) : base(message) { }
}

