﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.Interfaces
{
    public interface INameItem
    {
        string? Name { get; }
    }
}
