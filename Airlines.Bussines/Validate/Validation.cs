﻿using BussinesLogic.SearchAndSort;
using RiseAirlines.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace BussinesLogic.Validation
{
    public class Validation
    {
        private const int _airport_length = 3;
        private const int _max_airline_length = 6;

        public static bool HasOnlyLetters(string name)
        {
            foreach (char c in name)
            {
                if (!char.IsLetter(c) && c != ' ')
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsAlphaNumeric(string name)
        {
            foreach (char c in name)
            {
                if (!char.IsLetterOrDigit(c))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsNumeric(string name)
        {
            foreach (char c in name)
            {
                if (!char.IsDigit(c))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool UniqueValidation(List<string> arr, string input)
        {
            foreach (string s in arr)
            {
                if (input == s)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool ValidateAirport(string name)
            => name.Length == _airport_length && HasOnlyLetters(name);


        public static bool ValidateAirline(string name)
            => name.Length < _max_airline_length;

        public static bool ValidateFlight(string name)
            => IsAlphaNumeric(name);

        public static bool ValidatePassengerTicket(string[] inputs, HashSet<Flight> flights)
        {
            if (Search.FlightSearch(inputs[2], flights) is null)
            {
                return false;
            }
            Flight flight = Search.FlightSearch(inputs[2], flights);

            if (IsNumeric(inputs[3]))
            {
                if (int.Parse(inputs[3]) > 10)
                {
                    Console.WriteLine("Maximum seat reservation is 10!");
                    return false;
                }
            }
            if (IsNumeric(inputs[4]))
            {
                if (int.Parse(inputs[4]) > 20)
                {
                    Console.WriteLine("Maximum small baggage reservation is 20!");
                    return false;
                }
            }
            if (IsNumeric(inputs[5]))
            {
                if (int.Parse(inputs[5]) > 10)
                {
                    Console.WriteLine("Maximum large baggage reservation is 10!");
                    return false;
                }
            }
            return true;
        }

        public static bool ValidateCargoTicket(string[] inputs, HashSet<Flight> flights)
        {

            if (Search.FlightSearch(inputs[2], flights) is null)
            {
                return false;
            }
            Flight flight = Search.FlightSearch(inputs[2], flights);

            if (IsNumeric(inputs[3]))
            {
                if (int.Parse(inputs[3]) > 5000)
                {
                    Console.WriteLine("Maximum cargo weight reservation is 5000!");
                    return false;
                }
            }
            if (IsNumeric(inputs[4]))
            {
                if (int.Parse(inputs[4]) > 5)
                {
                    Console.WriteLine("Maximum cargo volume reservation is 5 m^3!");
                    return false;
                }
            }
            return true;
        }
        public static bool ValidateInputLength(string[] inputs, int length)
        {
            if(inputs.Length < length)
            {
                return false;
            }
            return true;
        }
    }
}