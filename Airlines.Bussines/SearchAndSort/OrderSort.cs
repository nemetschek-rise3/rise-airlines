﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesLogic.SearchAndSort;
public enum OrderSort
{
    Ascending,
    Descending
}
