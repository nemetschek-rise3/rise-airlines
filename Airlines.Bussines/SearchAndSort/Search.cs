﻿
using RiseAirlines.Exceptions;
using RiseAirlines.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace BussinesLogic.SearchAndSort;
public class Search
{
    internal static IEnumerable<Airport> AirportCountrySearch(string searchName, HashSet<Airport> airports)
    {
        IEnumerable<Airport> correctCountriesAirports = airports.Where(airport => airport.Country == searchName);

        if (correctCountriesAirports.Any())
        {
            return correctCountriesAirports;
        }
        else
        {
            throw new InvalidAirportException("No airports found!");
        }

    }

    internal static IEnumerable<Airport> AirportCitySearch(string searchName, HashSet<Airport> airports)
    {
        IEnumerable<Airport> correctCitiesAirports = airports.Where(airport => airport.City == searchName);

        if (correctCitiesAirports.Any())
        {
            return correctCitiesAirports;
        }
        else
        {
            throw new InvalidAirportException("No airports found!");
        }
    }

    internal static bool AirportSearch(string searchName, HashSet<Airport> airports)
    {
        return (airports.Any(airport => airport.Id == searchName));
    }

    internal static bool AirlineSearch(string searchName, HashSet<Airline> airlines)
    {
        return airlines.Any(airline => airline.Name == searchName);
    }

    internal static Flight FlightSearch(string searchName, HashSet<Flight> flights)
    {
        IEnumerable<Flight> foundFlights = flights.Where(flight => flight.Id == searchName);

        if (foundFlights.Any()!)
        {
            return foundFlights.FirstOrDefault()!;
        }
        else
        {
            return null!;
        }
    }
}
