﻿using Airlines.Bussines.Entities;
using Airlines.Bussines.Interfaces;
using RiseAirlines.Entities;
using RiseAirlines.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BussinesLogic.SearchAndSort;
public class Sort
{
    private const string ASCENDING = "ascending";
    private const string DESCENDING = "descending";
    private const string SORTQUESTION = "How do you want to sort - ascending or descending?";

    internal static HashSet<T> SortingByName<T>(HashSet<T> hashSetT, string sortingMethode) where T : INameItem
    {

        if (sortingMethode.Equals(ASCENDING))
        {
            List<T> tList = new List<T>(hashSetT);
            BubbleSortByName(tList, OrderSort.Ascending);
            HashSet<T> newHashSetT = new HashSet<T>(tList);
            return newHashSetT;
        }
        else if (sortingMethode.Equals(DESCENDING))
        {
            List<T> tList = new List<T>(hashSetT);
            BubbleSortByName(tList, OrderSort.Descending);
            HashSet<T> newHashSetT = new HashSet<T>(tList);
            return newHashSetT;
        }
        else
        {
            throw new InvalidSortException("Please write valid answer - ascending or descending? ");
        }
    }

    internal static HashSet<T> SortingById<T>(HashSet<T> hashSetT, string sortingMethode) where T : IdItem
    {
        if (sortingMethode.Equals(ASCENDING))
        {
            List<T> tList = new List<T>(hashSetT);
            BubbleSortById(tList, OrderSort.Ascending);
            HashSet<T> newHashSetT = new HashSet<T>(tList);
            return newHashSetT;
        }
        else if (sortingMethode.Equals(DESCENDING))
        {
            List<T> tList = new List<T>(hashSetT);
            BubbleSortById(tList, OrderSort.Descending);

            HashSet<T> newHashSetT = new HashSet<T>(tList);
            return newHashSetT;
        }
        else
        {
            throw new InvalidSortException("Please write valid answer - ascending or descending? ");
        }

    }

    internal static void BubbleSortByName<T>(List<T> listT, OrderSort orderSort) where T : INameItem
    {
        bool isSwapped;
        do
        {
            isSwapped = false;
            for (int i = 0; i < listT.Count - 1; i++)
            {
                if (orderSort == OrderSort.Ascending)
                {
                    if (listT[i].Name!.CompareTo(listT[i + 1].Name) > 0)
                    {
                        (listT[i + 1], listT[i]) = (listT[i], listT[i + 1]);
                        isSwapped = true;

                    }
                }
                else
                {
                    if (listT[i].Name!.CompareTo(listT[i + 1].Name) < 0)
                    {
                        (listT[i + 1], listT[i]) = (listT[i], listT[i + 1]);
                        isSwapped = true;
                    }
                }
            }
        } while (isSwapped);
    }

    internal static void BubbleSortById<T>(List<T> listT, OrderSort orderSort) where T : IdItem
    {
        bool isSwapped;
        do
        {
            isSwapped = false;
            for (int i = 0; i < listT.Count - 1; i++)
            {
                if (orderSort == OrderSort.Ascending)
                {
                    if (listT[i].Id.CompareTo(listT[i + 1].Id) > 0)
                    {
                        (listT[i + 1], listT[i]) = (listT[i], listT[i + 1]);
                        isSwapped = true;

                    }
                }
                else
                {
                    if (listT[i].Id.CompareTo(listT[i + 1].Id) < 0)
                    {
                        (listT[i + 1], listT[i]) = (listT[i], listT[i + 1]);
                        isSwapped = true;
                    }
                }
            }
        } while (isSwapped);
    }
}
