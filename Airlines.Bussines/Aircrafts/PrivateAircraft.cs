﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.Aircrafts
{
    public class PrivateAircraft : Aircraft
    {

        public  int Seats { get; set; }


        public PrivateAircraft(string model, int seats)
            : base(model)
        {
            Seats = seats;
        }


        public override void DisplayInfo()
        {
            base.DisplayInfo();
            Console.WriteLine($"Seats: {Seats}");
        }
    }
}
