﻿using RiseAirlines.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.DataStructures
{
    public class Node
    {
        public Flight? Flight { get; set; }
        public Node? Next { get; set; }

        public Node(Flight flight)
        {
            Flight = flight;
            Next = null;
        }
    }
}
