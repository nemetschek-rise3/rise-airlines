﻿using RiseAirlines.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.DataStructures
{
    public class AirportNode
    {
        public string AirportId { get; set; }
        public List<AirportNode> NextNodes { get; set; }
        public AirportNode? Parent { get; set; }

        public AirportNode(string id)
        {
            AirportId = id;
            NextNodes = new List<AirportNode>();
        }

        public override bool Equals(object? obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            AirportNode other = (AirportNode)obj;

            if (AirportId != other.AirportId)
            {
                return false;
            }

            if (NextNodes.Count != other.NextNodes.Count)
            {
                return false;
            }

            for (int i = 0; i < NextNodes.Count; i++)
            {
                if (!NextNodes[i].Equals(other.NextNodes[i]))
                {
                    return false;
                }
            }

            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
