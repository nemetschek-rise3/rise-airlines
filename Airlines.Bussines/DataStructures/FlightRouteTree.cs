﻿using BussinesLogic.SearchAndSort;
using RiseAirlines.Entities;
using RiseAirlines.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Bussines.Interfaces;

namespace Airlines.Bussines.DataStructures
{
    public class FlightRouteTree
    {
        private AirportNode? Root { get; set; }

        public AirportNode BuildTreeFromCSV(ArrayList flights)
        {
            Dictionary<string, AirportNode> airportNodes = new Dictionary<string, AirportNode>();
            string rootAirport = flights[0]!.ToString()!;
            airportNodes[rootAirport] = new AirportNode(rootAirport);
            flights.RemoveAt(0);
            foreach (Flight flight in flights)
            {
                if (!airportNodes.ContainsKey(flight.DepartureAirport!))
                {
                    airportNodes[flight.DepartureAirport!] = new AirportNode(flight.DepartureAirport!);
                }

                AirportNode departureAirport = airportNodes[flight.DepartureAirport!];

                if (!airportNodes.ContainsKey(flight.ArrivalAirport!))
                {
                    airportNodes[flight.ArrivalAirport!] = new AirportNode(flight.ArrivalAirport!);
                }

                AirportNode arrivalAirport = airportNodes[flight.ArrivalAirport!];

                departureAirport.NextNodes.Add(arrivalAirport);
                arrivalAirport.Parent = departureAirport;
            }
            return airportNodes.Values.First();

        }


        public AirportNode DFS(AirportNode current, string destinationAirportName)
        {
            if (current.AirportId.Equals(destinationAirportName))
            {
                return current;
            }
            foreach (AirportNode next in current.NextNodes)
            {
                AirportNode result = DFS(next, destinationAirportName);
                if (result != null)
                {
                    return result;
                }
            }

            return null!;
        }
        public List<string> FindDestinationRoute(AirportNode destinationAirportNode, AirportNode rootAirportNode)
        {
            if (destinationAirportNode == null)
            {
                throw new InvalidRouteException("The destination airport is not found!");
            }
            List<string> path = new List<string>();
            do
            {
                path.Add(destinationAirportNode.AirportId);
                destinationAirportNode = destinationAirportNode.Parent!;
            }
            while (destinationAirportNode != null);

            path.Reverse();
            return path;
        }
    }
}









