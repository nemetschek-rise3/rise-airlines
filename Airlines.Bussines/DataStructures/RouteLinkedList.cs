﻿using RiseAirlines.Entities;
using RiseAirlines.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.DataStructures
{
    public class RouteLinkedList
    {
        public Node? head;
        public bool Created { get; set; } = false;

        public void AddFlight(Flight flight)
        {

            if (flight != null)
            {
                Node newNode = new Node(flight);
                if (head == null)
                {
                    head = newNode;
                }
                else
                {
                    Node current = head;
                    current = findLast(current);
                    if (current.Flight!.ArrivalAirport!.Equals(flight.DepartureAirport))
                    {
                        current.Next = newNode;
                    }
                    else
                    {

                        throw new InvalidRouteException($"Arrival airport of flight {current.Flight.Id} and departure airport of flight {flight.Id} don't match!");

                    }
                }
            }
            else
            {
                throw new InvalidRouteException("Flight is not found!");

            }
        }

        public void RemoveFlight()
        {
            if (head == null)
            {
                throw new InvalidRouteException("Route is empty");
            }
            else if (head.Next == null)
            {
                head = null;
            }
            else
            {
                Node current = head;
                current = findPreLast(current);
                current.Next = null;
            }
        }
        public void Clear()
        {
            head = null;
        }

        public bool IsEmpty()
        {
            return head == null;
        }

        public Node findPreLast(Node current)
        {
            while (current.Next?.Next != null)
            {
                current = current.Next;
            }
            return current;
        }

        public Node findLast(Node current)
        {
            while (current.Next != null)
            {
                current = current.Next;
            }
            return current;
        }
    }
}
