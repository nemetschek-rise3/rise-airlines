﻿using RiseAirlines.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Rise_Airlines.EntitiesService
{
    public interface IService<T>
    {
        public static bool AllFieldsHaveValue(object obj)
        {
            Type type = obj.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                object value = property.GetValue(obj)!;

                if (value is null)
                    return false;
            }

            return true;
        }


        public static bool CompareSets(HashSet<T> set1, HashSet<T> set2)
        {
            if (set1.Count != set2.Count)
                return false;
            int counter = 0;
            foreach (T obj in set1)
                if (ContainsSet(set2, obj))
                    counter++;
            return set1.Count == counter;
        }
        public static bool ContainsSet(HashSet<T> set1, T obj)
        {

            foreach (T obj1 in set1)
                if (obj1!.Equals(obj))
                    return true;
            return false;
        }
        public static bool CompareLists(List<T> list1, List<T> list2)
        {
            if (list1.Count != list2.Count)
                return false;
            int counter = 0;
            foreach (T obj in list1)
                if (ContainsList(list2, obj))
                    counter++;
            return list1.Count == counter;
        }
        public static bool ContainsList(List<T> list1, T obj)
        {

            foreach (T obj1 in list1)
                if (obj1!.Equals(obj))
                    return true;
            return false;
        }

        public static bool CompareArrayLists(ArrayList list1, ArrayList list2)
        {
            if (!(list1[0]!.Equals(list2[0])))
            {
                return false;
            }

            if (list1.Count != list2.Count)
            {
                return false;
            }

            for (int i = 1; i < list1.Count; i++)
            {
                if (!list1[i]!.Equals(list2[i]))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
