﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Bussines;
using BussinesLogic.Validation;
using RiseAirlines.Entities;
#pragma warning disable CA1822 // Mark members as static

namespace Rise_Airlines.EntitiesService;
public class AirportsService : IService<Airport>
{
    public static HashSet<Airport> CreateAirports(string path)
    {
        HashSet<Airport> airports = [];
        using (StreamReader reader = new StreamReader(path))
        {
            bool isHeader = true;
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine()!;
                string[] values = line.Split(';')!;

                if (!isHeader)
                {
                    Airport airport = new Airport(values[0], values[1], values[2], values[3]);
                    if (IService<Airport>.AllFieldsHaveValue(airport))
                        _ = airports.Add(airport);
                }
                isHeader = false;
            }
        }
        return airports;
    }
}
