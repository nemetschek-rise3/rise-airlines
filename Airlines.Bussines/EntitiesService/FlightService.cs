﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Airlines.Bussines;
using Airlines.Bussines.Entities;
using BussinesLogic.SearchAndSort;
using BussinesLogic.Validation;
using RiseAirlines.Entities;
using RiseAirlines.Exceptions;
#pragma warning disable CA1822 


namespace Rise_Airlines.EntitiesService;
public class FlightService : IService<Flight>
{

    public static HashSet<Flight> CreateFlights(string path)

    {
        HashSet<Flight> flights = [];
        using (var reader = new StreamReader(path))
        {
            var isHeader = true;
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine()!;
                var values = line.Split(';')!;

                if (!isHeader)
                {
                    Flight flight = new Flight(values[0], values[1], values[2], values[3]);
                    if (IService<Flight>.AllFieldsHaveValue(flight))

                        _ = flights.Add(flight);
                }
                isHeader = false;
            }
        }
        return flights;

    }

    public static ArrayList ReadFlightsFromCsv(string path, HashSet<Flight> flights)

    {
        ArrayList treeFlights = new ArrayList();
        string rootAirport;
        using (StreamReader reader = new StreamReader(path))
        {
            bool isHeader = true;
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine()!;
                if (isHeader != true)
                {
                    Flight flight = Search.FlightSearch(line, flights);
                    if (flight != null)
                        treeFlights.Add(flight);
                    else
                        throw new InvalidFlightException("FLights from tree not found!");
                    isHeader = false;
                }
                else
                {
                    rootAirport = line;
                    treeFlights.Add(rootAirport);
                    isHeader = false;
                }
            }
            return treeFlights;
        }
    }
}
