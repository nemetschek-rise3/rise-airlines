﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Bussines;
using BussinesLogic.Validation;
using RiseAirlines.Entities;
#pragma warning disable CA1822 // Mark members as static

namespace Rise_Airlines.EntitiesService;
public class AirlineService : IService<Airline>
{
    public static HashSet<Airline> CreateAirlines(string path)
    {
        HashSet<Airline> airlines = [];

        using (StreamReader reader = new StreamReader(path))
        {
            bool isHeader = true;
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine()!;
                string[] values = line.Split(';')!;

                if (!isHeader)
                {
                    Airline airline = new Airline(values[0], values[1]);
                    if (IService<Airline>.AllFieldsHaveValue(airline))
                        _ = airlines.Add(airline);
                }
                isHeader = false;
            }
        }

        return airlines;
    }
}
