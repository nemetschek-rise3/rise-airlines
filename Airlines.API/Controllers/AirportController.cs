﻿using Airlines.Bussines.Dto;
using Airlines.Bussines.Service;
using Airlines.Persistance.Basic.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AirportController : ControllerBase
    {
        private readonly IAirportService _airportService;

        public AirportController(IAirportService airportService)
        {
            _airportService = airportService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            List<AirportDto> airports = _airportService.GetAirports();
            if(airports == null)
            {
                return StatusCode(500, new {message = "Internal Server Error - airports is null"});
            }
            if(airports.Count == 0) 
            {
                return NotFound();
            }
            return Ok(airports);
        }

        [HttpGet("filter")]
        public IActionResult GetByFilter([FromBody] string filter, string value)
        {
            List<AirportDto> airports = _airportService.GetAirportFilter(filter, value);
            if (airports == null)
            {
                return StatusCode(500, new { message = "Internal Server Error - airports is null" });
            }
            if (airports.Count == 0)
            {
                return NotFound();
            }
            return Ok(airports);
        }

        [HttpPost]
        public IActionResult Add([FromBody] AirportDto airportDto)
        {
            bool result = _airportService.AddAirport(airportDto);
            if (airportDto == null)
            {
                return StatusCode(500, new { message = "Internal Server Error - airport is null" });
            }
            if(result == false)
            {
                return NotFound();
            }
            return Ok(airportDto);
        }

        [HttpDelete]
        public IActionResult DeleteByFilter([FromQuery] string filter, string value)
        {
            bool result = _airportService.DeleteAirportByFilter(filter, value);
            if (filter == null || value == null)
            {
                return StatusCode(500, new { message = "Internal Server Error - filter or value are null" });
            }
            if(result == false)
            {
                return NotFound();
            }
            return Ok();
        }

        [HttpPut("update")]
        public IActionResult Update([FromQuery] int id, [FromBody] AirportDto airportDto)
        {
            bool result = _airportService.UpdateAirport(id, airportDto);
            if (airportDto == null)
            {
                return StatusCode(500, new { message = "Internal Server Error - airport is null" });
            }
            if(result == false)
            {
                return NotFound();
            }
            return Ok(airportDto);
        }
    }
}

