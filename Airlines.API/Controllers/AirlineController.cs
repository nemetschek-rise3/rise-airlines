﻿using Airlines.Bussines.Dto;
using Airlines.Bussines.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AirlineController : ControllerBase
    {
        private readonly IAirlineService _airlineService;

        public AirlineController(IAirlineService airlineService)
        {
            _airlineService = airlineService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            List<AirlineDto> airlines = _airlineService.GetAirlines();
            if (airlines == null)
            {
                return StatusCode(500, new { message = "Internal Server Error - airports is null" });
            }
            if (airlines.Count == 0)
            {
                return NotFound();
            }
            return Ok(airlines);
        }

        [HttpGet("filter")]
        public IActionResult GetByFilter(string filter, string value)
        {
            List<AirlineDto> airlines = _airlineService.GetAirlineFilter(filter, value);
            if (airlines == null)
            {
                return StatusCode(500, new { message = "Internal Server Error - airports is null" });
            }
            if (airlines.Count == 0)
            {
                return NotFound();
            }
            return Ok(airlines);
        }

        [HttpPost]
        public IActionResult Add(AirlineDto airlineDto)
        {
            bool result = _airlineService.AddAirline(airlineDto);
            if (result == false)
            {
                return StatusCode(500, new { message = "Internal Server Error - airport is null" });
            }
            return Ok(airlineDto);
        }

        [HttpDelete]
        public IActionResult DeleteByFilter(string filter, string value)
        {
            bool result = _airlineService.DeleteAirlineByFilter(filter, value);
            if (result == false)
            {
                return NotFound();
            }
            if (filter == null || value == null)
            {
                return StatusCode(500, new { message = "Internal Server Error - airport is null" });
            }
            return Ok();
        }

        [HttpPut("update")]
        public IActionResult Update([FromQuery] int id, [FromBody] AirlineDto airlineDto)
        {
            bool result = _airlineService.UpdateAirline(id,airlineDto);
            if (airlineDto == null)
            {
                return StatusCode(500, new { message = "Internal Server Error - airport is null" });
            }
            if(result == false)
            {
                return NotFound();
            }
            return Ok(airlineDto);
        }
    }
}
