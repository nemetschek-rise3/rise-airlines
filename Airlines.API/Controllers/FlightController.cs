﻿using Airlines.Bussines.Dto;
using Airlines.Bussines.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightController : ControllerBase
    {
        private readonly IFlightService _flightService;

        public FlightController(IFlightService flightService)
        {
            _flightService = flightService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            List<FlightDto> flights = _flightService.GetFlights();
            if (flights == null)
            {
                return StatusCode(500, new { message = "Internal Server Error - flights is null" });
            }
            if (flights.Count == 0)
            {
                return NotFound();
            }
            return Ok(flights);
        }

        [HttpGet("filter")]
        public IActionResult GetByFilter([FromBody] string filter, string value)
        {
            List<FlightDto> flights = _flightService.GetFlightByFilter(filter, value);
            if (flights == null)
            {
                return StatusCode(500, new { message = "Internal Server Error - flights is null" });
            }
            if (flights.Count == 0)
            {
                return NotFound();
            }
            return Ok(flights);
        }

        [HttpPost]
        public IActionResult Add([FromBody] FlightDto flightDto)
        {
            bool result = _flightService.AddFlight(flightDto);
            if (flightDto == null)
            {
                return StatusCode(500, new { message = "Internal Server Error - flight is null" });
            }
            if(result == false)
            {
                return NotFound();
            }
            return Ok(flightDto);
        }

        [HttpDelete]
        public IActionResult DeleteByFilter([FromQuery] string filter, string value)
        {
            bool result = _flightService.DeleteFlightByFilter(filter, value);
            if (filter == null || value == null)
            {
                return StatusCode(500, new { message = "Internal Server Error - filter or value are null" });
            }
            if(result == false)
            {
                return NotFound();
            }
            return Ok();
        }

        [HttpPut("update")]
        public IActionResult Update([FromQuery] int id, [FromBody] FlightDto flightDto)
        {
            bool result = _flightService.UpdateFlight(id, flightDto);
            if (flightDto == null)
            {
                return StatusCode(500, new { message = "Internal Server Error - flight is null" });
            }
            if(result == false)
            {
                return NotFound();
            }
            return Ok(flightDto);
        }
    }
}
