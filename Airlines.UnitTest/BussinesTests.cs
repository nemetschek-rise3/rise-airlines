﻿
using Airlines.Bussines.DataStructures;
using BussinesLogic.SearchAndSort;
using Rise_Airlines.EntitiesService;
using RiseAirlines.Entities;
using RiseAirlines.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Xunit.Sdk;

namespace Airlines.UnitTest
{
    [Collection("Sequental")]
    public class BussinesTests
    {

        [Fact]
        public void ShouldCreateAirports()
        {
            HashSet<Airport> expectedAirports = new HashSet<Airport>();
            Airport airport1 = new Airport("AAA", "John F Kennedy International Airport", "New York", "United States");
            Airport airport2 = new Airport("BBB", "Los Angeles International Airport", "Los Angeles", "United States");
            Airport airport3 = new Airport("CCC", "OHare International Airport", "Chicago", "United States");
            Airport airport4 = new Airport("DDD", "HartsfieldJackson Atlanta International Airport", "Atlanta", "United States");
            expectedAirports.Add(airport1);
            expectedAirports.Add(airport2);
            expectedAirports.Add(airport3);
            expectedAirports.Add(airport4);

            HashSet<Airport> airports = AirportsService.
                CreateAirports("TestAirports.csv");

            Assert.True(IService<Airport>.CompareSets(expectedAirports, airports));
        }

        [Fact]
        public void ShouldCreateAirlines()
        {
            HashSet<Airline> expectedAirlines = new HashSet<Airline>();
            Airline airline1 = new Airline("AA", "American Airlines");
            Airline airline2 = new Airline("UA", "United Airlines");
            Airline airline3 = new Airline("DL", "Delta AirLines");
            Airline airline4 = new Airline("BA", "British Airways");
            expectedAirlines.Add(airline1);
            expectedAirlines.Add(airline2);
            expectedAirlines.Add(airline3);
            expectedAirlines.Add(airline4);

            HashSet<Airline> airlines = AirlineService.
                CreateAirlines("TestAirlines.csv");


            Assert.True(IService<Airline>.CompareSets(expectedAirlines, airlines));
        }


        [Fact]
        public void ShouldCreateFlights()
        {
            HashSet<Flight> expectedFlights = new HashSet<Flight>();
            Flight flight1 = new Flight("F1", "JFK", "LAX", "Boing747");
            Flight flight2 = new Flight("F2", "LAX", "JFK", "Boing747");
            Flight flight3 = new Flight("F3", "ATL", "ORD", "Boing747");
            Flight flight4 = new Flight("F4", "ORD", "ATL", "Boing747");
            expectedFlights.Add(flight1);
            expectedFlights.Add(flight2);
            expectedFlights.Add(flight3);
            expectedFlights.Add(flight4);

            HashSet<Flight> flights = FlightService.CreateFlights("TestFlights.csv");

            Assert.True(IService<Flight>.CompareSets(expectedFlights, flights));
        }

        [Fact]
        public void ShouldContainAirportSet()
        {
            HashSet<Airport> expectedAirports = new HashSet<Airport>();
            Airport airport1 = new Airport("AAA", "John F Kennedy International Airport", "New York", "United States");
            Airport airport2 = new Airport("BBB", "Los Angeles International Airport", "Los Angeles", "United States");
            Airport airport3 = new Airport("CCC", "OHare International Airport", "Chicago", "United States");
            Airport airport4 = new Airport("DDD", "HartsfieldJackson Atlanta International Airport", "Atlanta", "United States");
            expectedAirports.Add(airport1);
            expectedAirports.Add(airport2);
            expectedAirports.Add(airport3);
            expectedAirports.Add(airport4);

            Airport airportTest = new Airport("AAA", "John F Kennedy International Airport", "New York", "United States");


            Assert.True(IService<Airport>.ContainsSet(expectedAirports, airportTest));
        }

        [Fact]
        public void ShouldNotContainAirportSet()
        {
            HashSet<Airport> expectedAirports = new HashSet<Airport>();
            Airport airport1 = new Airport("AAA", "John F Kennedy International Airport", "New York", "United States");
            Airport airport2 = new Airport("BBB", "Los Angeles International Airport", "Los Angeles", "United States");
            Airport airport3 = new Airport("CCC", "OHare International Airport", "Chicago", "United States");
            Airport airport4 = new Airport("DDD", "HartsfieldJackson Atlanta International Airport", "Atlanta", "United States");
            expectedAirports.Add(airport1);
            expectedAirports.Add(airport2);
            expectedAirports.Add(airport3);
            expectedAirports.Add(airport4);

            Airport airportTest = new Airport("BBB", "John F Kennedy International Airport", "New York", "United States");


            Assert.False(IService<Airport>.ContainsSet(expectedAirports, airportTest));
        }

        [Fact]
        public void ShouldContainAirlineSet()
        {
            HashSet<Airline> expectedAirlines = new HashSet<Airline>();
            Airline airline1 = new Airline("AA", "American Airlines");
            Airline airline2 = new Airline("UA", "United Airlines");
            Airline airline3 = new Airline("DL", "Delta AirLines");
            Airline airline4 = new Airline("BA", "British Airways");
            expectedAirlines.Add(airline1);
            expectedAirlines.Add(airline2);
            expectedAirlines.Add(airline3);
            expectedAirlines.Add(airline4);

            Airline testAirline = new Airline("AA", "American Airlines");

            Assert.True(IService<Airline>.ContainsSet(expectedAirlines, testAirline));

        }

        [Fact]
        public void ShouldNotContainAirlineSet()
        {
            HashSet<Airline> expectedAirlines = new HashSet<Airline>();
            Airline airline1 = new Airline("AA", "American Airlines");
            Airline airline2 = new Airline("UA", "United Airlines");
            Airline airline3 = new Airline("DL", "Delta AirLines");
            Airline airline4 = new Airline("BA", "British Airways");
            expectedAirlines.Add(airline1);
            expectedAirlines.Add(airline2);
            expectedAirlines.Add(airline3);
            expectedAirlines.Add(airline4);

            Airline testAirline = new Airline("VV", "American Airlines");

            Assert.False(IService<Airline>.ContainsSet(expectedAirlines, testAirline));

        }

        [Fact]
        public void ShouldContainFlightSet()
        {
            HashSet<Flight> expectedFlights = new HashSet<Flight>();
            Flight flight1 = new Flight("F1", "JFK", "LAX", "Boing747");
            Flight flight2 = new Flight("F1", "JFK", "LAX", "Boing747");
            Flight flight3 = new Flight("F2", "LAX", "JFK", "Boing747");
            Flight flight4 = new Flight("F3", "ATL", "ORD", "Boing747");
            expectedFlights.Add(flight1);
            expectedFlights.Add(flight2);
            expectedFlights.Add(flight3);
            expectedFlights.Add(flight4);

            Flight testFlight = new Flight("F1", "JFK", "LAX", "Boing747");


            Assert.True(IService<Flight>.ContainsSet(expectedFlights, testFlight));

        }

        [Fact]
        public void ShouldNotContainFlighteSet()
        {
            HashSet<Flight> expectedFlights = new HashSet<Flight>();
            Flight flight1 = new Flight("F1", "JFK", "LAX", "Boing747");
            Flight flight2 = new Flight("F1", "JFK", "LAX", "Boing747");
            Flight flight3 = new Flight("F2", "LAX", "JFK", "Boing747");
            Flight flight4 = new Flight("F3", "ATL", "ORD", "Boing747");
            expectedFlights.Add(flight1);
            expectedFlights.Add(flight2);
            expectedFlights.Add(flight3);
            expectedFlights.Add(flight4);

            Flight testFlight = new Flight("F3", "JFK", "LAX", "Boing747");

            Assert.False(IService<Flight>.ContainsSet(expectedFlights, testFlight));
        }


        [Fact]
        public static void ShouldAllAirportFieldsHaveValue()
        {
            Airport airport = new Airport("AAA", "Sofia Airport", "Sofia", "Bulgaria");

            bool isValid = IService<Airport>.AllFieldsHaveValue(airport);

            Assert.True(isValid);
        }

        [Fact]
        public static void ShouldNotAllAirportFieldsHaveValue()
        {
            Airport airport = new Airport();
            airport.Name = "Sofia Airport";
            airport.City = "Sofia";
            airport.Country = "Bulgaria";

            bool isValid = IService<Airport>.AllFieldsHaveValue(airport);

            Assert.False(isValid);
        }
        [Fact]
        public static void ShouldAllAirlinesFieldsHaveValue()
        {
            Airline airline = new Airline("AA", "bulgaria Airlines");

            bool isValid = IService<Airline>.AllFieldsHaveValue(airline);

            Assert.True(isValid);
        }

        [Fact]
        public static void ShouldNotAllAirlinesFieldsHaveValue()
        {
            Airline airline = new Airline();
            airline.Name = "Bulgarian Airlines";


            bool isValid = IService<Airline>.AllFieldsHaveValue(airline);

            Assert.False(isValid);
        }

        [Fact]
        public static void ShouldAllFlightsFieldsHaveValue()
        {
            Flight flight = new Flight("F11", "AAA", "BBB", "Boing747");

            bool isValid = IService<Flight>.AllFieldsHaveValue(flight);

            Assert.True(isValid);
        }

        [Fact]
        public static void ShouldNotAllFlightsFieldsHaveValue()
        {
            Flight flight = new Flight();
            flight.Id = "F11";


            bool isValid = IService<Flight>.AllFieldsHaveValue(flight);

            Assert.False(isValid);
        }

        [Fact]
        public void ShouldAddFlightToTheRoute()
        {
            Flight expectedFlight = new Flight("F1", "JFK", "LAX", "Boing747");
            RouteLinkedList route = new RouteLinkedList();

            route.AddFlight(expectedFlight);
            Flight actualFlight = route.head!.Flight!;

            Assert.True(actualFlight!.Id == expectedFlight.Id);

        }

        [Fact]
        public void ShouldNotAddFlightToTheRoute()
        {
            Flight expectedFlight = null!;

            RouteLinkedList route = new RouteLinkedList();
           
                Assert.Throws<InvalidRouteException>(() => route.AddFlight(expectedFlight));
           
        }

        [Fact]
        public void ShouldAddIncorrectFlightToTheRoute()
        {
            Flight flight1 = new Flight("F1", "JFK", "LAX", "Boing747");
            Flight flight2 = new Flight("F1", "JFK", "LAX", "Boing747");

            RouteLinkedList route = new RouteLinkedList();

            Assert.Throws<InvalidRouteException>(() =>
            {
                route.AddFlight(flight1);
                route.AddFlight(flight2);
            });


        }

        [Fact]
        public void ShouldRemoveFlightFromTheRoute()
        {
            Flight expectedFlight = new Flight("F1", "JFK", "LAX", "Boing747");
            RouteLinkedList route = new RouteLinkedList();
            route.AddFlight(expectedFlight);

            route.RemoveFlight();

            Assert.True(route.head == null);

        }

        [Fact]
        public void ShouldRemoveFlightFromEmptyRoute()
        {
            RouteLinkedList route = new RouteLinkedList();

            Assert.Throws<InvalidRouteException>(route.RemoveFlight);

        }

        [Fact]
        public void ShouldEqualsAirportNodes()
        {
            AirportNode laxAirportNode = new AirportNode("LAX");
            AirportNode lhrAirportNode = new AirportNode("LHR");
            AirportNode dxbAirportNode = new AirportNode("DXB");
            AirportNode sydAirportNode = new AirportNode("SYD");
            AirportNode cptAirportNode = new AirportNode("CPT");
            AirportNode hndAirportNode = new AirportNode("HND");
            laxAirportNode.NextNodes = new List<AirportNode>() { lhrAirportNode, sydAirportNode, hndAirportNode };
            lhrAirportNode.NextNodes.Add(dxbAirportNode);
            dxbAirportNode.NextNodes.Add(cptAirportNode);
            lhrAirportNode.Parent = laxAirportNode;
            sydAirportNode.Parent = laxAirportNode;
            hndAirportNode.Parent = laxAirportNode;
            dxbAirportNode.Parent = lhrAirportNode;
            cptAirportNode.Parent = dxbAirportNode;

            AirportNode copyLaxAirportNode = new AirportNode("LAX");
            AirportNode copyLhrAirportNode = new AirportNode("LHR");
            AirportNode copyDxbAirportNode = new AirportNode("DXB");
            AirportNode copySydAirportNode = new AirportNode("SYD");
            AirportNode copyCptAirportNode = new AirportNode("CPT");
            AirportNode copyHndAirportNode = new AirportNode("HND");
            copyLaxAirportNode.NextNodes = new List<AirportNode>() { copyLhrAirportNode, copySydAirportNode, copyHndAirportNode };
            copyLhrAirportNode.NextNodes.Add(dxbAirportNode);
            copyDxbAirportNode.NextNodes.Add(cptAirportNode);
            copyLhrAirportNode.Parent = copyLaxAirportNode;
            copySydAirportNode.Parent = copyLaxAirportNode;
            copyHndAirportNode.Parent = copyLaxAirportNode;
            copyDxbAirportNode.Parent = copyLhrAirportNode;
            copyCptAirportNode.Parent = copyDxbAirportNode;

            bool result = laxAirportNode.Equals(copyLaxAirportNode);
            Assert.True(result);
        }
        [Fact]
        public void ShouldNotEqualsAirportNodes()
        {
            AirportNode laxAirportNode = new AirportNode("LIX");//changed
            AirportNode lhrAirportNode = new AirportNode("LHR");
            AirportNode dxbAirportNode = new AirportNode("DXB");
            AirportNode sydAirportNode = new AirportNode("SYD");
            AirportNode cptAirportNode = new AirportNode("CPT");
            AirportNode hndAirportNode = new AirportNode("HND");
            laxAirportNode.NextNodes = new List<AirportNode>() { lhrAirportNode, sydAirportNode, hndAirportNode };
            lhrAirportNode.NextNodes.Add(dxbAirportNode);
            dxbAirportNode.NextNodes.Add(cptAirportNode);
            lhrAirportNode.Parent = laxAirportNode;
            sydAirportNode.Parent = laxAirportNode;
            hndAirportNode.Parent = laxAirportNode;
            dxbAirportNode.Parent = lhrAirportNode;
            cptAirportNode.Parent = dxbAirportNode;

            AirportNode copyLaxAirportNode = new AirportNode("LAX");
            AirportNode copyLhrAirportNode = new AirportNode("LHR");
            AirportNode copyDxbAirportNode = new AirportNode("DXB");
            AirportNode copySydAirportNode = new AirportNode("SYD");
            AirportNode copyCptAirportNode = new AirportNode("CPT");
            AirportNode copyHndAirportNode = new AirportNode("HND");
            copyLaxAirportNode.NextNodes = new List<AirportNode>() { copyLhrAirportNode, copySydAirportNode, copyHndAirportNode };
            copyLhrAirportNode.NextNodes.Add(dxbAirportNode);
            copyDxbAirportNode.NextNodes.Add(cptAirportNode);
            copyLhrAirportNode.Parent = copyLaxAirportNode;
            copySydAirportNode.Parent = copyLaxAirportNode;
            copyHndAirportNode.Parent = copyLaxAirportNode;
            copyDxbAirportNode.Parent = copyLhrAirportNode;
            copyCptAirportNode.Parent = copyDxbAirportNode;

            bool result = laxAirportNode.Equals(copyLaxAirportNode);
            Assert.False(result);
        }
        [Fact]
        public void ShouldBuildTreeFromCSV()
        {
            HashSet<Flight> flights = FlightService.CreateFlights("Flights.csv");
            ArrayList flightsTree = FlightService.ReadFlightsFromCsv("LAXRoute.csv", flights);

            AirportNode laxAirportNode = new AirportNode("LAX");
            AirportNode lhrAirportNode = new AirportNode("LHR");
            AirportNode dxbAirportNode = new AirportNode("DXB");
            AirportNode sydAirportNode = new AirportNode("SYD");
            AirportNode cptAirportNode = new AirportNode("CPT");
            AirportNode hndAirportNode = new AirportNode("HND");
            laxAirportNode.NextNodes = new List<AirportNode>() { lhrAirportNode, sydAirportNode, hndAirportNode };
            lhrAirportNode.NextNodes.Add(dxbAirportNode);
            dxbAirportNode.NextNodes.Add(cptAirportNode);
            lhrAirportNode.Parent = laxAirportNode;
            sydAirportNode.Parent = laxAirportNode;
            hndAirportNode.Parent = laxAirportNode;
            dxbAirportNode.Parent = lhrAirportNode;
            cptAirportNode.Parent = dxbAirportNode;

            FlightRouteTree flightRouteTree = new FlightRouteTree();
            AirportNode rootAirportNode = flightRouteTree.BuildTreeFromCSV(flightsTree);

            bool result = laxAirportNode.Equals(rootAirportNode);
            Assert.True(result);
        }

        [Fact]
        public void ShouldUseDFS()
        {
            AirportNode laxAirportNode = new AirportNode("LAX");
            AirportNode lhrAirportNode = new AirportNode("LHR");
            AirportNode dxbAirportNode = new AirportNode("DXB");
            AirportNode sydAirportNode = new AirportNode("SYD");
            AirportNode cptAirportNode = new AirportNode("CPT");
            AirportNode hndAirportNode = new AirportNode("HND");
            laxAirportNode.NextNodes = new List<AirportNode>() { lhrAirportNode, sydAirportNode, hndAirportNode };
            lhrAirportNode.NextNodes.Add(dxbAirportNode);
            dxbAirportNode.NextNodes.Add(cptAirportNode);
            lhrAirportNode.Parent = laxAirportNode;
            sydAirportNode.Parent = laxAirportNode;
            hndAirportNode.Parent = laxAirportNode;
            dxbAirportNode.Parent = lhrAirportNode;
            cptAirportNode.Parent = dxbAirportNode;

            FlightRouteTree flightRouteTree = new FlightRouteTree();

            AirportNode foundAirportNode = flightRouteTree.DFS(laxAirportNode, "CPT");

            bool result = cptAirportNode.Equals(foundAirportNode);
            Assert.True(result);

        }

        [Fact]
        public void ShouldFindDestinationRoute()
        {
            AirportNode laxAirportNode = new AirportNode("LAX");
            AirportNode lhrAirportNode = new AirportNode("LHR");
            AirportNode dxbAirportNode = new AirportNode("DXB");
            AirportNode sydAirportNode = new AirportNode("SYD");
            AirportNode cptAirportNode = new AirportNode("CPT");
            AirportNode hndAirportNode = new AirportNode("HND");
            laxAirportNode.NextNodes = new List<AirportNode>() { lhrAirportNode, sydAirportNode, hndAirportNode };
            lhrAirportNode.NextNodes.Add(dxbAirportNode);
            dxbAirportNode.NextNodes.Add(cptAirportNode);
            lhrAirportNode.Parent = laxAirportNode;
            sydAirportNode.Parent = laxAirportNode;
            hndAirportNode.Parent = laxAirportNode;
            dxbAirportNode.Parent = lhrAirportNode;
            cptAirportNode.Parent = dxbAirportNode;

            List<String> destinationRoute = new List<string>() { "LAX", "LHR", "DXB" };
            FlightRouteTree flightRouteTree = new FlightRouteTree();
            List<String> foundRoute = flightRouteTree.FindDestinationRoute(dxbAirportNode, laxAirportNode);

            bool result = IService<String>.CompareLists(destinationRoute, foundRoute);
            Assert.True(result);
        }

        [Fact]
        public void ShouldLoadFlightTreeFromCsv()
        {
            string rootAirport = "LAX";
            Flight flight1 = new Flight("F1", "LAX", "LHR", "Boing747");
            Flight flight2 = new Flight("F12", "LAX", "SYD", "AirbusA380");
            Flight flight3 = new Flight("F14", "LAX", "HND", "AirbusA380");
            Flight flight4 = new Flight("F3", "LHR", "DXB", "Boing747");
            Flight flight5 = new Flight("F7", "DXB", "CPT", "AirbusA320");
            ArrayList expFlightTree = new ArrayList();
            expFlightTree.Add(rootAirport);
            expFlightTree.Add(flight1);
            expFlightTree.Add(flight2);
            expFlightTree.Add(flight3);
            expFlightTree.Add(flight4);
            expFlightTree.Add(flight5);

            HashSet<Flight> flights = FlightService.CreateFlights("Flights.csv");
            ArrayList flightsTree = FlightService.ReadFlightsFromCsv("LAXRoute.csv", flights);
            bool result = IService<Flight>.
                CompareArrayLists(expFlightTree, flightsTree);// I passed flight to IService because of the error, this methode doesn't use generic, probably should move it to another class later.

            Assert.True(result);

        }
    }
}
