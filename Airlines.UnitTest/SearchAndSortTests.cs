﻿using Airlines.Bussines.Entities;
using BussinesLogic.SearchAndSort;
using Microsoft.VisualStudio.TestPlatform.Utilities;
using Rise_Airlines.EntitiesService;
using RiseAirlines.Entities;
using RiseAirlines.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Airlines.UnitTest
{
    [Collection("Sequental")]
    public class SearchAndSortTests
    {
        [Fact]
        public static void ShouldAirportCountrySearch()
        {

            HashSet<Airport> airports = AirportsService.
                 CreateAirports("TestAirports.csv");

            string airportName = "John F Kennedy International Airport";

            IEnumerable<Airport> airportsCountries = Search.AirportCountrySearch("United States", airports);
            bool result = airportsCountries.Any(airport => airport.Name == airportName);
            Assert.True(result);
        }

        [Fact]
        public static void ShouldNotAirportCountrySearch()
        {
            HashSet<Airport> airports = AirportsService.
                 CreateAirports("TestAirports.csv");

            string expectedOutput = "No airports found!";

            string errorMessage = "false";
            try
            {
                Search.AirportCountrySearch("Bulgaria", airports);
            }
            catch (InvalidAirportException ex)
            {
                errorMessage = ex.Message;
            }
            Assert.Equal(expectedOutput, errorMessage);

        }

        [Fact]
        public static void ShouldAirportCitySearch()
        {
            HashSet<Airport> airports = AirportsService.
                 CreateAirports("TestAirports.csv");

            string airportName = "John F Kennedy International Airport";

            IEnumerable<Airport> airportsCountries = Search.AirportCitySearch("New York", airports);
            bool result = airportsCountries.Any(airport => airport.Name == airportName);
            Assert.True(result);

        }

        [Fact]
        public static void ShouldNotAirportCitySearch()
        {
            HashSet<Airport> airports = AirportsService.
                 CreateAirports("TestAirports.csv");

            string expectedOutput = "No airports found!";

            string errorMessage = "false";
            try
            {
                Search.AirportCitySearch("Bulgaria", airports);
            }
            catch (InvalidAirportException ex)
            {
                errorMessage = ex.Message;
            }
            Assert.Equal(expectedOutput, errorMessage);


        }

        [Fact]
        public static void ShouldAirlineSearch()
        {
            HashSet<Airline> airlines = AirlineService.
                CreateAirlines("TestAirlines.csv");

            bool expOutput = true;


            bool result = Search.AirlineSearch("American Airlines", airlines);

            Assert.Equal(expOutput, result);
        }

        [Fact]
        public static void ShouldNotAirlineSearch()
        {
            HashSet<Airline> airlines = AirlineService.
                CreateAirlines("TestAirlines.csv");

            bool expOutput = false;


            bool result = Search.AirlineSearch("American Express", airlines);

            Assert.Equal(expOutput, result);

        }

       

        [Fact]
        public static void ShouldFlightSearch()
        {
            HashSet<Flight> flights = FlightService.
                 CreateFlights("TestFlights.csv");

            Flight expectedFlight = new Flight("F1", "JFK", "LAX", "Boing747");

            Flight flight = Search.FlightSearch("F1", flights);

            bool result = flight.Equals(expectedFlight);

            Assert.True(result);
        }

        [Fact]
        public static void ShouldNotFlightSearch()
        {
            HashSet<Flight> flights = FlightService.
                 CreateFlights("TestFlights.csv");

            Flight expectedFlight = new Flight("F1", "JJJ", "LAX", "Boing411");

            Flight flight = Search.FlightSearch("F1", flights);

            bool result = flight.Equals(expectedFlight);

            Assert.False(result);
        }

        [Fact]
        public static void ShouldEmptyFlightSearch()
        {
            HashSet<Flight> flights = FlightService.
                 CreateFlights("TestFlights.csv");

            Flight? expectedFlight = null;

            Flight flight = Search.FlightSearch("F16", flights);

            Assert.Equal(expectedFlight, flight);
        }

        [Fact]
        public void ShouldBubbleSortAirportById()
        {
            List<Airport> expectedAirports = new List<Airport>();
            Airport airport1 = new Airport("AAA", "John F Kennedy International Airport", "New York", "United States");
            Airport airport2 = new Airport("BBB", "Los Angeles International Airport", "Los Angeles", "United States");
            Airport airport3 = new Airport("CCC", "OHare International Airport", "Chicago", "United States");
            Airport airport4 = new Airport("DDD", "HartsfieldJackson Atlanta International Airport", "Atlanta", "United States");
            expectedAirports.Add(airport4);
            expectedAirports.Add(airport3);
            expectedAirports.Add(airport2);
            expectedAirports.Add(airport1);

            HashSet<Airport> airports = AirportsService.
                CreateAirports("TestAirports.csv");
            List<Airport> airportsList = new List<Airport>(airports);
            Sort.BubbleSortById(airportsList, OrderSort.Descending);

            Assert.True(IService<Airport>.CompareLists(expectedAirports, airportsList));


        }

        [Fact]
        public void ShouldBubbleSortAirportByName()
        {
            List<Airport> expectedAirports = new List<Airport>();
            Airport airport1 = new Airport("AAA", "John F Kennedy International Airport", "New York", "United States");
            Airport airport2 = new Airport("BBB", "Los Angeles International Airport", "Los Angeles", "United States");
            Airport airport3 = new Airport("CCC", "OHare International Airport", "Chicago", "United States");
            Airport airport4 = new Airport("DDD", "HartsfieldJackson Atlanta International Airport", "Atlanta", "United States");
            expectedAirports.Add(airport4);
            expectedAirports.Add(airport1);
            expectedAirports.Add(airport2);
            expectedAirports.Add(airport3);

            HashSet<Airport> airports = AirportsService.
                CreateAirports("TestAirports.csv");
            List<Airport> airportsList = new List<Airport>(airports);
            Sort.BubbleSortByName(airportsList, OrderSort.Ascending);

            Assert.True(IService<Airport>.CompareLists(expectedAirports, airportsList));

        }




        [Fact]
        public void ShouldBubbleSortAirlineById()
        {
            List<Airline> expectedAirlines = new List<Airline>();
            Airline airline1 = new Airline("AA", "American Airlines");
            Airline airline2 = new Airline("UA", "United Airlines");
            Airline airline3 = new Airline("DL", "Delta AirLines");
            Airline airline4 = new Airline("BA", "British Airways");
            expectedAirlines.Add(airline1);
            expectedAirlines.Add(airline4);
            expectedAirlines.Add(airline3);
            expectedAirlines.Add(airline2);

            HashSet<Airline> airlines = AirlineService.
                CreateAirlines("TestAirlines.csv");
            List<Airline> airlinesList = new List<Airline>(airlines);
            Sort.BubbleSortById(airlinesList, OrderSort.Ascending);

            Assert.True(IService<Airline>.CompareLists(expectedAirlines, airlinesList));

        }

        [Fact]
        public void ShouldBubbleSortAirlineByName()
        {
            List<Airline> expectedAirlines = new List<Airline>();
            Airline airline1 = new Airline("AA", "American Airlines");
            Airline airline2 = new Airline("UA", "United Airlines");
            Airline airline3 = new Airline("DL", "Delta AirLines");
            Airline airline4 = new Airline("BA", "British Airways");
            expectedAirlines.Add(airline1);
            expectedAirlines.Add(airline4);
            expectedAirlines.Add(airline3);
            expectedAirlines.Add(airline2);

            HashSet<Airline> airlines = AirlineService.
                CreateAirlines("TestAirlines.csv");
            List<Airline> airlinesList = new List<Airline>(airlines);
            Sort.BubbleSortByName(airlinesList, OrderSort.Ascending);

            Assert.True(IService<Airline>.CompareLists(expectedAirlines, airlinesList));

        }


        //[Fact]
        //public void PositiveBubbleSortDescending()
        //{
        //    string[] unsortedArr = ["bbb", "bab", "aaa", "cab", "abb"];

        //    string[] sortedArr = ["cab", "bbb", "bab", "abb", "aaa"];

        //    Sort.BubbleSort(unsortedArr, OrderSort.Descending);

        //    Assert.Equal(sortedArr, unsortedArr);
        //}

        //[Fact]
        //public void PositiveBubbleSortAscending()
        //{
        //    string[] unsortedArr = ["bbb", "bab", "aaa", "aab", "abb"];

        //    string[] sortedArr = ["aaa", "aab", "abb", "bab", "bbb"];

        //    Sort.BubbleSort(unsortedArr, OrderSort.Ascending);

        //    Assert.Equal(sortedArr, unsortedArr);
        //}

        //[Fact]
        //public void NegativeSelectionSortDescending()
        //{
        //    string[] unsortedArr = ["bbb", "bab", "aaa", "cab", "abb"];

        //    string[] sortedArr = ["aaa", "aab", "abb", "bab", "bbb"];

        //    Sort.SelectionSort(unsortedArr, OrderSort.Descending);

        //    Assert.NotEqual(sortedArr, unsortedArr);
        //}

        //[Fact]
        //public void NegativeSelectionSortAscending()
        //{
        //    string[] unsortedArr = ["bbb", "bab", "aaa", "aab", "abb"];

        //    string[] sortedArr = ["cab", "bbb", "bab", "abb", "aaa"];

        //    Sort.SelectionSort(unsortedArr, OrderSort.Ascending);

        //    Assert.NotEqual(sortedArr, unsortedArr);
        //}

        //[Fact]
        //public void ShouldBinarySearch()
        //{
        //    int expectedIndex = 0;
        //    string inputValue = "aba";
        //    string[] arr = ["aba", "abs", "bnb"];

        //    int outputValue = Search.BinarySearch(arr, inputValue, OrderSort.Ascending);

        //    Assert.Equal(expectedIndex, outputValue);
        //}

        //[Fact]
        //public void ShouldNotBinarySearch()
        //{
        //    int expectedValue = -1;
        //    string inputValue = "bba";
        //    string[] arr = ["aba", "abs", "bnb"];

        //    int outputValue = Search.BinarySearch(arr, inputValue, OrderSort.Ascending);

        //    Assert.Equal(expectedValue, outputValue);
        //}

        //[Fact]
        //public void PositiveBinarySearchManager()
        //{

        //    string[] airports = ["JFK", "LAX", "ORD", "DFW"];
        //    string[] airlines = ["Delta", "American", "United"];
        //    string[] flights = ["DL123", "AA456", "UA789"];

        //    string[] inputs = ["LAX"];
        //    string input = "y\nLAX\nn";

        //    string expectedPromt = "Enter input for search: ";
        //    using var sw = new StringWriter();

        //    Console.SetOut(sw);
        //    using StringReader sr = new StringReader(input);
        //    Console.SetIn(sr);
        //    Search.BinarySearchManager(airports, airlines, flights, OrderSort.Ascending);
        //    foreach (string input1 in inputs)
        //    {
        //        string expectedOutput = $"The input: {input1} is an airport!";
        //        Assert.Contains(expectedOutput, sw.ToString().Trim());
        //        Assert.Contains(expectedPromt, sw.ToString().Trim());
        //    }
        //}

        //[Fact]
        //public void NegativeBinarySearchManager()
        //{

        //    string[] airports = ["JFK", "LAX", "ORD", "DFW"];
        //    string[] airlines = ["Delta", "Amer", "Unit"];
        //    string[] flights = ["DL123", "AA456", "UA789"];

        //    string input = "y\nLAXy\nAmber\nn";
        //    string[] inputs = ["LAX", "Amber"];
        //    string expectedOutput = "The input is not found!";


        //    string expectedPromt = "Enter input for search: ";
        //    using var sw = new StringWriter();

        //    Console.SetOut(sw);
        //    using StringReader sr = new StringReader(input);
        //    Console.SetIn(sr);
        //    Search.BinarySearchManager(airports, airlines, flights, OrderSort.Ascending);
        //    for (int i = 0; i < inputs.Length; i++)
        //    {
        //        Assert.Contains(expectedOutput, sw.ToString().Trim());
        //        Assert.Contains(expectedPromt, sw.ToString().Trim());
        //    }
        //}

        //[Fact]
        //public void ShouldGenerateRandomString()
        //{
        //    string randomGeneratedString = Sort.GenerateRandomString();
        //    int length = randomGeneratedString.Length;

        //    Assert.True(length > 0);
        //    Assert.True(!string.IsNullOrEmpty(randomGeneratedString));
        //    Assert.True(length < 100);
        //}

        //[Fact]
        //public void ShouldGenerateRandomArray()
        //{
        //    int size = 100;

        //    int arrLength = Sort.GenerateRandomArray(size).Length;

        //    Assert.Equal(size, arrLength);
        //}

        //[Fact]
        //public void CompareSortingAlgorithmsSmallArr()
        //{
        //    Random random = new Random();
        //    int length = random.Next(1, 100);
        //    string[] randomArray = Sort.GenerateRandomArray(length);

        //    long bubbleIterations = Sort.CountBubbleSortIterations(randomArray);
        //    long selectionIterations = Sort.CountSelectionSortIterations(randomArray);

        //    Assert.True(bubbleIterations < selectionIterations);
        //}
    }
}
