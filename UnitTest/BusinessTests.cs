﻿
using Airlines.Bussines.DataStructures;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Airlines.Bussines.Service;
using Airlines.Bussines.StrategyAlgorithms;
using Airlines.Persistance.Basic.EntitiesService;
using Airlines.old.Persistance.Basic.Models;
using RiseAirlines.Persistance.Basic.EntitiesService;
using RiseAirlines.Persistance.Basic.Exceptions;
using Xunit.Sdk;
using FlightManager = RiseAirlines.Persistance.Basic.EntitiesService.FlightManager;

namespace Airlines.UnitTest
{
    [Collection("Sequental")]
    public class BusinessTests
    {
        private static DateTime flightDepDateTime = new DateTime(2024, 6, 1, 08, 00, 00);
        private static DateTime flightArrDateTime = new DateTime(2024, 6, 01, 15, 00, 00);
        private static DateOnly founded = new DateOnly(1967, 5, 28);

        [Fact]
        public void ShouldCreateAirports()
        {
            HashSet<AirportModel> expectedAirports = new HashSet<AirportModel>();
            AirportModel airport1 = new AirportModel(1,"AAA", "John F Kennedy International Airport", "New York", "United States",3,founded);
            AirportModel airport2 = new AirportModel(2,"BBB", "Los Angeles International Airport", "Los Angeles", "United States",4,founded);
            AirportModel airport3 = new AirportModel(3,"CCC", "OHare International Airport", "Chicago", "United States",4,founded);
            AirportModel airport4 = new AirportModel(4,"DDD", "HartsfieldJackson Atlanta International Airport", "Atlanta", "United States",3, founded);
            expectedAirports.Add(airport1);
            expectedAirports.Add(airport2);
            expectedAirports.Add(airport3);
            expectedAirports.Add(airport4);

            HashSet<AirportModel> airports = AirportsManager.
                CreateAirports("CSV/TestAirports.csv");
            Service<AirportModel> service = new Service<AirportModel>();

            Assert.True(service.CompareSets(expectedAirports, airports));
        }

        [Fact]
        public void ShouldCreateAirlines()
        {
            HashSet<AirlineModel> expectedAirlines = new HashSet<AirlineModel>();
            AirlineModel airline1 = new AirlineModel(1,"AA", founded,10);
            AirlineModel airline2 = new AirlineModel(2,"UA", founded,10);
            AirlineModel airline3 = new AirlineModel(3,"DL", founded,10);
            AirlineModel airline4 = new AirlineModel(4,"BA", founded, 10);
            expectedAirlines.Add(airline1);
            expectedAirlines.Add(airline2);
            expectedAirlines.Add(airline3);
            expectedAirlines.Add(airline4);

            HashSet<AirlineModel> airlines = AirlineManager.
                CreateAirlines("CSV/TestAirlines.csv");

            Service<AirlineModel> service = new Service<AirlineModel>();

            Assert.True(service.CompareSets(expectedAirlines, airlines));
        }


        [Fact]
        public void ShouldCreateFlights()
        {
            HashSet<FlightModel> expectedFlights = new HashSet<FlightModel>();
            FlightModel flight1 = new FlightModel(1,"F1", 1, 2, "Boing747",700, 7,flightArrDateTime,flightArrDateTime);
            FlightModel flight2 = new FlightModel(2, "F2", 2, 1, "Boing747", 700, 7, flightDepDateTime, flightArrDateTime);
            FlightModel flight3 = new FlightModel(3, "F3", 1, 3, "Boing747", 1050, 8,flightDepDateTime,flightArrDateTime);
            FlightModel flight4 = new FlightModel(4, "F4", 3, 1, "Boing747", 1050, 8, flightDepDateTime, flightArrDateTime);
            FlightModel flight5 = new FlightModel(5, "F5", 2, 3, "Boing747", 150, 1,flightDepDateTime,flightArrDateTime);
            FlightModel flight6 = new FlightModel(6, "F6", 3, 2, "Boing747", 150, 1, flightDepDateTime, flightArrDateTime);
            FlightModel flight7 = new FlightModel(7, "F7", 2, 4, "Boing747", 400, 2,flightDepDateTime,flightArrDateTime);
            FlightModel flight8 = new FlightModel(8, "F8", 4, 2, "Boing747", 400, 2, flightDepDateTime, flightArrDateTime);
            FlightModel flight9 = new FlightModel(9, "F9", 4, 3, "Boing747", 200, 1,flightDepDateTime,flightArrDateTime);
            FlightModel flight10 = new FlightModel(10,"F10", 3, 4, "Boing747", 200, 1,flightDepDateTime,flightArrDateTime);


            expectedFlights.Add(flight1);
            expectedFlights.Add(flight2);
            expectedFlights.Add(flight3);
            expectedFlights.Add(flight4);
            expectedFlights.Add(flight5);
            expectedFlights.Add(flight6);
            expectedFlights.Add(flight7);
            expectedFlights.Add(flight8);
            expectedFlights.Add(flight9);
            expectedFlights.Add(flight10);


            HashSet<FlightModel> flights = FlightManager.CreateFlights("CSV/TestFlights.csv");
            Service<FlightModel> service = new Service<FlightModel>();

            Assert.True(service.CompareSets(expectedFlights, flights));
        }

        [Fact]
        public void ShouldContainAirportSet()
        {
            HashSet<AirportModel> expectedAirports = new HashSet<AirportModel>();
            AirportModel airport1 = new AirportModel(1, "AAA", "John F Kennedy International Airport", "New York", "United States", 3, founded);
            AirportModel airport2 = new AirportModel(2, "BBB", "Los Angeles International Airport", "Los Angeles", "United States", 4, founded);
            AirportModel airport3 = new AirportModel(3, "CCC", "OHare International Airport", "Chicago", "United States", 4, founded);
            AirportModel airport4 = new AirportModel(4, "DDD", "HartsfieldJackson Atlanta International Airport", "Atlanta", "United States", 3, founded);
            expectedAirports.Add(airport1);
            expectedAirports.Add(airport2);
            expectedAirports.Add(airport3);
            expectedAirports.Add(airport4);

            AirportModel airportTest = new AirportModel(1, "AAA", "John F Kennedy International Airport", "New York", "United States", 3, founded);
            Service<AirportModel> service = new Service<AirportModel>();

            Assert.True(service.ContainsSet(expectedAirports, airportTest));
        }

        [Fact]
        public void ShouldNotContainAirportSet()
        {
            HashSet<AirportModel> expectedAirports = new HashSet<AirportModel>();
            AirportModel airport1 = new AirportModel(1, "AAA", "John F Kennedy International Airport", "New York", "United States", 3, founded);
            AirportModel airport2 = new AirportModel(2, "BBB", "Los Angeles International Airport", "Los Angeles", "United States", 4, founded);
            AirportModel airport3 = new AirportModel(3, "CCC", "OHare International Airport", "Chicago", "United States", 4, founded);
            AirportModel airport4 = new AirportModel(4, "DDD", "HartsfieldJackson Atlanta International Airport", "Atlanta", "United States", 3, founded);
            expectedAirports.Add(airport1);
            expectedAirports.Add(airport2);
            expectedAirports.Add(airport3);
            expectedAirports.Add(airport4);

            AirportModel airportTest = new AirportModel(6, "FFF", "John F Kennedy International Airport", "New York", "United States", 3, founded);
            Service<AirportModel> service = new Service<AirportModel>();

            Assert.False(service.ContainsSet(expectedAirports, airportTest));
        }

        [Fact]
        public void ShouldContainAirlineSet()
        {
            HashSet<AirlineModel> expectedAirlines = new HashSet<AirlineModel>();
            AirlineModel airline1 = new AirlineModel(1, "AA", founded, 10);
            AirlineModel airline2 = new AirlineModel(2, "UA", founded, 10);
            AirlineModel airline3 = new AirlineModel(3, "DL", founded, 10);
            AirlineModel airline4 = new AirlineModel(4, "BA", founded, 10);
            expectedAirlines.Add(airline1);
            expectedAirlines.Add(airline2);
            expectedAirlines.Add(airline3);
            expectedAirlines.Add(airline4);

            AirlineModel testAirline = new AirlineModel(1, "AA", founded, 10);
            Service<AirlineModel> service = new Service<AirlineModel>();

            Assert.True(service.ContainsSet(expectedAirlines, testAirline));

        }

        [Fact]
        public void ShouldNotContainAirlineSet()
        {
            HashSet<AirlineModel> expectedAirlines = new HashSet<AirlineModel>();
            AirlineModel airline1 = new AirlineModel(1, "AA", founded, 10);
            AirlineModel airline2 = new AirlineModel(2, "UA", founded, 10);
            AirlineModel airline3 = new AirlineModel(3, "DL", founded, 10);
            AirlineModel airline4 = new AirlineModel(4, "BA", founded, 10);
            expectedAirlines.Add(airline1);
            expectedAirlines.Add(airline2);
            expectedAirlines.Add(airline3);
            expectedAirlines.Add(airline4);

            AirlineModel testAirline = new AirlineModel(1, "AB", founded, 10);
            Service<AirlineModel> service = new Service<AirlineModel>();

            Assert.False(service.ContainsSet(expectedAirlines, testAirline));

        }

        [Fact]
        public void ShouldContainFlightSet()
        {
            HashSet<FlightModel> expectedFlights = new HashSet<FlightModel>();

            FlightModel flight1 = new FlightModel(1, "F1", 1, 2, "Boing747", 700, 7, flightArrDateTime, flightArrDateTime);
            FlightModel flight2 = new FlightModel(2, "F2", 2, 1, "Boing747", 700, 7, flightDepDateTime, flightArrDateTime);
            FlightModel flight3 = new FlightModel(3, "F3", 1, 3, "Boing747", 1050, 8, flightDepDateTime, flightArrDateTime);
            FlightModel flight4 = new FlightModel(4, "F4", 3, 1, "Boing747", 1050, 8, flightDepDateTime, flightArrDateTime);
            FlightModel flight5 = new FlightModel(5, "F5", 2, 3, "Boing747", 150, 1, flightDepDateTime, flightArrDateTime);
            
            expectedFlights.Add(flight1);
            expectedFlights.Add(flight2);
            expectedFlights.Add(flight3);
            expectedFlights.Add(flight4);
            expectedFlights.Add(flight5);


            FlightModel testFlight = new FlightModel(1, "F1", 1, 2, "Boing747", 700, 7, flightArrDateTime, flightArrDateTime);
            Service<FlightModel> service = new Service<FlightModel>();

            Assert.True(service.ContainsSet(expectedFlights, testFlight));
        }

        [Fact]
        public void ShouldNotContainFlightsSet()
        {
            HashSet<FlightModel> expectedFlights = new HashSet<FlightModel>();

            FlightModel flight1 = new FlightModel(1, "F1", 1, 2, "Boing747", 700, 7, flightArrDateTime, flightArrDateTime);
            FlightModel flight2 = new FlightModel(2, "F2", 2, 1, "Boing747", 700, 7, flightDepDateTime, flightArrDateTime);
            FlightModel flight3 = new FlightModel(3, "F3", 1, 3, "Boing747", 1050, 8, flightDepDateTime, flightArrDateTime);
            FlightModel flight4 = new FlightModel(4, "F4", 3, 1, "Boing747", 1050, 8, flightDepDateTime, flightArrDateTime);
            FlightModel flight5 = new FlightModel(5, "F5", 2, 3, "Boing747", 150, 1, flightDepDateTime, flightArrDateTime);


            expectedFlights.Add(flight1);
            expectedFlights.Add(flight2);
            expectedFlights.Add(flight3);
            expectedFlights.Add(flight4);
            expectedFlights.Add(flight5);


            FlightModel testFlight = new FlightModel(7, "F7", 1, 4, "Boing747", 700, 7, flightArrDateTime, flightArrDateTime);
            Service<FlightModel> service = new Service<FlightModel>();

            Assert.False(service.ContainsSet(expectedFlights, testFlight));
        }


        [Fact]
        public static void ShouldAllAirportFieldsHaveValue()
        {
            AirportModel airportTest = new AirportModel(1, "AAA", "John F Kennedy International Airport", "New York", "United States", 3, founded);
            Service<AirportModel> service = new Service<AirportModel>();

            bool isValid = service.AllFieldsHaveValue(airportTest);

            Assert.True(isValid);
        }

        [Fact]
        public static void ShouldNotAllAirportFieldsHaveValue()
        {
            AirportModel airport = new AirportModel();
            airport.Name = "Sofia Airport";
            airport.City = "Sofia";
            airport.Country = "Bulgaria";
            Service<AirportModel> service = new Service<AirportModel>();

            bool isValid = service.AllFieldsHaveValue(airport);

            Assert.False(isValid);
        }
        [Fact]
        public static void ShouldAllAirlinesFieldsHaveValue()
        {
            AirlineModel testAirline = new AirlineModel(1, "AA", founded, 10);
            Service<AirlineModel> service = new Service<AirlineModel>();

            bool isValid = service.AllFieldsHaveValue(testAirline);

            Assert.True(isValid);
        }

        [Fact]
        public static void ShouldNotAllAirlinesFieldsHaveValue()
        {
            AirlineModel airline = new AirlineModel();
            airline.Name = "BA";
            Service<AirlineModel> service = new Service<AirlineModel>();

            bool isValid = service.AllFieldsHaveValue(airline);

            Assert.False(isValid);
        }

        [Fact]
        public static void ShouldAllFlightsFieldsHaveValue()
        {
            FlightModel flight = new FlightModel(1, "F1", 1, 2, "Boing747", 700, 7, flightArrDateTime, flightArrDateTime);

            Service<AirlineModel> service = new Service<AirlineModel>();

            bool isValid = service.AllFieldsHaveValue(flight);

            Assert.True(isValid);
        }

        [Fact]
        public static void ShouldNotAllFlightsFieldsHaveValue()
        {
            FlightModel flight = new FlightModel();
            flight.Id = 1;
            Service<FlightModel> service = new Service<FlightModel>();

            bool isValid = service.AllFieldsHaveValue(flight);

            Assert.False(isValid);
        }

        [Fact]
        public void ShouldAddFlightToTheRoute()
        {
            FlightModel expectedFlight = new FlightModel(5, "F5", 2, 3, "Boing747", 150, 1, flightDepDateTime, flightArrDateTime);
            RouteLinkedList route = new RouteLinkedList();

            route.AddFlight(expectedFlight);
            FlightModel actualFlight = route.head!.Flight!;

            Assert.True(actualFlight!.Id == expectedFlight.Id);

        }

        [Fact]
        public void ShouldNotAddFlightToTheRoute()
        {
            FlightModel expectedFlight = null!;

            RouteLinkedList route = new RouteLinkedList();
           
                Assert.Throws<InvalidRouteException>(() => route.AddFlight(expectedFlight));
           
        }

        [Fact]
        public void ShouldAddIncorrectFlightToTheRoute()
        {
            FlightModel flight1 = new FlightModel(5, "F5", 2, 3, "Boing747", 150, 1, flightDepDateTime, flightArrDateTime);
            FlightModel flight2 = new FlightModel(5, "F5", 2, 3, "Boing747", 150, 1, flightDepDateTime, flightArrDateTime);

            RouteLinkedList route = new RouteLinkedList();

            Assert.Throws<InvalidRouteException>(() =>
            {
                route.AddFlight(flight1);
                route.AddFlight(flight2);
            });


        }

        [Fact]
        public void ShouldRemoveFlightFromTheRoute()
        {
            FlightModel expectedFlight = new FlightModel(5, "F5", 2, 3, "Boing747", 150, 1, flightDepDateTime, flightArrDateTime);
            RouteLinkedList route = new RouteLinkedList();
            route.AddFlight(expectedFlight);

            route.RemoveFlight();

            Assert.True(route.head == null);

        }

        [Fact]
        public void ShouldRemoveFlightFromEmptyRoute()
        {
            RouteLinkedList route = new RouteLinkedList();

            Assert.Throws<InvalidRouteException>(route.RemoveFlight);

        }

        //Test for Task17
        [Fact]
        public void ShouldEqualsAirportNodes()
        {
            TreeNode laxAirportNode = new TreeNode(1);
            TreeNode lhrAirportNode = new TreeNode(2);
            TreeNode dxbAirportNode = new TreeNode(5);
            TreeNode sydAirportNode = new TreeNode(9);
            TreeNode cptAirportNode = new TreeNode(7);
            TreeNode hndAirportNode = new TreeNode(10);
            laxAirportNode.NextNodes = new List<TreeNode>() { lhrAirportNode, sydAirportNode, hndAirportNode };
            lhrAirportNode.NextNodes.Add(dxbAirportNode);
            dxbAirportNode.NextNodes.Add(cptAirportNode);
            lhrAirportNode.Parent = laxAirportNode;
            sydAirportNode.Parent = laxAirportNode;
            hndAirportNode.Parent = laxAirportNode;
            dxbAirportNode.Parent = lhrAirportNode;
            cptAirportNode.Parent = dxbAirportNode;

            TreeNode copyLaxAirportNode = new TreeNode(1);
            TreeNode copyLhrAirportNode = new TreeNode(2);
            TreeNode copyDxbAirportNode = new TreeNode(5);
            TreeNode copySydAirportNode = new TreeNode(9);
            TreeNode copyCptAirportNode = new TreeNode(7);
            TreeNode copyHndAirportNode = new TreeNode(10);
            copyLaxAirportNode.NextNodes = new List<TreeNode>() { copyLhrAirportNode, copySydAirportNode, copyHndAirportNode };
            copyLhrAirportNode.NextNodes.Add(dxbAirportNode);
            copyDxbAirportNode.NextNodes.Add(cptAirportNode);
            copyLhrAirportNode.Parent = copyLaxAirportNode;
            copySydAirportNode.Parent = copyLaxAirportNode;
            copyHndAirportNode.Parent = copyLaxAirportNode;
            copyDxbAirportNode.Parent = copyLhrAirportNode;
            copyCptAirportNode.Parent = copyDxbAirportNode;

            bool result = laxAirportNode.Equals(copyLaxAirportNode);
            Assert.True(result);
        }
        [Fact]
        public void ShouldNotEqualsAirportNodes()
        {
            TreeNode laxAirportNode = new TreeNode(3);
            TreeNode lhrAirportNode = new TreeNode(2);
            TreeNode dxbAirportNode = new TreeNode(5);
            TreeNode sydAirportNode = new TreeNode(9);
            TreeNode cptAirportNode = new TreeNode(7);
            TreeNode hndAirportNode = new TreeNode(10);
            laxAirportNode.NextNodes = new List<TreeNode>() { lhrAirportNode, sydAirportNode, hndAirportNode };
            lhrAirportNode.NextNodes.Add(dxbAirportNode);
            dxbAirportNode.NextNodes.Add(cptAirportNode);
            lhrAirportNode.Parent = laxAirportNode;
            sydAirportNode.Parent = laxAirportNode;
            hndAirportNode.Parent = laxAirportNode;
            dxbAirportNode.Parent = lhrAirportNode;
            cptAirportNode.Parent = dxbAirportNode;

            TreeNode copyLaxAirportNode = new TreeNode(1);
            TreeNode copyLhrAirportNode = new TreeNode(2);
            TreeNode copyDxbAirportNode = new TreeNode(5);
            TreeNode copySydAirportNode = new TreeNode(9);
            TreeNode copyCptAirportNode = new TreeNode(7);
            TreeNode copyHndAirportNode = new TreeNode(10);
            copyLaxAirportNode.NextNodes = new List<TreeNode>() { copyLhrAirportNode, copySydAirportNode, copyHndAirportNode };
            copyLhrAirportNode.NextNodes.Add(dxbAirportNode);
            copyDxbAirportNode.NextNodes.Add(cptAirportNode);
            copyLhrAirportNode.Parent = copyLaxAirportNode;
            copySydAirportNode.Parent = copyLaxAirportNode;
            copyHndAirportNode.Parent = copyLaxAirportNode;
            copyDxbAirportNode.Parent = copyLhrAirportNode;
            copyCptAirportNode.Parent = copyDxbAirportNode;

            bool result = laxAirportNode.Equals(copyLaxAirportNode);
            Assert.False(result);
        }
        [Fact]
        public void ShouldBuildTreeFromCSV()
        {
            HashSet<FlightModel> flights = FlightManager.CreateFlights("CSV/TestFlights.csv");
            ArrayList flightsTree = FlightManager.ReadFlightsFromCsv("CSV/JFKRoute.csv", flights);

            TreeNode jfkAirportNode = new TreeNode(1);
            TreeNode lhrAirportNode = new TreeNode(2);
            TreeNode cdgAirportNode = new TreeNode(3);
            TreeNode fraAirportNode = new TreeNode(4);
            jfkAirportNode.NextNodes.Add(lhrAirportNode);
            lhrAirportNode.NextNodes.Add(cdgAirportNode);
            lhrAirportNode.NextNodes.Add(fraAirportNode);
            lhrAirportNode.Parent = jfkAirportNode;
            cdgAirportNode.Parent = lhrAirportNode;
            fraAirportNode.Parent = lhrAirportNode;

            FlightRouteTree flightRouteTree = new FlightRouteTree();
            TreeNode rootAirportNode = flightRouteTree.BuildTreeFromCSV(flightsTree);

            bool result = jfkAirportNode.Equals(rootAirportNode);
            Assert.True(result);
        }

        [Fact]
        public void ShouldUseDfs()
        {
            TreeNode laxAirportNode = new TreeNode(1);
            TreeNode lhrAirportNode = new TreeNode(2);
            TreeNode dxbAirportNode = new TreeNode(5);
            TreeNode sydAirportNode = new TreeNode(9);
            TreeNode cptAirportNode = new TreeNode(7);
            TreeNode hndAirportNode = new TreeNode(10);
            laxAirportNode.NextNodes = new List<TreeNode>() { lhrAirportNode, sydAirportNode, hndAirportNode };
            lhrAirportNode.NextNodes.Add(dxbAirportNode);
            dxbAirportNode.NextNodes.Add(cptAirportNode);
            lhrAirportNode.Parent = laxAirportNode;
            sydAirportNode.Parent = laxAirportNode;
            hndAirportNode.Parent = laxAirportNode;
            dxbAirportNode.Parent = lhrAirportNode;
            cptAirportNode.Parent = dxbAirportNode;

            FlightRouteTree flightRouteTree = new FlightRouteTree();

            TreeNode foundAirportNode = flightRouteTree.DFS(laxAirportNode, 7);

            bool result = cptAirportNode.Equals(foundAirportNode);
            Assert.True(result);

        }

        [Fact]
        public void ShouldFindDestinationRoute()
        {
            TreeNode laxAirportNode = new TreeNode(1);
            TreeNode lhrAirportNode = new TreeNode(2);
            TreeNode dxbAirportNode = new TreeNode(5);
            TreeNode sydAirportNode = new TreeNode(9);
            TreeNode cptAirportNode = new TreeNode(7);
            TreeNode hndAirportNode = new TreeNode(10);
            laxAirportNode.NextNodes = new List<TreeNode>() { lhrAirportNode, sydAirportNode, hndAirportNode };
            lhrAirportNode.NextNodes.Add(dxbAirportNode);
            dxbAirportNode.NextNodes.Add(cptAirportNode);
            lhrAirportNode.Parent = laxAirportNode;
            sydAirportNode.Parent = laxAirportNode;
            hndAirportNode.Parent = laxAirportNode;
            dxbAirportNode.Parent = lhrAirportNode;
            cptAirportNode.Parent = dxbAirportNode;

            List<int> destinationRoute = new List<int>() { 1, 2, 5 };
            FlightRouteTree flightRouteTree = new FlightRouteTree();
            List<int> foundRoute = flightRouteTree.FindDestinationRoute(dxbAirportNode, laxAirportNode);
            Service<int> service = new Service<int>();

            bool result = service.CompareLists(destinationRoute, foundRoute);
            Assert.True(result);
        }

        [Fact]
        public void ShouldLoadFlightTreeFromCsv()
        {
            string rootAirport = "1";
            FlightModel flight1 = new FlightModel(1, "F1", 1, 2, "Boing747", 700, 7, flightArrDateTime, flightArrDateTime); 
            FlightModel flight3 = new FlightModel(5, "F5", 2, 3, "Boing747", 150, 1, flightDepDateTime, flightArrDateTime);
            FlightModel flight4 = new FlightModel(7, "F7", 2, 4, "Boing747", 400, 2, flightDepDateTime, flightArrDateTime);

            ArrayList expFlightTree = new ArrayList();
            expFlightTree.Add(rootAirport);
            expFlightTree.Add(flight1);
            expFlightTree.Add(flight3);
            expFlightTree.Add(flight4);

            HashSet<FlightModel> flights = FlightManager.CreateFlights("CSV/TestFlights.csv");
            ArrayList flightsTree = FlightManager.ReadFlightsFromCsv("CSV/JFKRoute.csv", flights);
            Service<String> service = new Service<string>();

            bool result = service.
                CompareArrayLists(expFlightTree, flightsTree);// I passed flight to IService because of the error, this methode doesn't use generic, probably should move it to another class later.

            Assert.True(result);

        }

        [Fact]
        public void ShouldLoadGraph()
        {
            HashSet<FlightModel> flights = FlightManager.CreateFlights("CSV/TestFlights.csv");
            Dictionary<int, HashSet<FlightModel>> adjDictionary = new Dictionary<int, HashSet<FlightModel>>();

            FlightModel flight1 = new FlightModel(1, "F1", 1, 2, "Boing747", 700, 7, flightDepDateTime, flightArrDateTime);
            FlightModel flight2 = new FlightModel(2, "F2", 2, 1, "Boing747", 700, 7, flightDepDateTime, flightArrDateTime);
            FlightModel flight3 = new FlightModel(3, "F3", 1, 3, "Boing747", 1050, 8, flightDepDateTime, flightArrDateTime);
            FlightModel flight4 = new FlightModel(4, "F4", 3, 1, "Boing747", 1050, 8, flightDepDateTime, flightArrDateTime);
            FlightModel flight5 = new FlightModel(5, "F5", 2, 3, "Boing747", 150, 1, flightDepDateTime, flightArrDateTime);
            FlightModel flight6 = new FlightModel(6, "F6", 3, 2, "Boing747", 150, 1, flightDepDateTime, flightArrDateTime);
            FlightModel flight7 = new FlightModel(7, "F7", 2, 4, "Boing747", 400, 2, flightDepDateTime, flightArrDateTime);
            FlightModel flight8 = new FlightModel(8, "F8", 4, 2, "Boing747", 400, 2, flightDepDateTime, flightArrDateTime);
            FlightModel flight9 = new FlightModel(9, "F9", 4, 3, "Boing747", 200, 1, flightDepDateTime, flightArrDateTime);
            FlightModel flight10 = new FlightModel(10, "F10", 3, 4, "Boing747", 200, 1, flightDepDateTime, flightArrDateTime);


            adjDictionary[1] = new HashSet<FlightModel>() { flight1, flight3 };
            adjDictionary[2] = new HashSet<FlightModel>() { flight2, flight5, flight7 };
            adjDictionary[3] = new HashSet<FlightModel>() { flight4,flight6,flight10 };
            adjDictionary[4] = new HashSet<FlightModel>() { flight8,flight9 };

            GraphAirport graph = new GraphAirport();
            graph.LoadGraph(flights);

            bool result = graph.CompareGraph(adjDictionary);
            Assert.True(result);
        }

        [Fact]
        public void ShouldNotLoadGraph()
        {
            HashSet<FlightModel> flights = FlightManager.CreateFlights("CSV/TestFlights.csv");
            Dictionary<int, HashSet<FlightModel>> adjDictionary = new Dictionary<int, HashSet<FlightModel>>();

            FlightModel flight1 = new FlightModel(1, "F1", 1, 2, "Boing747", 700, 7, flightArrDateTime, flightArrDateTime);
            FlightModel flight3 = new FlightModel(3, "F3", 1, 3, "Boing747", 1050, 8, flightDepDateTime, flightArrDateTime);
            FlightModel flight4 = new FlightModel(4, "F4", 3, 1, "Boing747", 1050, 8, flightDepDateTime, flightArrDateTime);
            FlightModel flight5 = new FlightModel(5, "F5", 2, 3, "Boing747", 150, 1, flightDepDateTime, flightArrDateTime);
            FlightModel flight6 = new FlightModel(6, "F6", 3, 2, "Boing747", 150, 1, flightDepDateTime, flightArrDateTime);
            FlightModel flight7 = new FlightModel(7, "F7", 2, 4, "Boing747", 400, 2, flightDepDateTime, flightArrDateTime);
            FlightModel flight8 = new FlightModel(8, "F8", 4, 2, "Boing747", 400, 2, flightDepDateTime, flightArrDateTime);
            FlightModel flight9 = new FlightModel(9, "F9", 4, 3, "Boing747", 200, 1, flightDepDateTime, flightArrDateTime);
            FlightModel flight10 = new FlightModel(10, "F10", 3, 4, "Boing747", 200, 1, flightDepDateTime, flightArrDateTime);

            adjDictionary[1] = new HashSet<FlightModel>() { flight1, flight6 };
            adjDictionary[5] = new HashSet<FlightModel>() { flight3, flight4 };
            adjDictionary[3] = new HashSet<FlightModel>() { flight6, flight3, flight5 };
            adjDictionary[4] = new HashSet<FlightModel>() { flight4, flight5 };


            GraphAirport graph = new GraphAirport();
            graph.LoadGraph(flights);

            bool result = graph.CompareGraph(adjDictionary);
            Assert.False(result);
        }

        [Fact]
        public void ShouldBeConnected()
        {
            HashSet<FlightModel> flights = FlightManager.CreateFlights("CSV/TestFlights.csv");
            GraphAirport graph = new GraphAirport();
            graph.LoadGraph(flights);

            bool result = graph.IsConnected(2, 4);

            Assert.True(result);
        }

        [Fact]
        public void ShouldNotBeConnected()
        {
            HashSet<FlightModel> flights = FlightManager.CreateFlights("CSV/TestFlights.csv");
            GraphAirport graph = new GraphAirport();
            graph.LoadGraph(flights);

            bool result = graph.IsConnected(1, 8);

            Assert.False(result);
        }

        [Fact]
        public void ShouldBfs()
        {
            HashSet<FlightModel> flights = FlightManager.CreateFlights("CSV/TestFlights.csv");
            GraphAirport graph = new GraphAirport();
            graph.LoadGraph(flights);

            List<int> path = new List<int>() { 1,2,4 };

            List<int> graphPath = graph.Bfs(1, 4);

            bool result = path.SequenceEqual(graphPath);

            Assert.True(result);
        }

        [Fact]
        public void ShouldNotBfs()
        {
            HashSet<FlightModel> flights = FlightManager.CreateFlights("CSV/TestFlights.csv");
            GraphAirport graph = new GraphAirport();
            graph.LoadGraph(flights);

            List<int> path = new List<int>() { 1, 4, 2 };

            List<int> graphPath = graph.Bfs(1, 2);

            bool result = path.SequenceEqual(graphPath);


            Assert.False(result);
        }

        [Fact]
        public void ShouldThrowErrorBfs()
        {
            GraphAirport graph = new GraphAirport();

            List<int> path = new List<int>() { 1,10,5 };
            List<int> graphPath = new List<int>();

            Assert.Throws<InvalidRouteException>(() => graphPath = graph.Bfs(1, 5));
        }

        [Fact]
        public void ShouldFindNearestPathCheapAlgorithm()
        {
            List<int> expectedPath = new List<int>() { 1,2,3,4};
            HashSet<FlightModel> flights = FlightManager.CreateFlights("CSV/TestFlights.csv");
            Dictionary<int, List<FlightModel>> adjDictionary = new Dictionary<int, List<FlightModel>>();
            GraphAirport graph = new GraphAirport();
            graph.LoadGraph(flights);

            CheapAlgorithm cheapAlgorithm = new CheapAlgorithm(1,4);
            cheapAlgorithm.Execute(graph.AdjacencyList);

            List<int> nearestPath = graph.FindNearestPath(1, 4, cheapAlgorithm);
            Service<int> service = new Service<int>();

            bool result = service.CompareLists(expectedPath, nearestPath);

            Assert.True(result);
        }

        [Fact]
        public void ShouldFindNearestPathStopAlgorithm()
        {
            List<int> expectedPath = new List<int>() { 1,2,4};
            HashSet<FlightModel> flights = FlightManager.CreateFlights("CSV/TestFlights.csv");
            Dictionary<int, List<FlightModel>> adjDictionary = new Dictionary<int, List<FlightModel>>();
            GraphAirport graph = new GraphAirport();
            graph.LoadGraph(flights);

            StopAlgorithm stopAlgorithm = new StopAlgorithm(1,4);
            stopAlgorithm.Execute(graph.AdjacencyList);

            List<int> nearestPath = graph.FindNearestPath(1,4, stopAlgorithm);
            Service<int> service = new Service<int>();

            bool result = service.CompareLists(expectedPath, nearestPath);

            Assert.True(result);
        }

        [Fact]
        public void ShouldFindNearestPathTimeAlgorithm()
        {
            List<int> expectedPath = new List<int>() { 1,2,4};
            HashSet<FlightModel> flights = FlightManager.CreateFlights("CSV/TestFlights.csv");
            Dictionary<int, List<FlightModel>> adjDictionary = new Dictionary<int, List<FlightModel>>();
            GraphAirport graph = new GraphAirport();
            graph.LoadGraph(flights);

            TimeAlgorithm timeAlgorithm = new TimeAlgorithm(1,4);
            timeAlgorithm.Execute(graph.AdjacencyList);

            List<int> nearestPath = graph.FindNearestPath(1,4, timeAlgorithm);
            Service<int> service = new Service<int>();

            bool result = service.CompareLists(expectedPath, nearestPath);

            Assert.True(result);
        }

        [Fact]
        public void ShouldThrowExceptionNearestPathCheapAlgorithm()
        {
            List<int> expectedPath = new List<int>() { 1,2,3,4 };
            HashSet<FlightModel> flights = FlightManager.CreateFlights("CSV/TestFlights.csv");
            Dictionary<int, List<FlightModel>> adjDictionary = new Dictionary<int, List<FlightModel>>();
            GraphAirport graph = new GraphAirport();

            CheapAlgorithm cheapAlgorithm = new CheapAlgorithm(1,4);


            Assert.Throws<InvalidFlightException>(() => cheapAlgorithm.Execute(graph.AdjacencyList));
        }

        [Fact]
        public void ShouldThrowExceptionPathStopAlgorithm()
        {
            List<int> expectedPath = new List<int>() { 1,2,3,4 };
            HashSet<FlightModel> flights = FlightManager.CreateFlights("CSV/TestFlights.csv");
            Dictionary<int, List<FlightModel>> adjDictionary = new Dictionary<int, List<FlightModel>>();
            GraphAirport graph = new GraphAirport();

            StopAlgorithm stopAlgorithm = new StopAlgorithm(1,4);

            Assert.Throws<InvalidFlightException>(() => stopAlgorithm.Execute(graph.AdjacencyList));
        }

        [Fact]
        public void ShouldThrowExceptionNearestPathTimeAlgorithm()
        {
            List<int> expectedPath = new List<int>() { 1, 2, 4 };
            HashSet<FlightModel> flights = FlightManager.CreateFlights("CSV/TestFlights.csv");
            Dictionary<int, List<FlightModel>> adjDictionary = new Dictionary<int, List<FlightModel>>();
            GraphAirport graph = new GraphAirport();
           

            TimeAlgorithm timeAlgorithm = new TimeAlgorithm(1, 4);

            Assert.Throws<InvalidFlightException>(() => timeAlgorithm.Execute(graph.AdjacencyList));
        }
    }
}