﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Airlines.Bussines.Sort;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic;
using Airlines.Persistance.Basic.EntitiesService;
using RiseAirlines.Persistance.Basic.EntitiesService;
using RiseAirlines.Persistance.Basic.Exceptions;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Airlines.UnitTest
{
    [Collection("Sequental")]
    public class SearchAndSortTests
    {
        private static DateTime flightDepDateTime = new DateTime(2024, 4, 21, 14, 30, 0);
        private static DateTime flightArrDateTime = new DateTime(2024, 4, 21, 22, 0, 0);
        private static DateOnly founded = new DateOnly(1967, 5, 28);

        [Fact]
        public static void ShouldAirportCountrySearch()
        {

            HashSet<AirportModel> airports = AirportsManager.
                 CreateAirports("CSV/TestAirports.csv");

            string airportName = "John F Kennedy International Airport";

            IEnumerable<AirportModel> airportsCountries = Search.AirportCountrySearch("United States", airports);
            bool result = airportsCountries.Any(airport => airport.Name == airportName);
            Assert.True(result);
        }

        [Fact]
        public static void ShouldNotAirportCountrySearch()
        {
            HashSet<AirportModel> airports = AirportsManager.
                 CreateAirports("CSV/TestAirports.csv");

            string expectedOutput = "No airports found!";

            string errorMessage = "false";
            try
            {
                Search.AirportCountrySearch("Bulgaria", airports);
            }
            catch (InvalidAirportException ex)
            {
                errorMessage = ex.Message;
            }
            Assert.Equal(expectedOutput, errorMessage);

        }

        [Fact]
        public static void ShouldAirportCitySearch()
        {
            HashSet<AirportModel> airports = AirportsManager.
                 CreateAirports("CSV/TestAirports.csv");

            string airportName = "John F Kennedy International Airport";

            IEnumerable<AirportModel> airportsCountries = Search.AirportCitySearch("New York", airports);
            bool result = airportsCountries.Any(airport => airport.Name == airportName);
            Assert.True(result);

        }

        [Fact]
        public static void ShouldNotAirportCitySearch()
        {
            HashSet<AirportModel> airports = AirportsManager.
                 CreateAirports("CSV/TestAirports.csv");

            string expectedOutput = "No airports found!";

            string errorMessage = "false";
            try
            {
                Search.AirportCitySearch("Bulgaria", airports);
            }
            catch (InvalidAirportException ex)
            {
                errorMessage = ex.Message;
            }
            Assert.Equal(expectedOutput, errorMessage);


        }

        [Fact]
        public static void ShouldAirlineSearch()
        {
            HashSet<AirlineModel> airlines = AirlineManager.
                CreateAirlines("CSV/TestAirlines.csv");

            bool expOutput = true;


            bool result = Search.AirlineSearch("AA", airlines);

            Assert.Equal(expOutput, result);
        }

        [Fact]
        public static void ShouldNotAirlineSearch()
        {
            HashSet<AirlineModel> airlines = AirlineManager.
                CreateAirlines("CSV/TestAirlines.csv");

            bool expOutput = false;


            bool result = Search.AirlineSearch("American Express", airlines);

            Assert.Equal(expOutput, result);

        }

       

        [Fact]
        public static void ShouldFlightSearch()
        {
            HashSet<FlightModel> flights = FlightManager.
                 CreateFlights("CSV/TestFlights.csv");

            FlightModel expectedFlight = new FlightModel(5, "F5", 2, 3, "Boing747", 150, 1, flightDepDateTime, flightArrDateTime);

            FlightModel flight = Search.FlightSearch("F5", flights);

            bool result = flight.Equals(expectedFlight);

            Assert.True(result);
        }

        [Fact]
        public static void ShouldNotFlightSearch()
        {
            HashSet<FlightModel> flights = FlightManager.
                 CreateFlights("CSV/TestFlights.csv");

            FlightModel expectedFlight = new FlightModel(5, "F5", 2, 3, "Boing747", 150, 1, flightDepDateTime, flightArrDateTime);

            FlightModel flight = Search.FlightSearch("F1", flights);

            bool result = flight.Equals(expectedFlight);

            Assert.False(result);
        }

        [Fact]
        public static void ShouldEmptyFlightSearch()
        {
            HashSet<FlightModel> flights = FlightManager.
                 CreateFlights("CSV/TestFlights.csv");

            Assert.Throws<InvalidFlightException>(() => Search.FlightSearch("F16", flights));
        }

        [Fact]
        public void ShouldBubbleSortAirportById()
        {
            List<AirportModel> expectedAirports = new List<AirportModel>();
            AirportModel airport1 = new AirportModel(1, "AAA", "John F Kennedy International Airport", "New York", "United States", 3, founded);
            AirportModel airport2 = new AirportModel(2, "BBB", "Los Angeles International Airport", "Los Angeles", "United States", 4, founded);
            AirportModel airport3 = new AirportModel(3, "CCC", "OHare International Airport", "Chicago", "United States", 4, founded);
            AirportModel airport4 = new AirportModel(4, "DDD", "HartsfieldJackson Atlanta International Airport", "Atlanta", "United States", 3, founded);
            expectedAirports.Add(airport4);
            expectedAirports.Add(airport3);
            expectedAirports.Add(airport2);
            expectedAirports.Add(airport1);

            HashSet<AirportModel> airports = AirportsManager.
                CreateAirports("CSV/TestAirports.csv");
            List<AirportModel> airportsList = new List<AirportModel>(airports);
            Sort.BubbleSortById(airportsList, OrderSort.Descending);
            Service<AirportModel> service = new Service<AirportModel>();

            Assert.True(service.CompareLists(expectedAirports, airportsList));


        }

        [Fact]
        public void ShouldBubbleSortAirportByName()
        {
            List<AirportModel> expectedAirports = new List<AirportModel>();
            AirportModel airport1 = new AirportModel(1, "AAA", "John F Kennedy International Airport", "New York", "United States", 3, founded);
            AirportModel airport2 = new AirportModel(2, "BBB", "Los Angeles International Airport", "Los Angeles", "United States", 4, founded);
            AirportModel airport3 = new AirportModel(3, "CCC", "OHare International Airport", "Chicago", "United States", 4, founded);
            AirportModel airport4 = new AirportModel(4, "DDD", "HartsfieldJackson Atlanta International Airport", "Atlanta", "United States", 3, founded);
            expectedAirports.Add(airport4);
            expectedAirports.Add(airport1);
            expectedAirports.Add(airport2);
            expectedAirports.Add(airport3);

            HashSet<AirportModel> airports = AirportsManager.
                CreateAirports("CSV/TestAirports.csv");
            List<AirportModel> airportsList = new List<AirportModel>(airports);
            Sort.BubbleSortByName(airportsList, OrderSort.Ascending);
            Service<AirportModel> service = new Service<AirportModel>();

            Assert.True(service.CompareLists(expectedAirports, airportsList));

        }




        [Fact]
        public void ShouldBubbleSortAirlineById()
        {
            List<AirlineModel> expectedAirlines = new List<AirlineModel>();
            AirlineModel airline1 = new AirlineModel(1, "AA", founded, 10);
            AirlineModel airline2 = new AirlineModel(2, "UA", founded, 10);
            AirlineModel airline3 = new AirlineModel(3, "DL", founded, 10);
            AirlineModel airline4 = new AirlineModel(4, "BA", founded, 10);
            expectedAirlines.Add(airline1);
            expectedAirlines.Add(airline4);
            expectedAirlines.Add(airline3);
            expectedAirlines.Add(airline2);

            HashSet<AirlineModel> airlines = AirlineManager.
                CreateAirlines("CSV/TestAirlines.csv");
            List<AirlineModel> airlinesList = new List<AirlineModel>(airlines);
            Sort.BubbleSortById(airlinesList, OrderSort.Ascending);
            Service<AirlineModel> service = new Service<AirlineModel>();

            Assert.True(service.CompareLists(expectedAirlines, airlinesList));

        }

        [Fact]
        public void ShouldBubbleSortAirlineByName()
        {
            List<AirlineModel> expectedAirlines = new List<AirlineModel>();
            AirlineModel airline1 = new AirlineModel(1, "AA", founded, 10);
            AirlineModel airline2 = new AirlineModel(2, "UA", founded, 10);
            AirlineModel airline3 = new AirlineModel(3, "DL", founded, 10);
            AirlineModel airline4 = new AirlineModel(4, "BA", founded, 10);
            expectedAirlines.Add(airline1);
            expectedAirlines.Add(airline4);
            expectedAirlines.Add(airline3);
            expectedAirlines.Add(airline2);

            HashSet<AirlineModel> airlines = AirlineManager.
                CreateAirlines("CSV/TestAirlines.csv");
            List<AirlineModel> airlinesList = new List<AirlineModel>(airlines);
            Sort.BubbleSortByName(airlinesList, OrderSort.Ascending);
            Service<AirlineModel> service = new Service<AirlineModel>();

            Assert.True(service.CompareLists(expectedAirlines, airlinesList));

        }
    }
}
