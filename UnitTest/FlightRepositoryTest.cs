﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Entities;
using Airlines.Persistance.Basic.EntitiesService;
using Airlines.Persistance.Basic.Mapper;
using Airlines.Persistance.Basic.Repository;
using Microsoft.EntityFrameworkCore;
using Moq;
using Moq.EntityFrameworkCore;

namespace Airlines.UnitTest
{
    public class FlightRepositoryTest
    {
        private static DateTime flightDepDateTime = new DateTime(2024, 6, 1, 08, 00, 00);
        private static DateTime flightArrDateTime = new DateTime(2024, 6, 01, 15, 00, 00);
        private static DateOnly founded = new DateOnly(1967, 5, 28);

        private Mock<DB_Context> contextMock;
        private FlightRepository flightRepo;
        private FlightMapper flightMapper;


        public FlightRepositoryTest()
        { 
            flightMapper = new FlightMapper();
            contextMock = new Mock<DB_Context>();
            flightRepo = new FlightRepository(contextMock.Object);
        }

        //[Fact]
        //public void GetAllFlights()
        //{
        //    List<Flight> flights = new List<Flight>();
        //    FlightModel flight1 = new FlightModel(1, "F1", 1, 2, "Boing747", 400, 7, flightDepDateTime, flightArrDateTime);
        //    FlightModel flight2 = new FlightModel(2, "F2", 2, 1, "Boing747", 400, 7, flightDepDateTime, flightArrDateTime);
        //    flights.Add(flightMapper.MapToFlight(flight1));
        //    flights.Add(flightMapper.MapToFlight(flight2));

        //    List<Flight> expectedFlights = flights;

        //    contextMock.Setup(context => context.Flights).ReturnsDbSet(flights);

        //    List<Flight> allFlights = flightRepo.GetFlights();

        //    Service<Flight> service = new Service<Flight>();
        //    bool result = service.CompareLists(expectedFlights, allFlights);

        //    Assert.True(result);
        //}

        //[Fact]
        //public void GetFlightsByFilter()
        //{
        //    FlightModel flightModel1 = new FlightModel(1, "F1", 1, 2, "Boing747", 400, 7, flightDepDateTime, flightArrDateTime);
        //    Flight flight1 = flightMapper.MapToFlight(flightModel1);

        //    List<Flight> expectedFlights = new List<Flight>();
        //    expectedFlights.Add(flight1);

        //    contextMock.Setup(context => context.Flights).ReturnsDbSet(expectedFlights);

        //    List<Flight> flights = flightRepo.GetFlightsByFilter("FlightNumber", "F1");

        //    Service<Flight> service = new Service<Flight>();
        //    bool result = service.CompareLists(expectedFlights, flights);

        //    Assert.True(result);
        //}

    }
}
