using Airlines.Bussines.BussinesValidation;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using Newtonsoft.Json.Bson;
using Xunit.Abstractions;

namespace Airlines.UnitTest
{
    public class ValidationTests
    {
        [Fact]
        public void HasOnlyLetters()
        {
            string name = "word";

            bool isValid = Validation.HasOnlyLetters(name);

            Assert.True(isValid);
        }

        [Fact]
        public void HasNotOnlyLetters()
        {
            string name = "word12";

            bool isValid = Validation.HasOnlyLetters(name);

            Assert.False(isValid);
        }

        [Fact]
        public void AreAlphaNumeric()
        {
            string name = "word12";

            bool isValid = Validation.IsAlphaNumeric(name);

            Assert.True(isValid);
        }

        [Fact]
        public void AreNotAlphaNumeric()
        {
            string name = "word-12";

            bool isValid = Validation.IsAlphaNumeric(name);

            Assert.False(isValid);
        }

        [Fact]
        public void IsUnique()
        {

            List<string> airports = ["bnb", "rya"];
            string airport = "aba";

            bool isValid = Validation.UniqueValidation(airports, airport);

            Assert.True(isValid);
        }

        [Fact]
        public void IsNotUnique()
        {

            List<string> airports = ["aba", "bnb", "rya"];
            string airport = "aba";

            bool isValid = Validation.UniqueValidation(airports, airport);

            Assert.False(isValid);
        }

        [Fact]
        public void ShouldvalidateAirport()
        {
            
            string airport = "aba";

            bool isvalid = Validation.ValidateAirport(airport);

            Assert.True(isvalid);
        }

        [Fact]
        public void ShouldNotValidateAirport()
        {
            
            string airport = "abba";

            bool isvalid = Validation.ValidateAirport(airport);

            Assert.False(isvalid);
        }

        [Fact]
        public void ShouldValidateAirline()
        {
            
            string airline = "eSky";

            bool isValid = Validation.ValidateAirline(airline);

            Assert.True(isValid);
        }

        [Fact]
        public void ShouldNotValidateAirline()
        {
            string airline = "turkishAirlines";

            bool isValid = Validation.ValidateAirline(airline);

            Assert.False(isValid);
        }

        [Fact]
        public void ShouldValidateFlight()
        {
            string flight = "Nightfall808";

            bool isValid = Validation.ValidateFlight(flight);

            Assert.True(isValid);
        }

        [Fact]
        public void ShouldNotValidateFlight()
        {
            string flight = "Nightfall-808";

            bool isValid = Validation.ValidateFlight(flight);

            Assert.False(isValid);
        }
    }
}
