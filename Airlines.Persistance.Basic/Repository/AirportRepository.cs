﻿using Airlines.Persistance.Basic.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Persistance.Basic.Repository
{
    public class AirportRepository : IAirportRepository
    {

        private readonly DB_Context dbContext;

        public AirportRepository(DB_Context db_Context)
        {
            dbContext = db_Context;
        }

        public int GetCount()
        {
            return dbContext.Airports.Count();
        }

        public List<Airport> GetAirports()
        {
            List<Airport> response = dbContext.Airports.ToList();
            return response;
        }
        public List<Airport> GetAirportsByFilter(string filter, string value)
        {
            IQueryable<Airport> response = dbContext.Airports.Where(airport => EF.Functions.Like(EF.Property<string>(airport, filter), value));
            return response.ToList();
        }

        public bool AddAirport(Airport airport)
        {
            if (airport == null)
            {
                return false;
            }
            dbContext.Airports.Add(airport);
            dbContext.SaveChanges();
            return true;
        }

        public bool UpdateAirport(int id, Airport newAirport)
        {

            Airport airport = dbContext.Airports.Where(airport => airport.Id == id).FirstOrDefault()!;
            if (airport == null)
            {
                return false;
            }
            airport.Name = newAirport.Name;
            airport.Country = newAirport.Country;
            airport.City = newAirport.City;
            airport.Code = newAirport.Code;
            airport.RunwaysCount = newAirport.RunwaysCount;
            airport.Founded = newAirport.Founded;

            dbContext.Update(airport);
            dbContext.SaveChanges();
            return true;

        }


        public bool UpdateAirportName(string id, string name)
        {
            Airport airport = dbContext.Airports.FirstOrDefault(airport => airport.Id.Equals(id))!;
            if(airport == null)
            {
                return false;
            }
            airport.Name = name;
            dbContext.SaveChanges();
            return true;
        }

        public bool UpdateAirportFilter(string filter, string value)
        {
            List<Airport> airports = dbContext.Airports.Where(airport => EF.Property<string>(airport, filter).Equals(value)).ToList();
            if(airports.Count < 1 )
            {
                return false;
            }
            foreach (Airport airport in airports)
            {
                PropertyInfo property = typeof(Airport).GetProperty(filter)!;
                property.SetValue(airport, value);
            }
            dbContext.SaveChanges();
            return true;
        }

        public bool DeleteAirportByFilter(string filter, string value)
        {
            List<Airport> deleteAirports = dbContext.Airports.Where(airport => EF.Property<string>(airport, filter).Equals(value)).ToList();
            if(deleteAirports.Count < 1)
            {
                return false;
            }
            foreach (Airport airport in deleteAirports)
            {
                dbContext.Airports.Remove(airport);            
            }
            dbContext.SaveChanges();
            return true;
        }
    }
}
