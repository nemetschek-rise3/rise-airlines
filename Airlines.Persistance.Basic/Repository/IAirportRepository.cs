﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Airlines.Persistance.Basic.Entities;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistance.Basic.Repository
{
    public interface IAirportRepository
    {
        public int GetCount();
        public List<Airport> GetAirports();
        public List<Airport> GetAirportsByFilter(string filter, string value);
        public bool AddAirport(Airport airport);
        public bool UpdateAirport(int id, Airport airport);
        public bool DeleteAirportByFilter(string filter, string value);
    }
}
