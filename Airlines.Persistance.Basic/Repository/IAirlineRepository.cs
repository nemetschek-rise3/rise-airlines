﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Persistance.Basic.Entities;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistance.Basic.Repository
{
    public interface IAirlineRepository
    {
        public int GetCount();
        public List<Airline> GetAirlines();
        public List<Airline> GetAirlinesByFilter(string filter, string value);
        public bool AddAirline(Airline airline);
        public bool UpdateAirline(int id, Airline airline);
        //public void UpdateAirlineByFilter(int id, string filter, string value);
        public bool DeleteAirlineByFilter(string filter, string value);
    }
}
