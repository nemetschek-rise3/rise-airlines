﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices.JavaScript;
using System.Text;
using System.Threading.Tasks;
using Airlines.Persistance.Basic.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using DB_Context = Airlines.Persistance.Basic.Entities.DB_Context;

namespace Airlines.Persistance.Basic.Repository
{
    public class FlightRepository : IFlightRepository
    {

        private readonly DB_Context dbContext;
        public FlightRepository(DB_Context db_Context)
        {
            dbContext = db_Context;
        }

        public int GetCount()
        {
            return dbContext.Flights.Count();
        }

        public List<Flight> GetFlights()
        {
            List<Flight> response = dbContext.Flights.ToList();
            return response;
        }

        public List<Flight> GetFlightsByFilter(string filter, string value) 
        {
            List<Flight> response = dbContext.Flights.Where(flight => EF.Functions.Like(EF.Property<string>(flight, filter), value)).ToList();
            return response;
        }

        public bool AddFlight(Flight flight)
        {
            if(flight == null)
            {
                return false;
            }
            dbContext.Flights.Add(flight);
            dbContext.SaveChanges();
            return true;
        }

        public bool UpdateFlight(int id, Flight newFlight)
        {
            Flight flight = dbContext.Flights.FirstOrDefault(f => f.Id == id)!;

            if (flight != null)
            {
                flight.Code = newFlight.Code ?? flight.Code;
                flight.DepartureAirportId = newFlight.DepartureAirportId ?? flight.DepartureAirportId;
                flight.ArrivalAirportId = newFlight.ArrivalAirportId ?? flight.ArrivalAirportId;
                flight.DepartureDateTime = newFlight.DepartureDateTime ?? flight.DepartureDateTime;
                flight.ArrivalDateTime = newFlight.ArrivalDateTime ?? flight.ArrivalDateTime;
                flight.Price = newFlight.Price ?? flight.Price;
                flight.TimeHours = newFlight.TimeHours ?? flight.TimeHours;

                dbContext.SaveChanges();
                return true;
            }
            return false;
        }

        public bool UpdateFlightFilter(string filter, string value)
        {
            List<Flight> flights = dbContext.Flights.Where(flight => EF.Property<string>(flight, filter).Equals(value)).ToList();
            if(flights.Count < 1)
            {
                return false;
            }
            foreach (Flight flight in flights)
            {
                PropertyInfo property = typeof(Flight).GetProperty(filter)!;
                property.SetValue(flight, value);
            }
            dbContext.SaveChanges();
            return true;
        }

        public bool UpdateFlightDepartureTime(string id, DateTime departureTime)
        {
            Flight? flight = dbContext.Flights.FirstOrDefault(flight => flight.Id.Equals(id));
            if (flight != null)
            {
                flight.DepartureDateTime = departureTime;
                dbContext.SaveChanges();
                return true;
            }
            return false;
        }

        public bool UpdateFlightArrivalTime(string id, DateTime departureTime)
        {
            Flight? flight = dbContext.Flights.FirstOrDefault(flight => flight.Id.Equals(id));
            if (flight != null)
            {
                flight.ArrivalDateTime = departureTime;
                dbContext.SaveChanges();
                return true;
            }
            return false;
        }

        public bool DeleteFlightByFilter(string filter, string value)
        {
            List<Flight> deleteFlights = dbContext.Flights.Where(flight => EF.Property<string>(flight, filter).Equals(value)).ToList();
            if (deleteFlights == null)
            {
                return false;
            }
            foreach (Flight flight in deleteFlights)
            {
                dbContext.Flights.Remove(flight);
              
            }
            dbContext.SaveChanges();
            return true;
        }
    }
}
