﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Persistance.Basic.Entities;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistance.Basic.Repository
{
    public interface IFlightRepository
    {
        public int GetCount();
        public List<Flight> GetFlights();
        public List<Flight> GetFlightsByFilter(string filter, string value);
        public bool AddFlight(Flight flight);
        public bool UpdateFlight(int id, Flight flight);
        public bool UpdateFlightDepartureTime(string id, DateTime departureTime);
        public bool UpdateFlightArrivalTime(string id, DateTime departureTime);
        public bool DeleteFlightByFilter(string filter, string value);
    }
}
