﻿using Airlines.Persistance.Basic.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Persistance.Basic.Repository
{
    public class AirlineRepository : IAirlineRepository { 

        public AirlineRepository(DB_Context db_Context)
        {
            dbContext = db_Context;
        }
        private readonly DB_Context dbContext;

        public int GetCount()
        {
            return dbContext.Airlines.Count();
        }

        public List<Airline> GetAirlines()
        {
            List<Airline> response = dbContext.Airlines.ToList();
            return response;
        }

        public List<Airline> GetAirlinesByFilter(string filter, string value)
        {
            List<Airline> response = dbContext.Airlines.Where(airline => EF.Functions.Like(EF.Property<string>(airline, filter), value)).ToList();
            return response;
        }

        public bool AddAirline(Airline airline)
        {
            if(airline == null)
            {
                return false;
            }

            dbContext.Airlines.Add(airline);
            dbContext.SaveChanges();
            return true;
        }

        public bool UpdateAirline(int id, Airline newAirline)
        {

            Airline airline = dbContext.Airlines.Where(airline => airline.Id == id).FirstOrDefault()!;
            if(airline == null)
            {
                return false;
            }

            airline.Name = newAirline.Name;
            airline.FleetSize = newAirline.FleetSize;
            airline.Description = newAirline.Description;
            airline.Founded = newAirline.Founded;

            dbContext.Update(airline);
            dbContext.SaveChanges();
            return true;
        }


        public bool DeleteAirlineByFilter(string filter, string value)
        {
            List<Airline> deleteAirlines = dbContext.Airlines.Where(airline => EF.Property<string>(airline, filter).Equals(value)).ToList();
            if(deleteAirlines.Count < 1)
            {
                return false;
            }
            foreach (Airline airline in deleteAirlines)  
            {
                dbContext.Airlines.Remove(airline);             
            }
            dbContext.SaveChanges();
            return true;
        }
    }
}
