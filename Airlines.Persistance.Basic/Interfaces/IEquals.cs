﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Persistance.Basic.Interfaces
{
    internal interface IEquals
    {
        bool Equals(object obj);
    }
}
