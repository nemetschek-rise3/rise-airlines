﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Persistance.Basic.Interfaces
{
    public interface IdItem
    {
        int Id { get; }
    }
}
