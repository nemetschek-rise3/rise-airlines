﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Persistance.Basic.Exceptions;
public class InvalidAirlineException : Exception
{
    public InvalidAirlineException(string message) : base(message) { }
}
