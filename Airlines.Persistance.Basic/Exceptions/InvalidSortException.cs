﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Persistance.Basic.Exceptions;
public class InvalidSortException : Exception
{
    public InvalidSortException(string message) : base(message) { }
}
