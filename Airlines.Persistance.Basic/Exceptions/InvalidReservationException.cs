﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Persistance.Basic.Exceptions;
public class InvalidReservationException : Exception
{
    public InvalidReservationException(string message) : base(message) { }
}
