﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Persistance.Basic.Enums
{
    public enum FlightProperties
    {
        FlightId,
        FlightNumber,
        DepartureAirportId,
        ArrivalAirportId,
        DepartureDateTime,
        ArrivalDateTime,
        Price,
        TimeHours
    }
}
