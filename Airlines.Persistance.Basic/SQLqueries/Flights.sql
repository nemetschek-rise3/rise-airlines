CREATE TABLE Flights
(
FlightId INT PRIMARY KEY,
FlightNumber VARCHAR(10),
DepartureAirportId INT,
ArrivalAirportId INT,
DepartureDateTime DATETIME,
ArrivalDateTime DATETIME,
Price INT,
TimeHours INT

FOREIGN KEY (DepartureAirportId) REFERENCES Airports(AirportId),
FOREIGN KEY (ArrivalAirportId) REFERENCES Airports(AirportId),
CONSTRAINT CHK_Departure_DateTime CHECK (DepartureDateTime > GETDATE()),
CONSTRAINT CHK_Arrival_DateTime CHECK (ArrivalDateTime > DepartureDateTime)
);
