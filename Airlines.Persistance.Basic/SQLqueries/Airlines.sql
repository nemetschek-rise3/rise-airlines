CREATE TABLE Airlines
(
AirlineId INT PRIMARY KEY,
Name VARCHAR(255),
Founded DATE,
FleetSize INT,
Description VARCHAR(255)
);