INSERT INTO Airports(AirportID, Name, Country, City, Code, RunwaysCount, Founded)
VALUES
(1,'Los Angeles International Airport','United States','Los Angeles','LAX',3,'1937-10-16'),
(2,'John F Kennedy International Airport','United States','New York','JFK',4,'1961-01-15'),
(3,'Heathrow Airport','UnitedKingdom','London','LHR',3,'1946-05-31');

INSERT INTO Airlines (AirlineID, Name, Founded, FleetSize, Description)
VALUES
(1,'Bulgaria Air','2002-12-04',5,'National flag carrier airline of Bulgaria'),
(2,'Alitalia','1946-09-16',12,'Flag carrier airline of Italy');

INSERT INTO Flights (FlightId, FlightNumber, DepartureAirportId,ArrivalAirportId, DepartureDateTime, ArrivalDateTime, Price, TimeHours)
VALUES
(1,'F1',1,2,'2024-04-17 10:00:00', '2024-04-17 16:00:00', 300, 6),
(2,'F2',2,3,'2024-04-17 20:00:00', '2024-04-18 03:00:00', 700, 7);