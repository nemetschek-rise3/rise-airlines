SELECT dep.Name, dep.Country, dep.City, dep.Code, dep.RunwaysCount, dep.Founded,
	f.FlightNumber, f.DepartureDateTime, f.ArrivalDateTime
    FROM airports dep
    JOIN flights f ON dep.AirportID = f.DepartureAirportId
    WHERE CONVERT(date, f.DepartureDateTime) = CONVERT(date, DATEADD(day, 1, GETDATE()))
	UNION
SELECT arr.Name, arr.Country, arr.City, arr.Code, arr.RunwaysCount, arr.Founded,
	f.FlightNumber, f.DepartureDateTime, f.ArrivalDateTime
    FROM airports arr
    JOIN flights f ON arr.AirportID = f.ArrivalAirportId
    WHERE DATEDIFF(DAY, GETDATE(), f.DepartureDateTime) = 1

    SELECT Count(FlightId) as DepartedFlights
FROM Flights f WHERE DepartureDateTime < GETDATE();


SELECT * FROM flights WHERE TimeHours = (SELECT MAX(TimeHours) FROM flights)