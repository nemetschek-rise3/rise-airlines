CREATE INDEX IX_Airports_Code ON Airports(Code);
CREATE INDEX IX_Flights_DepartureAirport ON Flights(DepartureAirportId);
CREATE INDEX IX_Flights_ArrivalAirport ON Flights(ArrivalAirportId);
CREATE INDEX IX_Airlines_Name ON Airlines(Name);