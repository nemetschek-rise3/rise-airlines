CREATE TABLE Airports
(
AirportId INT PRIMARY KEY,
Name VARCHAR(255),
Country VARCHAR(255),
City VARCHAR(255),
Code VARCHAR(3),
RunwaysCount INT,
Founded DATE
);