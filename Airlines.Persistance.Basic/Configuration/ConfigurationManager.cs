﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Persistance.Basic.Configuration
{
    public class ConfigurationManager 
    {
        private static IConfigurationRoot _configuration;

        static ConfigurationManager()
        {
            _configuration = new ConfigurationBuilder()
                .AddJsonFile("appSettings.json")
                .Build();
        }

        public static string GetConnectionString(string name)
        {
            return _configuration.GetConnectionString(name)!;
        }
    }
}
