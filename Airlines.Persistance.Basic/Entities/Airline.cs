﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistance.Basic.Entities;

[Index("Name", Name = "IX_Airlines_Name")]
public partial class Airline
{
    [StringLength(255)]
    [Unicode(false)]
    public string Name { get; set; }

    public DateOnly? Founded { get; set; }

    public int? FleetSize { get; set; }

    [StringLength(255)]
    [Unicode(false)]
    public string Description { get; set; }

    [Key]
    [Column("ID")]
    public int Id { get; set; }
}