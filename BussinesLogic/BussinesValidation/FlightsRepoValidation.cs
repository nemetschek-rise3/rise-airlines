﻿using Airlines.Persistance.Basic.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.BussinesValidation
{
    public class FlightsRepoValidation
    {
        public const string ID = "Id";
        public const string CODE = "Code";
        public const string DEPARTUREAIRPORTID = "DepartureAirportId";
        public const string ARRIVALAIRPORTID = "ArrivalAirportId";
        public const string DEPARTUREDATETIME = "DepartureDateTime";
        public const string ARRIVALDATETIME = "ArrivalDateTime";
        public const string PRICE = "Price";
        public const string TIMEHOURS = "TimeHours";

        public bool ValidateFilter(string filter)
        {
            switch (filter)
            {
                case ID:
                    break;
                case CODE:
                    break;
                case DEPARTUREAIRPORTID:
                    break;
                case ARRIVALAIRPORTID:
                    break;
                case DEPARTUREDATETIME:
                    break;
                case ARRIVALDATETIME:
                    break;
                case PRICE:
                    break;
                case TIMEHOURS:
                    break;
                default:
                    return false;
            }

            return true;
        }


        public bool ValidateFlight(string name)
        {
            foreach (char c in name)
            {
                if (!char.IsLetterOrDigit(c))
                {
                    return false;
                }
            }

            return true;
        }
    }
}