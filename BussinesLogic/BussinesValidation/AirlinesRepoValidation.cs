﻿using Airlines.Persistance.Basic.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.BussinesValidation
{
    public class AirlinesRepoValidation
    {
        public const string AIRLINEID = "AirlineId";
        public const string NAME = "Name";
        public const string FOUNDED = "Founded";
        public const string FLEETSIZE = "FleetSize";
        public const string DESCRIPTION = "Description";

        public bool ValidateFilter(string filter)
        {
            switch (filter)
            {
                case AIRLINEID:
                    break;
                case NAME:
                    break;
                case FOUNDED:
                    break;
                case FLEETSIZE:
                    break;
                case DESCRIPTION:
                    break;
                default:
                    return false;
            }
            return true;
        }

        public bool ValidateAirline(string name)
        {
            if (name.Length < 6)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
