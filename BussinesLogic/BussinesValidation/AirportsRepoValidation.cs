﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.BussinesValidation
{
    public class AirportsRepoValidation
    {
        public const string AIRPORTID = "AirportId";
        public const string NAME = "Name";
        public const string COUNTRY = "Country";
        public const string CITY = "City";
        public const string CODE = "Code";
        public const string RUNWAYSCOUNT = "RunwaysCount";
        public const string FOUNDED = "Founded";

        public bool ValidateFilter(string filter)
        {
            switch (filter)
            {
                case AIRPORTID:
                    break;
                case CODE:
                    break;
                case NAME:
                    break;
                case COUNTRY:
                    break;
                case CITY:
                    break;
                case RUNWAYSCOUNT:
                    break;
                case FOUNDED:
                    break;
                default:
                    return false;
            }
            return true;
        }


        public bool ValidateAirport(string name)
        {
           return Validation.HasOnlyLetters(name) && name.Length == 3;
        }
    }
}
