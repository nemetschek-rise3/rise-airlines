﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Airlines.Persistance.Basic;
using Airlines.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Models.Reservations;
using RiseAirlines.Persistance.Basic.Exceptions;
using System.Data;
using Airlines.Bussines.DataStructures;
using Airlines.old.Persistance.Basic.Models;

namespace Airlines.Bussines.BussinesValidation
{
    public class CommandValidation
    {
        public void ValidateExistCommand(string[] commands, EntitiesDto entities)
        {
            if (commands!.Length is not 2)
            {

                throw new InvalidCommandException("The command should be from 2 words! exist <airline name>");
            }

            if (entities.Airlines.Count == 0)
            {
                throw new InvalidAirlineException("There is no airlines!");
            }
        }

        public void ValidateBatchCommand(string[] commands)
        {
            if (commands!.Length is not 2)
            {
                throw new InvalidCommandException(
                    "Invalid batch command. The command should be 2 words - ex : batch run! ");
            }

            if (commands[1].Equals("start"))
            {
                throw new InvalidCommandException(
                    "Invalid batch command. First execute batch start to start the batch mode!");
            }
        }

        public void ValidateHelpCommand(string[] commands)
        {
            if (commands.Length is not 1)
            {
                throw new InvalidCommandException("Command is longer than 1 word, did you mean 'help'");
            }
        }


        public void ValidateListCommand(string[] commands, EntitiesDto entities)
        {
            if (commands!.Length is not 3)
            {
                throw new InvalidCommandException("Command should be 3 words - list <from> <inputdata>");
            }

            if (!commands![1].Equals("city") && !commands[1].Equals("country"))
            {
                throw new InvalidCommandException("Wrong input - enter city or country");
            }

            if (entities!.Airports.Count == 0)
            {
                throw new InvalidAirportException("There is no airports!");
            }
        }

        public void ValidatePrintCommand(string[] commands)
        {
            if (commands!.Length < 2)
            {
                throw new InvalidCommandException("Enter entity to print!");
            }

            if (commands!.Length > 2)
            {
                throw new InvalidCommandException(
                    "Invalid input - the command should contain only 2 words! Did you mean print <entity>?");
            }
        }

        public void ValidateReserveTicketCommand(string[] commands, List<Reservation> reservations)
        {
            if (commands!.Length is not 5)
            {
                throw new InvalidCommandException("You haven't put enough data for reservation!");

            }

            if (reservations is null)
            {
                throw new InvalidCommandException("Reservations are null!");
            }
        }

        public void ValidatePassengerTicketCommand(string[] inputs, HashSet<FlightModel> flights)
        {
            if (inputs!.Length is not 5)
            {
                throw new InvalidCommandException("Invalid input: you have to write flight Id, Seats, Small Baggage count, Large Baggage count!");

            }


            FlightModel flight = Search.FlightSearch(inputs[2], flights);

            if (Validation.IsNumeric(inputs[3]))
            {
                if (int.Parse(inputs[3]) > 10)
                {
                    throw new InvalidReservationException("Maximum seat reservation is 10!");
                }
            }

            if (Validation.IsNumeric(inputs[4]))
            {
                if (int.Parse(inputs[4]) > 20)
                {
                    throw new InvalidReservationException("Maximum small baggage reservation is 20!");
                }
            }

            if (Validation.IsNumeric(inputs[5]))
            {
                if (int.Parse(inputs[5]) > 10)
                {
                    throw new InvalidReservationException("Maximum large baggage reservation is 10!");
                }
            }
        }

        public void ValidateCargoTicketCommand(string[] inputs, HashSet<FlightModel> flights)
        {
            if (inputs!.Length is not 6)
            {
                throw new InvalidCommandException("Invalid input: you have to write flight Id, Cargo weight and Cargo volume!");

            }

            FlightModel flight = Search.FlightSearch(inputs[2], flights);

            if (Validation.IsNumeric(inputs[3]))
            {
                if (int.Parse(inputs[3]) > 5000)
                {
                    throw new InvalidReservationException("Maximum cargo weight reservation is 5000!");
                }
            }

            if (Validation.IsNumeric(inputs[4]))
            {
                if (int.Parse(inputs[4]) > 5)
                {
                    throw new InvalidReservationException("Maximum cargo volume reservation is 5 m^3!");
                }
            }
        }

        public void ValidateAddRouteCommand(string[] commands, RouteLinkedList route)
        {
            if (commands.Length is not 3)
            {
                throw new InvalidCommandException(
                    "Invalid input. Enter flight Id or too many words in the command!");
            }

            if (!route.Created)
            {
                throw new InvalidCommandException("There is not route created, first write command route new!");
            }
        }


        public void ValidateNewRouteCommand(string[] commands)
        {
            if (commands.Length is not 2)
            {
                throw new InvalidCommandException(
                    "Invalid command - did you mean route new? The command should be only 2 words!");
            }
        }

        public void ValidateRemoveRouteCommand(string[] commands, RouteLinkedList route)
        {
            if (route.IsEmpty())
            {
                throw new InvalidCommandException("There is not route created, first write command route new!");
            }
        }

        public void ValidateFindRouteCommand(string[] commands)
        {
            if (commands.Length is not 4)
            {
                throw new InvalidCommandException("Enter the starting airport and the destination airport!");
            }
            bool validDepAirportId = int.TryParse(commands[2], out int depAirpId);
            bool validArrAirportId = int.TryParse(commands[3], out int arrAirpId);
            if (!validDepAirportId)
            {
                throw new InvalidRouteException("Enter valid departure airport Id!");
            }

            if (!validArrAirportId)
            {
                throw new InvalidRouteException("Enter valid arrival airport Id!");
            }
        }

        public void ValidateCheckRouteCommand(string[] commands)
        {
            if (commands.Length is not 4)
            {
                throw new InvalidCommandException("Enter the starting airport and the destination airport!");
            }
            bool validDepAirportId = int.TryParse(commands[2], out int depAirpId);
            bool validArrAirportId = int.TryParse(commands[3], out int arrAirpId);
            if (!validDepAirportId)
            {
                throw new InvalidRouteException("Enter valid departure airport Id!");
            }

            if (!validArrAirportId)
            {
                throw new InvalidRouteException("Enter valid arrival airport Id!");
            }
        }

        public void ValidateSearchRouteCommand(string[] commands)
        {
            if (commands.Length is not 5)
            {
                throw new InvalidCommandException("Enter starting airport, destination airport and strategy!");
            }
            bool validDepAirportId = int.TryParse(commands[2], out int depAirpId);
            bool validArrAirportId = int.TryParse(commands[3], out int arrAirpId);
            if (!validDepAirportId)
            {
                throw new InvalidRouteException("Enter valid departure airport Id!");
            }

            if (!validArrAirportId)
            {
                throw new InvalidRouteException("Enter valid arrival airport Id!");
            }

        }
    }
}


