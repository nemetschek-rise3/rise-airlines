﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using Airlines.Persistance.Basic.Entities;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using RiseAirlines.Persistance.Basic.Exceptions;

namespace Airlines.Bussines.BussinesValidation
{
    public class Validation
    {
        private const int _airport_length = 3;
        private const int _max_airline_length = 6;

        public static bool HasOnlyLetters(string name)
        {
            foreach (char c in name)
            {
                if (!char.IsLetter(c) && c != ' ')
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsAlphaNumeric(string name)
        {
            foreach (char c in name)
            {
                if (!char.IsLetterOrDigit(c))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsNumeric(string name)
        {
            foreach (char c in name)
            {
                if (!char.IsDigit(c))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool UniqueValidation(List<string> arr, string input)
        {
            foreach (string s in arr)
            {
                if (input == s)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool ValidateId(string id)
        {
            return int.TryParse(id, out int validId);
        }

        public static bool ValidateAirport(string name)
            => name.Length == _airport_length && HasOnlyLetters(name);


        public static bool ValidateAirline(string name)
            => name.Length < _max_airline_length;

        public static bool ValidateFlight(string name)
            => IsAlphaNumeric(name);


        public static bool ValidateInputLength(string[] inputs, int length)
        {
            if (inputs.Length < length)
            {
                return false;
            }
            return true;
        }
    }
}