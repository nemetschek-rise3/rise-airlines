﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Bussines.Dto;
using Airlines.Bussines.Mapper;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Entities;
using AutoMapper;

namespace Airlines.Persistance.Basic.Mapper
{
    public class AirportMapper : IAirportMapper
    {
        private readonly IMapper _mapper;
        public AirportMapper()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
                cfg.CreateMap<Airport, AirportDto>().ReverseMap());

            _mapper = config.CreateMapper();
        }

        public Airport MapToAirport(AirportDto airportModel)
        {
            return _mapper.Map<Airport>(airportModel);
        }

        public AirportDto MapToAirportDto(Airport airport)
        {
            return _mapper.Map<AirportDto>(airport);
        }
    }
}