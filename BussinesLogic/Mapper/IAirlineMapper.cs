﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Bussines.Dto;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Entities;

namespace Airlines.Bussines.Mapper
{
    public interface IAirlineMapper
    {
        public Airline MapToAirline(AirlineDto airlineModel);
        public AirlineDto MapToAirlineDto(Airline airline);
    }
}
