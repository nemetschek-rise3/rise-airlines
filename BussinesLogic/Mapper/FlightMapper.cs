﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Bussines.Dto;
using Airlines.Bussines.Mapper;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Entities;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Airlines.Persistance.Basic.Mapper
{
    public class FlightMapper : IFlightMapper
    {
        private readonly IMapper _mapper;

        public FlightMapper()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Flight, FlightDto>().ReverseMap()
                .ForMember(dest => dest.DepartureAirport, opt => opt.Ignore())
                .ForMember(dest => dest.ArrivalAirport, opt => opt.Ignore());
                //cfg.CreateMap<Airport, AirportDto>().ReverseMap();
            });

            _mapper = config.CreateMapper();
        }

        public Flight MapToFlight(FlightDto flightDto)
        {
            Flight flight = _mapper.Map<Flight>(flightDto);
            return flight;
        }

        public FlightDto MapToFlightDto(Flight flight)
        {
            FlightDto flightDto = _mapper.Map<FlightDto>(flight);
            return flightDto;
        }
    }
}
