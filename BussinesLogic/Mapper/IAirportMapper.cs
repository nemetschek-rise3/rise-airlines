﻿using Airlines.Bussines.Dto;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.Mapper
{
    public interface IAirportMapper 
    {
        public Airport MapToAirport(AirportDto airportModel);
        public AirportDto MapToAirportDto(Airport airport);
    }
}
