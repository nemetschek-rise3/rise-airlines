﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Bussines.Dto;
using Airlines.Bussines.Mapper;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Entities;
using Airlines.Persistance.Basic.Repository;
using AutoMapper;

namespace Airlines.Persistance.Basic.Mapper
{
    public class AirlineMapper : IAirlineMapper
    {
        private readonly IMapper _mapper;
        public AirlineMapper()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
                cfg.CreateMap<Airline, AirlineDto>().ReverseMap());

            _mapper = config.CreateMapper();
        }

        public Airline MapToAirline(AirlineDto airlineModel)
        {
            return _mapper.Map<Airline>(airlineModel);
        }

        public AirlineDto MapToAirlineDto(Airline airline)
        {
            return _mapper.Map<AirlineDto>(airline);
        }
    }
}
