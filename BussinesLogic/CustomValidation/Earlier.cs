﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.CustomValidation
{
    public class Earlier : ValidationAttribute
    {
        private readonly string _compareDateTime;

        public Earlier(string compareDateTime)
        {
            _compareDateTime = compareDateTime;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            PropertyInfo propertyInfo = validationContext.ObjectType.GetProperty(_compareDateTime)!;
            if (propertyInfo == null)
            {
                return new ValidationResult($"Property {_compareDateTime} not found on object.");
            }

            var comparisonValue = (DateTime)propertyInfo!.GetValue(validationContext.ObjectInstance)!;

            if ((DateTime)value <= comparisonValue)
            {
                return new ValidationResult($"{validationContext.DisplayName} must be greater than {_compareDateTime}.");
            }

            return ValidationResult.Success!;
        }
    }
}
