﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Airlines.Bussines.CustomValidation
{
    public class Later : ValidationAttribute
    {
        private readonly string _compareDateTime;

        public Later(string compareDateTime)
        {
            _compareDateTime = compareDateTime;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            PropertyInfo propertyInfo = validationContext.ObjectType.GetProperty(_compareDateTime)!;
            if (propertyInfo != null)
            {
                var comparisonValue = propertyInfo.GetValue(validationContext.ObjectInstance) as DateTime?;

                if (comparisonValue != null && (DateTime)value <= comparisonValue)
                {
                    return new ValidationResult($"{validationContext.DisplayName} must be greater than {_compareDateTime}.");
                }
            }
            return ValidationResult.Success!;
        }
    }
}