﻿using Airlines.Persistance.Basic.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Bussines.CustomValidation;
using System.Runtime.CompilerServices;

namespace Airlines.Bussines.Dto
{
    public class FlightDto
    {
        public FlightDto()
        {

        }

        public FlightDto(string code, int departureId, int arrivalId, DateTime departureDt, DateTime arrivalDt, int price, int time)
        {
            Code = code;
            DepartureAirportId = departureId;
            ArrivalAirportId = arrivalId;
            DepartureDateTime = departureDt;
            ArrivalDateTime = arrivalDt;
            Price = price;
            TimeHours = time;
        }

        [Required(ErrorMessage = "Code is required")]
        [RegularExpression("^(?=.*[a-zA-Z])(?=.*\\d).+$", ErrorMessage = "Code should contain at least 1 letter and 1 number")]
        public string? Code { get; set; }

        [Required(ErrorMessage = "Departure Airport Id is required")]
        public int? DepartureAirportId { get; set; }

        [Required(ErrorMessage = "Arrival Airport Id is required")]
        public int? ArrivalAirportId { get; set; }

        [Required(ErrorMessage = "Departure Time is required")]
        public DateTime? DepartureDateTime { get; set; }

        [Required(ErrorMessage = "Arrival Time is required")]
        [Later(nameof(DepartureDateTime))]
        public DateTime? ArrivalDateTime { get; set; }

        public int? Price { get; set; }

        public int? TimeHours { get; set; }

        public virtual Airport? ArrivalAirport { get; set; }

        public virtual Airport? DepartureAirport { get; set; }
    }
}
