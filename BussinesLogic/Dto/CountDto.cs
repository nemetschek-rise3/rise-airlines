﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.Dto
{
    public class CountDto
    {
        public CountDto(int airportsCount, int airlinesCount, int flightsCount)
        {
            AirportsCount = airportsCount;
            AirlinesCount = airlinesCount;
            FlightsCount = flightsCount;
        }

        public int AirportsCount {  get; set; }
        public int AirlinesCount {  get; set; }
        public int FlightsCount {  get; set; }
    }
}
