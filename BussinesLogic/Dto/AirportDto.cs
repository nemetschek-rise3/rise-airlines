﻿using Airlines.Persistance.Basic.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Query.Internal;

namespace Airlines.Bussines.Dto
{
    public class AirportDto
    {
        public AirportDto()
        {
            
        }
        public AirportDto(string name, string country, string city,int runwaysCount, DateTime founded)
        {
            Name = name; 
            Country = country;
            City = city;
            RunwaysCount = runwaysCount;
            Founded = founded;
        }

        [Required(ErrorMessage = "Name is required")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Field must contain only letters")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "Country is required")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Field must contain only letters")]
        public string? Country { get; set; }

        [Required(ErrorMessage = "City is required")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Field must contain only letters")]
        public string? City { get; set; }

        [Required(ErrorMessage = "Code is required")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Field must contain only letters")]
        [StringLength(3, MinimumLength = 3, ErrorMessage = "Code must be 3 characters")]
        public string? Code { get; set; }

        [Required(ErrorMessage = "Runways Count is required")]
        [Display(Name = "Runways Count")]
        public int? RunwaysCount { get; set; }
        
        public DateTime? Founded { get; set; }
    }
}
