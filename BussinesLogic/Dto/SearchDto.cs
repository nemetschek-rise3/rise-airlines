﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.Dto
{
    public class SearchDto
    {
        public SearchDto()
        {   
        }

        public SearchDto(string filter, string searchInput)
        {
            Filter = filter;
            SearchInput = searchInput;
        }

        public string Filter {  get; set; }
        public string SearchInput {  get; set; }
    }
}
