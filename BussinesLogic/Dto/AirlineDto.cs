﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.Dto
{
    public class AirlineDto
    {
        public AirlineDto() { }

        public AirlineDto(string name, DateOnly founded, int fleetSize, string description)
        {
            Name = name;
            FleetSize = fleetSize;
            Founded = founded;
            Description = description;
        }

        [Required(ErrorMessage = "Name is required")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Field must contain only letters")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "Founded is required")]
        public DateOnly? Founded { get; set; }

        [Required(ErrorMessage = "FleetSize is required")]
        public int? FleetSize { get; set; }

        [Required(ErrorMessage = "Description is required")]
        public string? Description { get; set; }
    }
}
