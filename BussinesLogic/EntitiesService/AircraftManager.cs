﻿using Airlines.Persistance.Basic.Models.Aircrafts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Persistance.Basic.EntitiesService;
public class AircraftManager
{
    public static AircraftDto CreateAircrafts(string path)
    {
        bool isHeader = true;
        HashSet<PassengerAircraft> passengerAircrafts = [];
        HashSet<CargoAircraft> cargoAircrafts = [];
        HashSet<PrivateAircraft> privateAircrafts = [];
        AircraftDto aircraftDto = new AircraftDto(passengerAircrafts, cargoAircrafts, privateAircrafts);


        using (StreamReader reader = new StreamReader(path))
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine()!;
                string[] values = line.Split(';')!;

                if (!isHeader)
                {
                    if (values[0] is not null || !HasMoreThan1Field(values[1], values[2], values[3]))
                    {

                        aircraftDto = AddField(values, aircraftDto);

                    }
                }
                isHeader = false;
            }
       
        return aircraftDto;
    }

    public static AircraftDto AddField(string[] values, AircraftDto dto)
    {
        string model = values[0];
        bool hasSeats = int.TryParse(values[1], out int seats);
        bool hasCargoWeight = int.TryParse(values[2], out int cargoWeight);
        bool hasCargoVolume = int.TryParse(values[3], out int cargoVolume);

        if (!hasSeats && hasCargoVolume && hasCargoWeight)
        {
            CargoAircraft cargoAircraft = new CargoAircraft(model, cargoWeight, cargoVolume);
            dto.CargoAircrafts!.Add(cargoAircraft);
        }
        else if (hasSeats && hasCargoVolume && hasCargoVolume)
        {
            PassengerAircraft passengerAircraft = new PassengerAircraft(model, seats, cargoWeight, cargoVolume);
            dto.PassengerAircrafts!.Add(passengerAircraft);
        }
        else if (hasSeats && !hasCargoWeight && !hasCargoVolume)
        {
            PrivateAircraft privateAircraft = new PrivateAircraft(model, seats);
            dto.PrivateAircrafts!.Add(privateAircraft);
        }

        return dto;
    }
    public static bool HasMoreThan1Field(string seats, string cargoWeight, string cargoVolume)
    {
        List<string> fields = [seats, cargoWeight, cargoVolume];

        int counter = 0;
        foreach (string field in fields)
            if (field.Equals("-"))
            {
                counter++;
            }
        return counter <= 2;

    }
}
