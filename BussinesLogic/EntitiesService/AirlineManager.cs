﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Persistance.Basic.EntitiesService;
using Airlines.old.Persistance.Basic.Models;
using RiseAirlines.Persistance.Basic.Exceptions;

#pragma warning disable CA1822 // Mark members as static

namespace RiseAirlines.Persistance.Basic.EntitiesService;
public class AirlineManager : Service<AirlineModel>
{
    public static HashSet<AirlineModel> CreateAirlines(string path)
    {
        Service<AirlineModel> service = new Service<AirlineModel>();
        HashSet<AirlineModel> airlines = [];

        using (StreamReader reader = new StreamReader(path))
        {
            bool isHeader = true;
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine()!;
                string[] values = line.Split(';')!;
                bool validId = int.TryParse(values[0], out int id);
                string name = values[1];
                bool validFoundedDate = DateOnly.TryParse(values[2], out DateOnly founded);
                bool validFleetSize = int.TryParse(values[3], out int fleetSize);
                if (!isHeader)
                {
                    if (!validId)
                    {
                        throw new InvalidAirlineException($"Invalid id of airline {values[0]}");
                    }

                    if (!validFoundedDate)
                    {
                        throw new InvalidAirlineException($"Invalid date of airline {values[2]}");
                    }

                    if (!validFleetSize)
                    {
                        throw new InvalidAirlineException($"Invalid Fleet size of airline {values[3]}");
                    }

                    AirlineModel airline = new AirlineModel(id, name, founded, fleetSize);
                    if (service.AllFieldsHaveValue(airline))
                        _ = airlines.Add(airline);
                }
                isHeader = false;
            }
        }

        return airlines;
    }
}
