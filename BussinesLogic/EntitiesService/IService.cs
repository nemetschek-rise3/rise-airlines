﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Persistance.Basic.EntitiesService
{
    public interface IService<T>
    {
        public bool AllFieldsHaveValue(object obj);

        public bool CompareSets(HashSet<T> set1, HashSet<T> set2);

        public bool ContainsSet(HashSet<T> set1, T obj);

        public bool CompareLists(List<T> list1, List<T> list2);

        public bool ContainsList(List<T> list1, T obj);

        public bool CompareArrayLists(ArrayList list1, ArrayList list2);
    }
}
