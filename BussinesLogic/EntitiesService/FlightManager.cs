﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Airlines.Persistance.Basic;
using Airlines.Persistance.Basic.EntitiesService;
using Airlines.old.Persistance.Basic.Models;
using RiseAirlines.Persistance.Basic.Exceptions;

#pragma warning disable CA1822 


namespace RiseAirlines.Persistance.Basic.EntitiesService;
public class FlightManager : Service<FlightModel>
{

    public static HashSet<FlightModel> CreateFlights(string path)
    {
        Service<FlightModel> service = new Service<FlightModel>();
        HashSet<FlightModel> flights = [];
        using (StreamReader reader = new StreamReader(path))
        {
            bool isHeader = true;
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine()!;

                if (!isHeader)
                {
                    string[] values = line.Split(';')!;
                    bool validId = int.TryParse(values[0], out int flightId);
                    bool validDepAirport = int.TryParse(values[2], out int depAirportId);
                    bool validArrAirport = int.TryParse(values[3], out int arrAirportId);
                    bool validPrice = int.TryParse(values[5], out int price);
                    bool validTime = int.TryParse(values[6], out int time);
                    bool validDepDt = DateTime.TryParse(values[7], out DateTime depDateTime);
                    bool validArrDt = DateTime.TryParse(values[8],out DateTime arrDateTime);
                    if (values.Length is 9)
                    {
                        if (!validId)
                        {
                            throw new InvalidFlightException("Flight must have valid id with numbers!");
                        }

                        if (!validDepAirport)
                        {
                            throw new InvalidFlightException($"Invalid departure airport id {values[2]}!");
                        }

                        if (!validArrAirport)
                        {
                            throw new InvalidFlightException($"Invalid arrival airport id  {values[3]}!");
                        }

                        if (!validDepDt)
                        {
                            throw new InvalidFlightException($"Invalid Departure Date Time format  {values[7]}!");
                        }

                        if (!validArrDt)
                        {
                            throw new InvalidFlightException($"Invalid Arrival Date Time format  {values[8]}!");
                        }

                        if (!validPrice)
                        {
                            throw new InvalidFlightException($"Flights can contain only number for flights price  {values[5]}!");
                        }

                        if (!validTime)
                        {
                            throw new InvalidFlightException($"Flights can contain only number for flights time {values[6]}!");
                        }

                        FlightModel flight = new FlightModel(flightId, values[1],depAirportId, arrAirportId, values[4], price, time, depDateTime, arrDateTime );
                        if (service.AllFieldsHaveValue(flight))

                            _ = flights.Add(flight);
                    }
                }
                isHeader = false;
            }
        }
        return flights;

    }

    public static ArrayList ReadFlightsFromCsv(string path, HashSet<FlightModel> flights)

    {
        ArrayList treeFlights = new ArrayList();
        string rootAirport;
        using (StreamReader reader = new StreamReader(path))
        {
            bool isHeader = true;
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine()!;
                if (isHeader != true)
                {
                    FlightModel flight = Search.FlightSearch(line, flights);
                    if (flight != null)
                        treeFlights.Add(flight);
                    else
                        throw new InvalidFlightException("FLights from tree not found!");
                    isHeader = false;
                }
                else
                {
                    rootAirport = line;
                    treeFlights.Add(rootAirport);
                    isHeader = false;
                }
            }
            return treeFlights;
        }
    }
}
