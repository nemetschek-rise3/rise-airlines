﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Persistance.Basic.EntitiesService;
using Airlines.old.Persistance.Basic.Models;
using RiseAirlines.Persistance.Basic.Exceptions;

#pragma warning disable CA1822 // Mark members as static

namespace RiseAirlines.Persistance.Basic.EntitiesService;
public class AirportsManager : Service<AirportModel>
{
    public static HashSet<AirportModel> CreateAirports(string path)
    {
        Service<AirportModel> service = new Service<AirportModel>();
        HashSet<AirportModel> airports = [];
        using (StreamReader reader = new StreamReader(path))
        {
            bool isHeader = true;

            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine()!;
                string[] values = line.Split(';')!;

                bool validId = int.TryParse(values[0], out int id);
                bool validRunwaysCount = int.TryParse(values[5], out int runwaysCount);
                bool validFounded = DateOnly.TryParse(values[6], out DateOnly founded);

                if (!isHeader)
                {
                    if (!validId)
                    {
                        throw new InvalidAirportException($"Invalid Id of airport {values[0]}");
                    }

                    if (!validRunwaysCount)
                    {
                        throw new InvalidAirportException($"Invalid runways count of airport {values[5]}");
                    }

                    if (!validFounded)
                    {
                        throw new InvalidAirportException($"Invalid founded date of airport {values[6]}");
                    }

                    AirportModel airport = new AirportModel(id, values[1], values[2], values[3],values[4],runwaysCount,founded);
                    if (service.AllFieldsHaveValue(airport))
                        _ = airports.Add(airport);
                }
                isHeader = false;
            }
        }
        return airports;
    }
}
