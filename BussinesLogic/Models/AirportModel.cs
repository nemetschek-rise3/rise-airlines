﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Bussines.BussinesValidation;
using Airlines.Persistance.Basic.Entities;
using RiseAirlines.Persistance.Basic.Exceptions;
using RiseAirlines.Persistance.Basic.Interfaces;

namespace Airlines.old.Persistance.Basic.Models
{
    public class AirportModel : INameItem, IdItem
    {
        public AirportModel(){}
        public AirportModel(int id, string code,string name, string city, string country, int runwaysCount, DateOnly founded)
        {
            this.Id = id;
            this.Name = name;
            this.City = city;
            this.Code = code;
            this.Country = country;
            this.RunwaysCount = runwaysCount;
            this.Founded = founded;
            FlightArrivalAirports = new List<Flight>();
            FlightDepartureAirports = new List<Flight>();
        }

        private string? code;
        private string? name;
        private string? city;
        private string? country;
        
        public int Id { get; set; }
        public string Code
        {
            get { return code!; }
            set
            {
                if (!Validation.ValidateAirport(value))
                {
                    throw new InvalidAirportException(
                        "Error: Code must have only 3 letters. The airport is not going to be saved!");
                }
                else
                {
                    code = value;
                }
            }
        }

        public string? Name
        {
            get { return name; }
            set
            {
                if (!Validation.HasOnlyLetters(value!))
                {
                    throw new InvalidAirportException(
                        $"Error: Name must contain only letters! {value}. The airport is not going to be saved!");
                }
                else
                {
                    name = value!;
                }

            }
        }

        public string? City
        {
            get { return city; }
            set
            {
                if (!Validation.HasOnlyLetters(value!))
                {
                    throw new InvalidAirportException(
                        $"Error: Name must contain only letters! {value}. The airport is not going to be saved!");
                }
                else
                {
                    city = value!;
                }

            }
        }

        public string? Country
        {
            get { return country; }
            set
            {
                if (!Validation.HasOnlyLetters(value!))
                {
                    throw new InvalidAirportException(
                        $"Error: Name must contain only letters! {value}. The airport is not going to be saved!");
                }
                else
                {
                    country = value!;
                }

            }
        }

        public int? RunwaysCount { get; set; }

        public DateOnly? Founded { get; set; }

        public virtual ICollection<Flight> FlightArrivalAirports { get; set; } = null!;

        public virtual ICollection<Flight> FlightDepartureAirports { get; set; } = null!;

        public override bool Equals(object? obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }

            AirportModel airport = (AirportModel)obj;
            return this.Name!.Equals(airport.Name) && this.Id.Equals(airport.Id) && this.City!.Equals(airport.City) &&
                   this.Country!.Equals(airport.Country);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name, City, Country);
        }
    }
}
