﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Persistance.Basic.Models.Reservations
{
    public class PassengerReservation : Reservation
    {
        public int Seats { get; set; }
        public int SmallBaggageCount { get; set; }
        public int LargeBaggageCount { get; set; }

        public PassengerReservation(string flightIdentifier, int seats, int largeBaggageCount, int smallBaggageCount) : base(flightIdentifier)
        {
            Seats = seats;
            LargeBaggageCount = largeBaggageCount;
            SmallBaggageCount = smallBaggageCount;
        }

        public static PassengerReservation CreatePassengerReservation(string _flightId, int _seats, int _smallBaggageCount, int _largeBaggageCount)
        {
            return new PassengerReservation(_flightId, _seats, _smallBaggageCount, _largeBaggageCount);
        }
        public override string GetInfo()
        {
            return $"Passenger Ticket Reservation for Flight {FlightIdentifier}: Seats: {Seats}, Small Baggage: {SmallBaggageCount}, Large Baggage: {LargeBaggageCount}";
        }
    }
}
