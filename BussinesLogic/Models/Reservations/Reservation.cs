﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Persistance.Basic.Models.Reservations
{
    public class Reservation
    {
        public string FlightIdentifier { get; set; }

        public Reservation(string flightIdentifier)
        {
            FlightIdentifier = flightIdentifier;
        }

        public virtual string GetInfo()
        {
            return null!;
        }
    }
}
