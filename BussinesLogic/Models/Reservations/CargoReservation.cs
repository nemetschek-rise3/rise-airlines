﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Persistance.Basic.Models.Reservations
{
    public class CargoReservation : Reservation
    {
        public double CargoWeight { get; set; }
        public double CargoVolume { get; set; }

        public CargoReservation(string flightIdentifier, double cargoWeight, double cargoVolume)
            : base(flightIdentifier)
        {
            CargoWeight = cargoWeight;
            CargoVolume = cargoVolume;
        }

        public override string GetInfo()
        {
            return $"Cargo Reservation for Flight {FlightIdentifier}: Weight: {CargoWeight} kg, Volume: {CargoVolume} m³";
        }
    }
}
