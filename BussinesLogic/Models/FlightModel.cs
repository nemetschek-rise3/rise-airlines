﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Airlines.Bussines.BussinesValidation;
using Airlines.Persistance.Basic.Entities;
using RiseAirlines.Persistance.Basic.Exceptions;
using RiseAirlines.Persistance.Basic.Interfaces;

namespace Airlines.old.Persistance.Basic.Models;

public class FlightModel : IdItem
{
    public FlightModel() { }
    public FlightModel(int id, string code, int departureAirport, int arrivalAirport, string aircraftModel,  int price, int time, DateTime departureDT, DateTime arrivalDT)
    {
        this.Id = id!;
        this.Code = code;
        this.ArrivalAirport = arrivalAirport;
        this.DepartureAirport = departureAirport;
        this.AircraftModel = aircraftModel;
        this.Price = price;
        this.Time = time;
        this.DepartureDateTime = departureDT;
        this.ArrivalDateTime = arrivalDT;
    }

    private string? code;
    private int arrivalAirportId;
    private int departureAirportId;
    private string? aircraft;
    private int price;
    private int time;


    public int Id { get; set; }
    public string Code
    {
        get { return code!; }
        set
        {
            if (!Validation.ValidateFlight(value))
            {
                throw new InvalidFlightException(
                    "Error: Flight Id must contain only letters or digits! The flight is not going to be saved!");
            }
            else
            { 
                code = value;
            }
        }
    }

    public int DepartureAirport
    {
        get { return departureAirportId!; }
        set
        {
            if (!Validation.ValidateId(value.ToString()))
            { 
                throw new InvalidFlightException("Error: Departure Airport must contain numbers! The flight is not going to be saved!");
            }
            else
            {
                departureAirportId = value!;
            }
        }
    }

    public int ArrivalAirport
    {
        get { return arrivalAirportId; }
        set
        {
            if (!Validation.ValidateId(value.ToString()))
            {
                throw new InvalidFlightException("Error: AirportId must contain only numbers! The flight is not going to be saved!");
            }
            else
            {
                arrivalAirportId = value!;
            }
        }
    }

    public string AircraftModel
    {
        get => aircraft!;
        set
        {
            aircraft = value;
        }
    }

    public int Price
    {
        get => price;
        set
        {
            price = value;
        }
    }

    public int Time
    {
        get => time;
        set
        {
            time = value;
        }
    }

    public DateTime? DepartureDateTime { get; set; }

    public DateTime? ArrivalDateTime { get; set; }

    public override bool Equals(object? obj)
    {
        if (obj == null || this.GetType() != obj.GetType())
        {
            return false;
        }
        FlightModel flight = (FlightModel)obj;
        return Id!.Equals(flight.Id) && ArrivalAirport!.Equals(flight.ArrivalAirport) && 
               DepartureAirport!.Equals(flight.DepartureAirport) && AircraftModel.Equals(flight.AircraftModel) && Price == flight.Price && Time == flight.Time;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Id, DepartureAirport, ArrivalAirport);
    }
}
