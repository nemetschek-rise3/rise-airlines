﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Bussines.BussinesValidation;
using Airlines.Persistance.Basic.Models;
using RiseAirlines.Persistance.Basic.Exceptions;
using RiseAirlines.Persistance.Basic.Interfaces;

namespace Airlines.old.Persistance.Basic.Models;

public class AirlineModel : INameItem, IdItem
{
    public AirlineModel()
    {
    }

    public AirlineModel(int id, string name, DateOnly founded, int fleetSize)
    {
        this.Id = id;
        this.Name = name;
        this.Founded = founded;
        this.FleetSize = fleetSize;
    }

    private string? name;

    public int Id { get; set; }

    public string? Name
    {
        get { return name; }
        set
        {
            if (!Validation.ValidateAirline(value!))
            {
                throw new InvalidAirlineException(
                    $"Error: Name must contain less than 6 letters! {value}. The airline is not going to be saved!");
            }
            else
            {
                name = value!;
            }

        }
    }

    public DateOnly? Founded { get; set; }

    public int? FleetSize { get; set; }

    public override bool Equals(object? obj)
    {
        if (obj == null || this.GetType() != obj.GetType())
        {
            return false;
        }

        AirlineModel airline = (AirlineModel)obj;
        return this.Name!.Equals(airline.Name) && this.Id.Equals(airline.Id);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Id, Name);
    }
}