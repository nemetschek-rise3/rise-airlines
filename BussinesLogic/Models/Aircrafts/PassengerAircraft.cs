﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Persistance.Basic.Models.Aircrafts
{
    public class PassengerAircraft : Aircraft
    {

        public int Seats { get; set; }
        public int CargoWeight { get; set; }
        public int CargoVolume { get; set; }

        public PassengerAircraft(string model, int cargoWeight, int cargoVolume, int seats)
            : base(model)
        {
            Seats = seats;
            CargoWeight = cargoWeight;
            CargoVolume = cargoVolume;
        }

        public override void DisplayInfo()
        {
            base.DisplayInfo();
            Console.WriteLine($"Seats: {Seats}, Baggage Weight: {CargoWeight} kg, Baggage Volume: {CargoVolume} cubic meters");
        }
    }
}
