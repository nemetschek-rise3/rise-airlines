﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Persistance.Basic.Models.Aircrafts
{
    public class CargoAircraft : Aircraft
    {

        public int CargoWeight { get; set; }
        public int CargoVolume { get; set; }


        public CargoAircraft(string model, int cargoWeight, int cargoVolume)
            : base(model)
        {
            CargoWeight = cargoWeight;
            CargoVolume = cargoVolume;
        }


        public override void DisplayInfo()
        {
            base.DisplayInfo();
            Console.WriteLine($"Cargo Weight: {CargoWeight} kg, Cargo Volume: {CargoVolume} cubic meters");
        }
    }
}
