﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Persistance.Basic.Models.Aircrafts
{
    public class Aircraft
    {
        public string Model { get; set; }

        public Aircraft(string model)
        {
            Model = model;
        }

        public virtual void DisplayInfo()
        {
            Console.WriteLine($"Model is {Model}");
        }
    }
}
