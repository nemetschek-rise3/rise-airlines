﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Persistance.Basic.Models.Aircrafts
{
    public class AircraftDto
    {
        public HashSet<PassengerAircraft>? PassengerAircrafts { get; set; }
        public HashSet<CargoAircraft>? CargoAircrafts { get; set; }
        public HashSet<PrivateAircraft>? PrivateAircrafts { get; set; }

        public AircraftDto(HashSet<PassengerAircraft> passengerAircrafts, HashSet<CargoAircraft> cargoAircrafts, HashSet<PrivateAircraft> privateAircrafts)
        {
            PassengerAircrafts = passengerAircrafts;
            CargoAircrafts = cargoAircrafts;
            PrivateAircrafts = privateAircrafts;
        }
    }

}
