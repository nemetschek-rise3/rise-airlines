﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Persistance.Basic.Entities;
using Airlines.Persistance.Basic.Models.Aircrafts;

namespace Airlines.old.Persistance.Basic.Models
{
    public class EntitiesDto
    {
        public EntitiesDto(HashSet<AirportModel> airports, HashSet<AirlineModel> airlines, HashSet<FlightModel> flights,
            AircraftDto aircraftDto)
        {
            this.Airports = airports;
            this.Airlines = airlines;
            this.Flights = flights;
            this.AircraftDto = aircraftDto;
        }

        private EntitiesDto(HashSet<AirportModel> airports, HashSet<AirlineModel> airlines, HashSet<FlightModel> flights)
        {
            this.Airports = airports;
            this.Airlines = airlines;
            this.Flights = flights;
        }

        public AircraftDto? AircraftDto { get; set; }
        public HashSet<AirportModel> Airports { get; set; }
        public HashSet<AirlineModel> Airlines { get; set; }
        public HashSet<FlightModel> Flights { get; set; }

    }

}
