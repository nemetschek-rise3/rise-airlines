﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.old.Persistance.Basic.Models;
using Microsoft.VisualBasic.CompilerServices;
using RiseAirlines.Persistance.Basic.Exceptions;

namespace Airlines.Bussines.DataStructures
{
    public class FlightRouteTree
    {
        private TreeNode? Root { get; set; }

        public TreeNode BuildTreeFromCSV(ArrayList flights)
        {
            Dictionary<int, TreeNode> airportNodes = new Dictionary<int, TreeNode>();
            string? root = (string)flights[0]!;
            int.TryParse(root,out int rootAirport);
            airportNodes[rootAirport] = new TreeNode(rootAirport);
            flights.RemoveAt(0);
            foreach (FlightModel flight in flights)
            {
                if (!airportNodes.ContainsKey(flight.DepartureAirport!))
                {
                    airportNodes[flight.DepartureAirport!] = new TreeNode(flight.DepartureAirport!);
                }

                TreeNode departureAirport = airportNodes[flight.DepartureAirport!];

                if (!airportNodes.ContainsKey(flight.ArrivalAirport!))
                {
                    airportNodes[flight.ArrivalAirport!] = new TreeNode(flight.ArrivalAirport!);
                }

                TreeNode arrivalAirport = airportNodes[flight.ArrivalAirport!];

                departureAirport.NextNodes.Add(arrivalAirport);
                arrivalAirport.Parent = departureAirport;
            }
            return airportNodes.Values.First();

        }


        public TreeNode DFS(TreeNode current, int destinationAirportId)
        {
            if (current.AirportId.Equals(destinationAirportId))
            {
                return current;
            }
            foreach (TreeNode next in current.NextNodes)
            {
                TreeNode result = DFS(next, destinationAirportId);
                if (result != null)
                {
                    return result;
                }
            }

            return null!;
        }
        public List<int> FindDestinationRoute(TreeNode destinationAirportNode, TreeNode rootAirportNode)
        {
            if (destinationAirportNode == null)
            {
                throw new InvalidRouteException("The destination airport is not found!");
            }
            List<int> path = new List<int>();
            do
            {
                path.Add(destinationAirportNode.AirportId);
                destinationAirportNode = destinationAirportNode.Parent!;
            }
            while (destinationAirportNode != null);

            path.Reverse();
            return path;
        }
    }
}
