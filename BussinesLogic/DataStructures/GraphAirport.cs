﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Airlines.Bussines.StrategyAlgorithms;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Entities;
using Airlines.Persistance.Basic.EntitiesService;
using Microsoft.EntityFrameworkCore.Infrastructure.Internal;
using RiseAirlines.Persistance.Basic.EntitiesService;
using RiseAirlines.Persistance.Basic.Exceptions;

namespace Airlines.Bussines.DataStructures
{
    public class GraphAirport
    {
        public Dictionary<int, HashSet<FlightModel>> AdjacencyList { get; set; }

        public GraphAirport()
        {
            AdjacencyList = new Dictionary<int, HashSet<FlightModel>>();
        }

        public void LoadGraph(HashSet<FlightModel> flights)
        {
            foreach (FlightModel flight in flights)
            {
                if (!AdjacencyList.ContainsKey(flight.DepartureAirport!))
                {
                    AdjacencyList[flight.DepartureAirport!] = new HashSet<FlightModel>();
                }

                AdjacencyList[flight.DepartureAirport!].Add(flight);
            }
        }

        public bool IsConnected(int startAirport, int destinationAirport)
        {
            if (!AdjacencyList.ContainsKey(startAirport) || !AdjacencyList.ContainsKey(destinationAirport))
            {
                return false;
            }

            if (startAirport == destinationAirport)
            {
                return true;
            }

            HashSet<int> visited = new HashSet<int>();
            Stack<int> stack = new Stack<int>();
            stack.Push(startAirport);

            while (stack.Count > 0)
            {
                int currentAirport = stack.Pop();
                visited.Add(currentAirport);

                if (currentAirport == destinationAirport)
                {
                    return true;
                }

                if (AdjacencyList.ContainsKey(currentAirport))
                {
                    foreach (FlightModel flight in AdjacencyList[currentAirport])
                    {
                        if (!visited.Contains(flight.ArrivalAirport))
                        {
                            stack.Push(flight.ArrivalAirport);
                        }
                    }
                }
            }

            return false;
        }

        public List<int> Bfs(int startAirport, int destinationAirport)
        {
            if (!AdjacencyList.ContainsKey(startAirport))
            {
                throw new InvalidRouteException("The starting airport doesn't exist!");
            }
            Dictionary<int, int> parent = new Dictionary<int, int>();
            Dictionary<int, bool> visited = new Dictionary<int, bool>();
            foreach (int vertex in AdjacencyList.Keys)
            {
                parent[vertex] = 0;
                visited[vertex] = false;
            }

            Queue<int> nextVertexes = new Queue<int>();
            nextVertexes.Enqueue(startAirport);

            while (nextVertexes.Count > 0)
            {
                int current = nextVertexes.Dequeue();
                visited[current] = true;
                foreach (FlightModel flight in AdjacencyList[current])
                {
                    int child = flight.ArrivalAirport;

                    if (!visited[child])
                    {
                        visited[child] = true;
                        parent[child] = current;
                        nextVertexes.Enqueue(child);

                        if (child.Equals(destinationAirport))
                        {
                            return FindPath(parent, startAirport, destinationAirport);
                        }
                    }
                }
            }

            throw new InvalidRouteException("The destination airport is not found or is not connected to the start!");
        }

        public List<int> FindPath(Dictionary<int, int> parent, int startAirport, int destinationAirport)
        {
            List<int> nearestPath = new List<int>();
            int current = destinationAirport;

            while (current != 0)
            {
                nearestPath.Add(current);
                current = parent.GetValueOrDefault(current)!;
            }

            nearestPath.Reverse();
            return nearestPath;
        }

        public bool CompareGraph(Dictionary<int, HashSet<FlightModel>> adjDictionary)
        {
            Service<FlightModel> service = new Service<FlightModel>();
            return AdjacencyList.Count == adjDictionary.Count &&
                   AdjacencyList.Keys.All(k =>
                       adjDictionary.ContainsKey(k) &&
                       service.CompareSets(AdjacencyList[k], adjDictionary[k]));
        }

        public List<int> FindNearestPath(int startAirportId, int destinationAirportId, Algorithm algorithm)
        {
            algorithm.Execute(AdjacencyList);

            List<int> nearestPath = FindPath(algorithm.Parent, startAirportId, destinationAirportId);
            return nearestPath;
        }
    }
}
