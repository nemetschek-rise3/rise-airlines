﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.old.Persistance.Basic.Models;


namespace Airlines.Bussines.DataStructures
{
    public class LinkedListNode
    {
        public FlightModel? Flight { get; set; }
        public LinkedListNode? Next { get; set; }

        public LinkedListNode(FlightModel flight)
        {
            Flight = flight;
            Next = null;
        }
    }
}