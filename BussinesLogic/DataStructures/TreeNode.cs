﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.DataStructures
{
    public class TreeNode
    {
        public int AirportId { get; set; }
        public List<TreeNode> NextNodes { get; set; }
        public TreeNode? Parent { get; set; }

        public TreeNode(int id)
        {
            AirportId = id;
            NextNodes = new List<TreeNode>();
        }

        public override bool Equals(object? obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            TreeNode other = (TreeNode)obj;

            if (AirportId != other.AirportId)
            {
                return false;
            }

            if (NextNodes.Count != other.NextNodes.Count)
            {
                return false;
            }

            for (int i = 0; i < NextNodes.Count; i++)
            {
                if (!NextNodes[i].Equals(other.NextNodes[i]))
                {
                    return false;
                }
            }

            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
