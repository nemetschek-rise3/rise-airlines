﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.old.Persistance.Basic.Models;
using RiseAirlines.Persistance.Basic.Exceptions;

namespace Airlines.Bussines.DataStructures
{
    public class RouteLinkedList
    {
        public LinkedListNode? head;
        public bool Created { get; set; } = false;

        public void AddFlight(FlightModel flight)
        {

            if (flight != null)
            {
                LinkedListNode newNode = new LinkedListNode(flight);
                if (head == null)
                {
                    head = newNode;
                }
                else
                {
                    LinkedListNode current = head;
                    current = findLast(current);
                    if (current.Flight!.ArrivalAirport!.Equals(flight.DepartureAirport))
                    {
                        current.Next = newNode;
                    }
                    else
                    {

                        throw new InvalidRouteException($"Arrival airport of flight {current.Flight.Code} and departure airport of flight {flight.Code} don't match!");

                    }
                }
            }
            else
            {
                throw new InvalidRouteException("Flight is not found!");

            }
        }

        public void RemoveFlight()
        {
            if (head == null)
            {
                throw new InvalidRouteException("Route is empty");
            }
            else if (head.Next == null)
            {
                head = null;
            }
            else
            {
                LinkedListNode current = head;
                current = findPreLast(current);
                current.Next = null;
            }
        }
        public void Clear()
        {
            head = null;
        }

        public bool IsEmpty()
        {
            return head == null;
        }

        public LinkedListNode findPreLast(LinkedListNode current)
        {
            while (current.Next?.Next != null)
            {
                current = current.Next;
            }
            return current;
        }

        public LinkedListNode findLast(LinkedListNode current)
        {
            while (current.Next != null)
            {
                current = current.Next;
            }
            return current;
        }
    }
}
