﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Airlines.old.Persistance.Basic.Models;
using RiseAirlines.Persistance.Basic.Exceptions;

namespace Airlines.Persistance.Basic;
public static class Search
{
    public static IEnumerable<AirportModel> AirportCountrySearch(string searchName, HashSet<AirportModel> airports)
    {
        IEnumerable<AirportModel> correctCountriesAirports = airports.Where(airport => airport.Country == searchName);

        if (correctCountriesAirports.Any())
        {
            return correctCountriesAirports;
        }
        else
        {
            throw new InvalidAirportException("No airports found!");
        }

    }

    public static AirportModel AirportIdSearch(int id, HashSet<AirportModel> airports)
    {
        AirportModel airport = airports.FirstOrDefault(airport => airport.Id == id)!;
        return airport!;
    }

    public static IEnumerable<AirportModel> AirportCitySearch(string searchName, HashSet<AirportModel> airports)
    {
        IEnumerable<AirportModel> correctCitiesAirports = airports.Where(airport => airport.City == searchName);

        if (correctCitiesAirports.Any())
        {
            return correctCitiesAirports;
        }
        else
        {
            throw new InvalidAirportException("No airports found!");
        }
    }

    public static bool AirportSearch(string searchName, HashSet<AirportModel> airports)
    {
        return (airports.Any(airport => airport.Code.Equals(searchName)));
    }

    public static bool AirlineSearch(string searchName, HashSet<AirlineModel> airlines)
    {
        return airlines.Any(airline => airline.Name == searchName);
    }

    public static FlightModel FlightSearch(string searchName, HashSet<FlightModel> flights)
    {
        IEnumerable<FlightModel> foundFlights = flights.Where(flight => flight.Code.Equals(searchName));

        if (foundFlights.Any()!)
        {
            return foundFlights.FirstOrDefault()!;
        }

        throw new InvalidFlightException($"The flight {searchName} is invalid!");
    }
}
