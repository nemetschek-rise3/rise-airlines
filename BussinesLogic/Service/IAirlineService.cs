﻿using Airlines.Bussines.Dto;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.Service
{
    public interface IAirlineService
    {
        public int GetCount();
        public List<AirlineDto> GetAirlines();

        public List<AirlineDto> GetAirlineFilter(string filter, string value);

        public bool AddAirline(AirlineDto airlineDto);

        public bool DeleteAirlineByFilter(string filter, string value);

        public bool UpdateAirline(int id,AirlineDto airlineDto);
    }
}
