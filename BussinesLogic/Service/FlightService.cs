﻿using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Entities;
using Airlines.Persistance.Basic.Mapper;
using Airlines.Persistance.Basic.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Bussines.Mapper;
using Airlines.Persistance.Basic.Enums;
using Airlines.Bussines.Dto;

namespace Airlines.Bussines.Service
{
    public class FlightService : IFlightService
    {
        private readonly IFlightRepository Repo;
        private readonly IFlightMapper Mapper;

        public FlightService(IFlightRepository flightRepo, IFlightMapper mapper)
        {
            Repo = flightRepo;
            Mapper = mapper;
        }
        public int GetCount()
        {
            return Repo.GetCount();
        }
        public List<FlightDto> GetFlights()
        {
            List<Flight> flights = Repo.GetFlights();
            List<FlightDto> flightDtos = new List<FlightDto>();
            foreach (Flight flight in flights)
            {
                flightDtos.Add(Mapper.MapToFlightDto(flight));
            }

            return flightDtos;
        }

        public List<FlightDto> GetFlightByFilter(string filter, string value)
        {
            List<Flight> flights = Repo.GetFlightsByFilter(filter, value);
            List<FlightDto> flightDtos = new List<FlightDto>();
            foreach (Flight flight in flights)
            {
                flightDtos.Add(Mapper.MapToFlightDto(flight));
            }
            return flightDtos;
        }

        public bool AddFlight(FlightDto flightDto)
        {
            try
            {
                Flight flight = Mapper.MapToFlight(flightDto);
                return Repo.AddFlight(flight);
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message + "There is a problem with mapping flightDto to flight!");
                return false;
            }                   
        }

        public bool DeleteFlightByFilter(string filter, string value)
        {
            return Repo.DeleteFlightByFilter(filter, value);
        }

        public bool UpdateFlight(int id, FlightDto flightDto)
        {
            try
            {
                Flight flight = Mapper.MapToFlight(flightDto);
                return Repo.UpdateFlight(id, flight);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "There is a problem with mapping flightDto to flight!");
                return false;
            }           
        }
    }
}
