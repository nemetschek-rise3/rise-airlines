﻿using Airlines.Bussines.Dto;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.Service
{
    public interface IAirportService
    {
        public int GetCount();
        public List<AirportDto> GetAirports();

        public List<AirportDto> GetAirportFilter(string filter, string value);

        public bool AddAirport(AirportDto airportDto);

        public bool DeleteAirportByFilter(string filter, string value);

        public bool UpdateAirport(int id, AirportDto airportDto);
    }
}
