﻿using Airlines.Bussines.Dto;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Bussines.Service
{
    public interface IFlightService
    {
        public int GetCount();
        public List<FlightDto> GetFlights();

        public List<FlightDto> GetFlightByFilter(string filter, string value);

        public bool AddFlight(FlightDto flightDto);

        public bool DeleteFlightByFilter(string filter, string value);

        public bool UpdateFlight(int id, FlightDto flightDto);
    }
}
