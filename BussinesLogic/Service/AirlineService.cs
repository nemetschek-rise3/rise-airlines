﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Bussines.Dto;
using Airlines.Bussines.Mapper;
using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Entities;
using Airlines.Persistance.Basic.Mapper;
using Airlines.Persistance.Basic.Repository;

namespace Airlines.Bussines.Service
{
    public class AirlineService : IAirlineService
    {
        private readonly IAirlineRepository Repo;
        private readonly IAirlineMapper Mapper;

        public AirlineService(IAirlineRepository airlineRepo,IAirlineMapper mapper)
        {
            Repo = airlineRepo;
            Mapper = mapper;
        }

        public int GetCount()
        {
            return Repo.GetCount();
        }
        public List<AirlineDto> GetAirlines()
        {
            List<Airline> airlines = Repo.GetAirlines();
            List<AirlineDto> airlineDtos = new List<AirlineDto>();
            foreach (Airline airline in airlines)
            {
                airlineDtos.Add(Mapper.MapToAirlineDto(airline));
            }
            return airlineDtos;
        }

        public List<AirlineDto> GetAirlineFilter(string filter, string value)
        {
            List<Airline> airlines = Repo.GetAirlinesByFilter(filter, value);
            List<AirlineDto> airlineDtos = new List<AirlineDto>();
            foreach (Airline airline in airlines)
            {
                airlineDtos.Add(Mapper.MapToAirlineDto(airline));
            }
            return airlineDtos;
        }

        public bool AddAirline(AirlineDto airlineDto)
        {
            try
            {
                Airline airline = Mapper.MapToAirline(airlineDto);
                return Repo.AddAirline(airline);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message + "There is a problem with mapping to airline");
                return false;
            }           
        }

        public bool DeleteAirlineByFilter(string filter, string value)
        {
            return Repo.DeleteAirlineByFilter(filter, value);
        }

        public bool UpdateAirline(int id, AirlineDto airlineDto)
        {
            try
            {
                Airline airline = Mapper.MapToAirline(airlineDto);
                return Repo.UpdateAirline(id, airline);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "There is a problem with mapping to airline");
                return false;
            }
           
        }
    }
}
