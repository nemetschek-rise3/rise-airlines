﻿using Airlines.old.Persistance.Basic.Models;
using Airlines.Persistance.Basic.Entities;
using Airlines.Persistance.Basic.Mapper;
using Airlines.Persistance.Basic.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Bussines.Mapper;
using Airlines.Bussines.Dto;

namespace Airlines.Bussines.Service
{
    public class AirportService : IAirportService
    {
        private readonly IAirportRepository Repo;
        private readonly IAirportMapper Mapper;

        public AirportService(IAirportRepository airportRepo, IAirportMapper mapper)
        {
            Repo = airportRepo;
            Mapper = mapper;
        }

        public int GetCount()
        {
            return Repo.GetCount();
        }
        public List<AirportDto> GetAirports()
        {
            List<Airport> airports = Repo.GetAirports();
            List<AirportDto> airportDtos = new List<AirportDto>();
            foreach (Airport airport in airports)
            {
                airportDtos.Add(Mapper.MapToAirportDto(airport));
            }

            return airportDtos;
        }

        public List<AirportDto> GetAirportFilter(string filter, string value)
        {
            List<Airport> airports = Repo.GetAirportsByFilter(filter, value);
            List<AirportDto> airportDtos = new List<AirportDto>();
            foreach (Airport airport in airports)
            {
                airportDtos.Add(Mapper.MapToAirportDto(airport));
            }
            return airportDtos;
        }

        public bool AddAirport(AirportDto airportDto)
        {
            try
            {
                Airport airport = Mapper.MapToAirport(airportDto);
                return Repo.AddAirport(airport);
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message + "There is a problem with mapping airportDto to airport");
                return false;
            }       
        }

        public bool DeleteAirportByFilter(string filter, string value)
        {
            return Repo.DeleteAirportByFilter(filter, value);
        }

        public bool UpdateAirport(int id, AirportDto airportDto)
        {
            try
            {
                Airport airport = Mapper.MapToAirport(airportDto);
                return Repo.UpdateAirport(id, airport);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "There is a problem with mapping airportDto to airport");
                return false;
            }
        }
    }
}
