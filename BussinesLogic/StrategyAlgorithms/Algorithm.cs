﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.old.Persistance.Basic.Models;

namespace Airlines.Bussines.StrategyAlgorithms
{
    public class Algorithm
    {
        public int StartAirportId { get; set; }
        public int EndAirportId { get; set; }
        public Dictionary<int, int> Parent { get; set; }

        public Algorithm(int startAirportId, int endAirportId)
        {
            StartAirportId = startAirportId;
            EndAirportId = endAirportId;
            Parent = new Dictionary<int, int>();
        }

        public virtual void Execute(Dictionary<int, HashSet<FlightModel>> adjacencyList){}
    }
}
