﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.old.Persistance.Basic.Models;
using RiseAirlines.Persistance.Basic.Exceptions;

namespace Airlines.Bussines.StrategyAlgorithms
{
    public class StopAlgorithm : Algorithm
    {
        public Dictionary<int, int> Stops { get; set; }

        public StopAlgorithm(int startAirportId, int endAirportId) : base(startAirportId, endAirportId)
        {
            Stops = new Dictionary<int, int>();
        }

        public override void Execute(Dictionary<int, HashSet<FlightModel>> adjacencyList)
        {
            if (adjacencyList.Count < 1)
            {
                throw new InvalidFlightException("There is no available flights!");
            }
            foreach (int vertex in adjacencyList.Keys)
            {
                if (vertex == StartAirportId)
                {
                    Stops[vertex] = 0;
                }
                else
                {
                    Stops[vertex] = int.MaxValue;
                }
                Parent[vertex] = 0;
            }
            PriorityQueue<int, int> pq = new PriorityQueue<int, int>();

            pq.Enqueue(StartAirportId, 0);
            while (pq.Count > 0)
            {
                int current = pq.Dequeue();

                if (current.Equals(EndAirportId))
                {
                    break;
                }


                foreach (FlightModel flight in adjacencyList[current])
                {
                    int neighborAirport = flight.ArrivalAirport;
                    int newStops = Stops[current] + 1;
                    if (newStops < Stops[neighborAirport])
                    {

                        Parent[neighborAirport] = current;
                        Stops[neighborAirport] = newStops;
                        pq.Enqueue(neighborAirport, newStops);
                    }
                }
            }
        }
    }
}
