﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.old.Persistance.Basic.Models;
using RiseAirlines.Persistance.Basic.Exceptions;

namespace Airlines.Bussines.StrategyAlgorithms
{
    public class CheapAlgorithm : Algorithm
    {
        public Dictionary<int, int> Price { get; set; }

        public CheapAlgorithm(int startAirportId, int endAirportId) : base(startAirportId, endAirportId)
        {
            Price = new Dictionary<int, int>();
        }

        public override void Execute(Dictionary<int, HashSet<FlightModel>> adjacencyList)
        {
            if (adjacencyList.Count < 1)
            {
                throw new InvalidFlightException("There is no available flights!");
            }

            foreach (int vertex in adjacencyList.Keys)
            {
                if (vertex == StartAirportId)
                {
                    Price[vertex] = 0;
                }
                else
                {
                    Price[vertex] = int.MaxValue;
                }

                Parent[vertex] = 0;
            }

            PriorityQueue<int, int> pq = new PriorityQueue<int, int>();

            pq.Enqueue(StartAirportId, 0);
            while (pq.Count > 0)
            {
                int current = pq.Dequeue();

                if (current.Equals(EndAirportId))
                {
                    break;
                }


                foreach (FlightModel flight in adjacencyList[current])
                {
                    int neighborAirport = flight.ArrivalAirport;
                    int newPrice = Price[current] + flight.Price;
                    if (newPrice < Price[neighborAirport])
                    {

                        Parent[neighborAirport] = current;
                        Price[neighborAirport] = newPrice;
                        pq.Enqueue(neighborAirport, newPrice);
                    }
                }
            }
        }
    }
}
