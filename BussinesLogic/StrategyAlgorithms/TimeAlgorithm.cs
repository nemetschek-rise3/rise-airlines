﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.old.Persistance.Basic.Models;
using RiseAirlines.Persistance.Basic.Exceptions;

namespace Airlines.Bussines.StrategyAlgorithms
{
    public class TimeAlgorithm : Algorithm
    {
        public Dictionary<int, int> Times { get; set; }

        public TimeAlgorithm(int startAirportId, int endAirportId) : base(startAirportId, endAirportId)
        {
            Times = new Dictionary<int, int>();
        }

        public override void Execute(Dictionary<int, HashSet<FlightModel>> adjacencyList)
        {
            if (adjacencyList.Count < 1)
            {
                throw new InvalidFlightException("There is no available flights!");
            }

            foreach (int vertex in adjacencyList.Keys)
            {
                if (vertex == StartAirportId)
                {
                    Times[vertex] = 0;
                }
                else
                {
                    Times[vertex] = int.MaxValue;
                }
                Parent[vertex] = 0;
            }
            PriorityQueue<int, int> pq = new PriorityQueue<int, int>();

            pq.Enqueue(StartAirportId, 0);
            while (pq.Count > 0)
            {
                int current = pq.Dequeue();

                if (current.Equals(EndAirportId))
                {
                    break;
                }


                foreach (FlightModel flight in adjacencyList[current])
                {
                    int neighborAirport = flight.ArrivalAirport;
                    int newTime = flight.Time + Times[current];
                    if (newTime < Times[neighborAirport])
                    {

                        Parent[neighborAirport] = current;
                        Times[neighborAirport] = newTime;
                        pq.Enqueue(neighborAirport, newTime);
                    }
                }
            }
        }
    }
}
