﻿using Microsoft.VisualBasic.FileIO;
using RiseAirlines.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Airlines.Console
{
    public class CsvReader
    {
        public static bool AllFieldsHaveValue(object obj)
        {
            Type type = obj.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                object value = property.GetValue(obj)!;

                if (value == null || (value.GetType().IsValueType && value.Equals(Activator.CreateInstance(value.GetType()))))
                {
                    return false;
                }
            }

            return true;
        }
        public static void ReadAirportCsvFile(string path, HashSet<Airport> airports)
        {
            using (StreamReader reader = new StreamReader(path))
            {
                bool isHeader = true;
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine()!;
                    string[] values = line.Split(';')!;

                    if (!isHeader)
                    {
                        Airport airport = new Airport(values[0], values[1], values[2], values[3]);
                        if (AllFieldsHaveValue(airport))
                        {
                            airports.Add(airport);
                        }
                    }
                    isHeader = false;
                }
            }
        }
        public static void ReadAirlineCsvFile(string path, HashSet<Airline> airlines)
        {
            using (StreamReader reader = new StreamReader(path))
            {
                bool isHeader = true;
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine()!;
                    string[] values = line.Split(';')!;

                    if (!isHeader)
                    {
                        Airline airline = new Airline(values[0], values[1]);
                        if (AllFieldsHaveValue(airline))
                        {
                            airlines.Add(airline);
                        }
                    }
                    isHeader = false;
                }
            }
        }

        public static void ReadFlightCsvFile(string path, HashSet<Flight> flights)
        {
            using (StreamReader reader = new StreamReader(path))
            {
                bool isHeader = true;
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine()!;
                    string[] values = line.Split(';')!;

                    if (!isHeader)
                    {
                        Flight flight = new Flight(values[0], values[1], values[2]);
                        if (AllFieldsHaveValue(flight))
                        {
                            
                            flights.Add(flight);
                        }
                    }
                    isHeader = false;
                }
            }
        }
    }
}
