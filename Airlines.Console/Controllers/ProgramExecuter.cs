﻿using Airlines.Bussines.DataStructures;
using Airlines.Bussines.Entities;
using Airlines.Bussines.Reservations;
using BussinesLogic.SearchAndSort;
using BussinesLogic.Validation;
using RiseAirlines.Commands;
using RiseAirlines.Entities;
using RiseAirlines.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Controllers;
public class ProgramExecuter
{
    public static void Start(EntitiesDto dto)
    {
        bool continuing = true;
        RouteLinkedList route = new RouteLinkedList();
        List<Reservation> reservations = [];
        List<AirportNode> roots = new List<AirportNode>();
        while (continuing)
        {
            Console.WriteLine("list a command (help)");
            string inputLine = Console.ReadLine()!;
            string[] inputs = inputLine.Split(' ');

            try
            {
                switch (inputs[0])
                {
                    case "batch":
                        BatchCommand batchCommand = BatchCommand.CreateBatchCommand(inputs, dto, route, reservations);
                        CommandExecuter(batchCommand);
                        break;

                    case "close":
                        continuing = false;
                        break;

                    case "help":

                        HelpCommand helpCommand = HelpCommand.CreateHelpCommand(inputs);
                        CommandExecuter(helpCommand);
                        break;

                    case "list":
                        ListCommand listCommand = ListCommand.CreateListCommand(inputs, dto);
                        CommandExecuter(listCommand);
                        break;

                    case "exist":

                        ExistCommand existCommand = ExistCommand.CreateExistCommand(inputs, dto);
                        CommandExecuter(existCommand);
                        break;

                    case "sort":
                        SortCommand sortCommand = SortCommand.CreateSortCommand(inputs, dto);
                        CommandExecuter(sortCommand);
                        break;

                    case "route":
                        RouteCommand routeCommand = RouteCommand.CreateRouteCommand(inputs, route, dto);
                        CommandExecuter(routeCommand);
                        break;

                    case "print":

                        PrintCommand printCommand = PrintCommand.CreatePrintCommand(inputs, dto, route, reservations);
                        CommandExecuter(printCommand);
                        break;

                    case "reserve":
                        ReserveCommand reserveCommand = ReserveCommand.CreateReserveCommand(inputs, dto, reservations);
                        CommandExecuter(reserveCommand);
                        break;

                    default:
                        throw new InvalidCommandException("Invalid command");
                }
            }
            catch (InvalidCommandException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    public static void CommandExecuter(Command command)
    {

        if (command.IsValid())
            command.Execute();
    }

    public static Command FindCommand(string inputLine, EntitiesDto dto, RouteLinkedList route, List<Reservation> reservations)
    {
        string[] inputs = inputLine.Split(' ');

        switch (inputs[0])
        {
            case "help":

                HelpCommand helpCommand = HelpCommand.CreateHelpCommand(inputs);
                return helpCommand;

            case "list":
                ListCommand listCommand = ListCommand.CreateListCommand(inputs, dto);
                return listCommand;

            case "exist":

                ExistCommand existCommand = ExistCommand.CreateExistCommand(inputs, dto);
                return existCommand;

            case "sort":
                SortCommand sortCommand = SortCommand.CreateSortCommand(inputs, dto);
                return sortCommand;

            case "route":
                RouteCommand routeCommand = RouteCommand.CreateRouteCommand(inputs, route, dto);
                return routeCommand;

            case "print":

                PrintCommand printCommand = PrintCommand.CreatePrintCommand(inputs, dto, route, reservations);
                return printCommand;

            case "reserve":
                ReserveCommand reserveCommand = ReserveCommand.CreateReserveCommand(inputs, dto, reservations);
                return reserveCommand;

            default:
                Console.WriteLine("Invalid command");
                Command emptyCommand = Command.CreateCommand();
                return emptyCommand;
        }
    }
}
