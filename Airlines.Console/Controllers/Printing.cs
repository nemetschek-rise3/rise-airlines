﻿using Airlines.Bussines.Aircrafts;
using Airlines.Bussines.DataStructures;
using Airlines.Bussines.Entities;
using Airlines.Bussines.Reservations;
using RiseAirlines.Entities;
using RiseAirlines.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Controllers;
public static class Printing
{
    public static void PrintAirports(HashSet<Airport> airports)
    {
        Console.WriteLine("Names of airports are: ");
        foreach (Airport airport in airports)
            Console.WriteLine(airport.Name);
    }

    public static void PrintAirlines(HashSet<Airline> airlines)
    {
        Console.WriteLine("Names of airlines are: ");
        foreach (Airline airline in airlines)
            Console.WriteLine(airline.Name);
    }

    public static void PrintFlights(HashSet<Flight> flights)
    {
        Console.WriteLine("Names of flights are: ");
        foreach (Flight flight in flights)
            Console.Write($"{flight.Id} {flight.DepartureAirport} {flight.ArrivalAirport}\n");
    }

    public static void PrintRoute(RouteLinkedList route)
    {
        Node current = route.head!;

        if (route.head == null)
            throw new InvalidCommandException("The route is empty");
        else
        {
            Console.WriteLine("Route airports are: ");
            while (current != null)
            {
                Console.WriteLine($"{current.Flight!.DepartureAirport} - {current.Flight.ArrivalAirport}");
                current = current.Next!;
            }
        }
    }
    public static void PrintAircrafts(AircraftDto aircraftDto)
    {
        HashSet<CargoAircraft> cargoAircrafts = aircraftDto.CargoAircrafts!;
        HashSet<PassengerAircraft> passengerAircrafts = aircraftDto.PassengerAircrafts!;
        foreach (var cargoAircraft in cargoAircrafts!)
            Console.WriteLine(cargoAircraft.Model);
        foreach (var passengerAircraft in passengerAircrafts)
            Console.WriteLine(passengerAircraft.Model);
    }

    public static void PrintReservations(List<Reservation> reservations)
    {

        foreach (Reservation reservation in reservations)
            reservation.DisplayInfo();
    }

    public static void PrintOutput(HashSet<Airport> airports, HashSet<Airline> airlines, HashSet<Flight> flights)
    {
        PrintAirports(airports);
        PrintAirlines(airlines);
        PrintFlights(flights);
    }

    public static void PrintEntities(string[] inputs, EntitiesDto dto, RouteLinkedList route, List<Reservation> reservations)
    {
        if (inputs[1].Equals("airports"))
            PrintAirports(dto.Airports);
        else if (inputs[1].Equals("airlines"))
            PrintAirlines(dto.Airlines);
        else if (inputs[1].Equals("flights"))
            PrintFlights(dto.Flights);
        else if (inputs[1].Equals("aircrafts"))
            PrintAircrafts(dto.AircraftDto!);
        else if (inputs[1].Equals("reservations"))
            PrintReservations(reservations);
        else if (inputs[1].Equals("route"))
            PrintRoute(route);
        else
            throw new InvalidCommandException("Invalid input, enter only airports, airlines, flights, aircrafts!");
    }

    public static void PrintHelpCommands(List<string> commands)
    {
        foreach (string command in commands)
            Console.WriteLine(command);
    }
    public static void PrintTreeRoute(List<string> route)
    {
        int count = 1;
        Console.Write("Found route is: ");
        foreach (string airport in route)
            if (count < route.Count)
            {
                Console.Write($"{airport} - ");
                count++;
            }
            else
                Console.Write($"{airport}");
        Console.WriteLine();
    }
}
