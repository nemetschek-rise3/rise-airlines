﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Commands;
public class Command : ICommand
{

    public string[]? Commands { get; set; }

    public Command() { }
    public Command(string[] commands)
    {
        Commands = commands;
    }

    public static Command CreateCommand(string[] commands)
    {
        return new Command(commands);
    }
    public static Command CreateCommand()
    {
        return new Command();
    }

    public virtual bool IsValid()
    {
        return true;
    }
    public virtual void Execute() { }
}
