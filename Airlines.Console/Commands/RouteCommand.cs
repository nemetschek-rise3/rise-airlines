﻿using Airlines.Bussines.DataStructures;
using Airlines.Bussines.Entities;
using BussinesLogic.SearchAndSort;
using RiseAirlines.Controllers;
using Rise_Airlines.EntitiesService;
using RiseAirlines.Entities;
using RiseAirlines.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Commands;
public class RouteCommand : Command
{
    public RouteLinkedList Route { get; set; }
    public EntitiesDto Entities { get; set; }


    public RouteCommand(string[] commands, RouteLinkedList route, EntitiesDto entities) : base(commands)
    {
        Route = route;
        Entities = entities;

    }

    public static RouteCommand CreateRouteCommand(string[] commands, RouteLinkedList route, EntitiesDto entities)
    {
        return new RouteCommand(commands, route, entities);
    }

    public override bool IsValid()
    {
        try
        {
            if (Commands!.Length is < 2 or > 4)
            {
                throw new InvalidRouteException("Invalid input. Enter command to route!");
            }
            if (Route is null)
            {
                throw new InvalidRouteException("The route is null!");
            }
        }
        catch (InvalidRouteException ex)
        {
            Console.WriteLine(ex.Message);
            return false;
        }
        return true;
    }

    public override void Execute()
    {
        try
        {
            switch (Commands![1])
            {
                case "new":

                    if (Commands.Length == 2)
                    {
                        Console.WriteLine("You created a new route! Select from the commands below!");
                        Route.Created = true;
                        Route.Clear();
                    }
                    else
                    {
                        throw new InvalidCommandException("Invalid command - did you mean route new? The command should be only 2 words!");
                    }
                    break;

                case "add":
                    if (Commands.Length is not 3)
                    {
                        throw new InvalidCommandException("Invalid input. Enter flight Id or too many words in the command!");
                    }
                    if (Route.Created == false)
                    {
                        throw new InvalidCommandException("There is not route created, first write command route new!");
                    }

                    Route.AddFlight(Search.FlightSearch(Commands[2], Entities.Flights));
                    Console.WriteLine($"You successfully added {Commands[2]} to the route!");

                    break;

                case "remove":
                    if (Route.IsEmpty())
                    {
                        throw new InvalidCommandException("There is not route created, first write command route new!");
                    }
                    Route.RemoveFlight();
                    Console.WriteLine("You successfully removed last flight from the route!");
                    break;

                case "find":
                    if (Commands.Length is not 4)
                    {
                        throw new InvalidCommandException("Enter the starting airport and the destination airport!");
                    }
                    string rootAirport = Commands[2];
                    switch (rootAirport)
                    {
                        case "CPT":
                            FindAndPrintRoute(".\\Routes\\CPTRoute.csv");
                            break;
                        case "DXB":
                            FindAndPrintRoute(".\\Routes\\DXBRoute.csv");
                            break;
                        case "HND":
                            FindAndPrintRoute(".\\Routes\\HNDRoute.csv");
                            break;
                        case "LAX":
                            FindAndPrintRoute(".\\Routes\\LAXRoute.csv");
                            break;
                        case "LHR":
                            FindAndPrintRoute(".\\Routes\\LHRRoute.csv");
                            break;
                        case "SYD":
                            FindAndPrintRoute(".\\Routes\\SYDRoute.csv");
                            break;
                        default:
                            throw new InvalidCommandException("There is no such root airport!");
                    }
                    break;

                default:
                    throw new InvalidCommandException("Invalid route command!");
            }
        }
        catch (InvalidCommandException ex)
        {
            Console.WriteLine(ex.Message);
        }
        catch (InvalidRouteException ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
    public List<String> FindRoute(string path, string destinationAirport)
    {

        ArrayList treeFlights = FlightService.ReadFlightsFromCsv(path, Entities.Flights);
        FlightRouteTree flightRouteTree = new FlightRouteTree();
        AirportNode root = flightRouteTree.BuildTreeFromCSV(treeFlights);
        AirportNode destinationNode = flightRouteTree.DFS(root, destinationAirport);
        List<String> route = flightRouteTree.FindDestinationRoute(destinationNode, root);
        return route;
    }

    public void FindAndPrintRoute(string path)
    {
        List<String> route = FindRoute(path,Commands![3]);
        Printing.PrintTreeRoute(route);
    }

}


