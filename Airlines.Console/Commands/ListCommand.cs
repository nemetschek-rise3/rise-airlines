﻿using Airlines.Bussines.Entities;
using BussinesLogic.SearchAndSort;
using RiseAirlines.Exceptions;
using RiseAirlines.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RiseAirlines.Entities;

namespace RiseAirlines.Commands;
public class ListCommand : Command
{
    private ListCommand(string[] commands, EntitiesDto entitiesDto) : base(commands)
    {
        EntitiesDto = entitiesDto;
    }

    public EntitiesDto? EntitiesDto { get; set; }

    public static ListCommand CreateListCommand(string[] inputs, EntitiesDto entitiesDto)
    {
        return new ListCommand(inputs, entitiesDto);
    }

    public override bool IsValid()
    {
        try
        {
            if (Commands!.Length is not 3)
            {
                throw new InvalidCommandException("Command should be 3 words - list <from> <inputdata>");
            }

            if (!Commands![1].Equals("city") && !Commands[1].Equals("country"))
            {
                throw new InvalidCommandException("Wrong input - enter city or country");
            }
            if (EntitiesDto!.Airports.Count == 0)
            {
                throw new InvalidAirportException("There is no airports!");
            }
        }
        catch (InvalidCommandException ex)
        {
            Console.WriteLine(ex.Message);
            return false;
        }
        catch (InvalidAirportException ex)
        {
            Console.WriteLine(ex.Message);
            return false;
        }
        return true;
    }

    public override void Execute()
    {
        try
        {
            if (Commands![1].Equals("city"))
            {
                IEnumerable<Airport> cities = Search.AirportCitySearch(Commands[2], EntitiesDto!.Airports);
                foreach (Airport c in cities)
                {
                    Console.WriteLine(c.Name);
                }
            }
            else
            {
                IEnumerable<Airport> countries = Search.AirportCountrySearch(Commands[2], EntitiesDto!.Airports);
                foreach (Airport c in countries)
                {
                    Console.WriteLine(c.Name);
                }
            }
        }

        catch (InvalidAirportException ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
}
