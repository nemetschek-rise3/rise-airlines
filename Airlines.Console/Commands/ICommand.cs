﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Commands
{
    public interface ICommand
    {

        void Execute();
        bool IsValid();
    }
}
