﻿using Airlines.Bussines.Entities;
using Airlines.Bussines.Reservations;
using BussinesLogic.Validation;
using RiseAirlines.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Commands;
public class ReserveCommand : Command
{
    public List<Reservation> Reservations { get; set; }
    public EntitiesDto Entities { get; set; }

    private ReserveCommand(string[] commands, EntitiesDto entities, List<Reservation> reservations) : base(commands)
    {
        Entities = entities;
        Reservations = reservations;
    }

    public static ReserveCommand CreateReserveCommand(string[] inputs, EntitiesDto entities, List<Reservation> reservations)
    {
        return new ReserveCommand(inputs, entities, reservations);
    }

    public override bool IsValid()
    {
        try
        {
            if (Commands!.Length is < 5 or > 6)
            {
                throw new InvalidCommandException("You haven't put enough data for reservation!");
            }
            if (Reservations is null)
            {
                throw new InvalidCommandException("Reservations are null!");
            }
        }
        catch (InvalidCommandException ex)
        {
            Console.WriteLine(ex.Message);
            return false;
        }
        return true;
    }

    public override void Execute()
    {
        try
        {
            if (Commands![1].Equals("ticket"))
            {
                if (Commands.Length is 5)
                {
                    throw new InvalidCommandException("You forgot one field to reserve a ticket, try again!");
                }
                else if (Validation.ValidatePassengerTicket(Commands, Entities.Flights))
                {
                    Reservation reservation = PassengerReservation.CreatePassengerReservation(Commands[2], int.Parse(Commands[3]), int.Parse(Commands[4]), int.Parse(Commands[5]));
                    Reservations.Add(reservation);
                    Console.WriteLine("You successfuly created a reservation!");
                    reservation.DisplayInfo();
                }
            }

            else if (Commands[1].Equals("cargo"))
            {
                if (Commands.Length is 6)
                {
                    throw new InvalidCommandException("You added one additional field to reserve a cargo, try again!");
                }
                else if (Validation.ValidateCargoTicket(Commands, Entities.Flights))
                {
                    CargoReservation reservation = new CargoReservation(Commands[2], int.Parse(Commands[3]), int.Parse(Commands[4]));
                    Reservations.Add(reservation);
                    Console.WriteLine("You successfuly created a reservation!");
                    reservation.DisplayInfo();
                }
            }
            else
            {
                throw new InvalidCommandException("Invalid input - you can only reserve ticket or cargo!");
            }
        }
        catch (InvalidCommandException ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
}
