﻿using Airlines.Bussines.Entities;
using BussinesLogic.SearchAndSort;
using RiseAirlines.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Commands;
public class ExistCommand : Command
{
    private ExistCommand(string[] commands, EntitiesDto entitiesDto) : base(commands)
    {
        EntitiesDto = entitiesDto;
    }

    public EntitiesDto EntitiesDto { get; set; }

    public static ExistCommand CreateExistCommand(string[] inputs, EntitiesDto entitiesDto)
    {
        return new ExistCommand(inputs, entitiesDto);
    }

    public override bool IsValid()
    {
        try
        {
            if (Commands!.Length is not 2)
            {
                throw new InvalidCommandException("The command should be from 2 words! exist <airline name>");
            }
            if (EntitiesDto.Airlines.Count == 0)
            {
                throw new InvalidAirlineException("There is no airlines!");
            }
        }
        catch (InvalidCommandException ex)
        {
            Console.WriteLine(ex.Message);
            return false;
        }
        catch (InvalidAirlineException ex)
        {
            Console.WriteLine(ex.Message);
            return false;
        }
        return true;
    }
    public override void Execute()
    {
        Console.WriteLine(Search.AirlineSearch(Commands![1], EntitiesDto.Airlines));

    }
}
