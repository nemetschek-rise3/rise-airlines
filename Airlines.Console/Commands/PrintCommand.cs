﻿using Airlines.Bussines.DataStructures;
using Airlines.Bussines.Entities;
using Airlines.Bussines.Reservations;
using RiseAirlines.Controllers;
using RiseAirlines.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Commands;
public class PrintCommand : Command
{
    public EntitiesDto Entities { get; set; }
    public RouteLinkedList Route { get; set; }
    public List<Reservation> Reservations { get; set; }

    private PrintCommand(string[] commands, EntitiesDto entities, RouteLinkedList route, List<Reservation> reservations) : base(commands)
    {
        Entities = entities;
        Route = route;
        Reservations = reservations;
    }

    public static PrintCommand CreatePrintCommand(string[] inputs, EntitiesDto entities, RouteLinkedList route, List<Reservation> reservations)
    {
        return new PrintCommand(inputs, entities, route, reservations);
    }

    public override bool IsValid()
    {
        try
        {
            if (Commands!.Length < 2)
            {
                throw new InvalidCommandException("Enter entity to print!");
            }
            if (Commands!.Length > 2)
            {
                throw new InvalidCommandException("Invalid input - the command should contain only 2 words! Did you mean print <entity>?");
            }
        }
        catch (InvalidCommandException ex)
        {
            Console.WriteLine(ex.Message);
            return false;
        }
        return true;
    }

    public override void Execute()
    {
        Printing.PrintEntities(Commands!, Entities, Route, Reservations);
    }
}
