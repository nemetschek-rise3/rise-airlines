﻿using RiseAirlines.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Commands
{
    public class HelpCommand : Command
    {
        private HelpCommand(string[] commands) : base(commands)
        {
        }

        public static HelpCommand CreateHelpCommand(string[] inputs)
        {
            return new HelpCommand(inputs);
        }

        public override bool IsValid()
        {
            if (Commands!.Length is 1)
            {
                return true;
            }
            else
            {
                try
                {
                    throw new InvalidCommandException("Command is longer than 1 word, did you mean 'help'");
                }
                catch (InvalidCommandException ex)
                {
                    Console.WriteLine(ex.Message);
                    return false;

                }
            }
        }

        public override void Execute()
        {
            HelpReader("DefaultHelpCommands.csv");
        }

        public static void HelpReader(string path)
        {
            using (StreamReader reader = new StreamReader(path))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine()!;
                    string[] words = line.Split(' ');
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(words[0] + " ");
                    Console.ResetColor();
                    for (int i = 1; i < words.Length; i++)
                    {
                        Console.Write(words[i] + " ");
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
