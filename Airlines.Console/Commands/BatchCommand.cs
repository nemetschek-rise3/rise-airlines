﻿using Airlines.Bussines.DataStructures;
using Airlines.Bussines.Entities;
using Airlines.Bussines.Reservations;
using RiseAirlines.Controllers;
using RiseAirlines.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseAirlines.Commands;
public class BatchCommand : Command
{

    public EntitiesDto Entities { get; set; }
    public RouteLinkedList Route { get; set; }
    public List<Reservation> Reservations { get; set; }
    private BatchCommand(string[] commands, EntitiesDto entities, RouteLinkedList route, List<Reservation> reservations) : base(commands)
    {
        Entities = entities;
        Route = route;
        Reservations = reservations;

    }

    public static BatchCommand CreateBatchCommand(string[] commands, EntitiesDto entities, RouteLinkedList route, List<Reservation> reservations)
    {
        return new BatchCommand(commands, entities, route, reservations);
    }

    public override bool IsValid()
    {
        try
        {
            if (!Commands![0].Equals("batch") || Commands!.Length is not 2)
            {
                throw new InvalidCommandException("Invalid batch command. The command should be 2 words - ex : batch run! ");
            }
        }
        catch (InvalidCommandException ex)
        {
            Console.WriteLine(ex.Message);
            return false;
        }
        try
        {
            if (!Commands[1].Equals("start"))
            {
                throw new InvalidCommandException("Invalid batch command. First execute batch start to start the batch mode!");
            }
        }
        catch (InvalidCommandException ex)
        {
            Console.WriteLine(ex.Message);
            return false;
        }
        return true;
    }

    public override void Execute()
    {
        string inputLine;
        Queue<Command> batchCommands = new Queue<Command>();

        string batchCommand;
        while (true)
        {
            Console.WriteLine("Batch mode: list a command to add to the batch: (help)");
            inputLine = Console.ReadLine()!;

            if (inputLine.Equals("batch run") || inputLine.Equals("batch close"))
            {
                batchCommand = inputLine;
                break;
            }
            if (inputLine.Equals("help"))
            {
                HelpCommand.HelpReader("DefaultHelpCommands.csv");
            }
            else
            {
                Command currentCommand = ProgramExecuter.FindCommand(inputLine, Entities, Route, Reservations);

                if (currentCommand.Commands is not null)
                {
                    batchCommands.Enqueue(currentCommand);
                }
            }
        }
        if (batchCommand.Equals("batch run"))
        {
            foreach (Command command in batchCommands)
            {
                if (command.IsValid())
                {
                    command.Execute();
                }
            }
        }
    }
}
