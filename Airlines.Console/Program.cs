﻿using Airlines.Bussines;
using Airlines.Bussines.Aircrafts;
using Airlines.Bussines.Entities;
using BussinesLogic.SearchAndSort;
using RiseAirlines.Controllers;
using Rise_Airlines.EntitiesService;
using RiseAirlines.Entities;
using System.Reflection;

namespace RiseAirlines;
public class Program
{
    public static void Main(string[] args)
    {
        ArgumentNullException.ThrowIfNull(args);


    
        HashSet<Airport> airports = AirportsService.CreateAirports("Airports.csv");
        HashSet<Airline> airlines = AirlineService.CreateAirlines("Airlines.csv");
        HashSet<Flight> flights = FlightService.CreateFlights("Flights.csv");
        AircraftDto aircraftDto = AircraftService.CreateAircrafts("Aircrafts.csv");
        EntitiesDto entityDto = new EntitiesDto(airports, airlines, flights, aircraftDto);

        //Search.SearchManager(airports, airlines);
        //dto = Sort.Sorting(dto);
        //airports = dto.Airports;
        //airlines = dto.Airlines;
        //flights = dto.Flights;
        //RouteLinkedList route = FlightService.FlightRouteManager(flights);
        //Printing.PrintRoute(route);
        //Printing.PrintOutput(airports, airlines, flights);

        ProgramExecuter.Start(entityDto);
    }
}
