#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
USER app
WORKDIR /app
EXPOSE 8080

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["AirlinesWeb/AirlinesWeb.csproj", "AirlinesWeb/"]
COPY ["BussinesLogic/Airlines.Bussines.csproj", "BussinesLogic/"]
COPY ["Airlines.Persistance.Basic/Airlines.Persistance.Basic.csproj", "Airlines.Persistance.Basic/"]
RUN dotnet restore "./AirlinesWeb/AirlinesWeb.csproj"
COPY . .
WORKDIR "/src/AirlinesWeb"
RUN dotnet build "./AirlinesWeb.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "./AirlinesWeb.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM mcr.microsoft.com/mssql/server AS mssql
ENV SA_PASSWORD=adragnev
ENV ACCEPT_EULA=Y

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "AirlinesWeb.dll"]