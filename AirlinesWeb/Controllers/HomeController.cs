using Airlines.Bussines.Dto;
using Airlines.Bussines.Service;
using Airlines.Persistance.Basic.Entities;
using Airlines.Persistance.Basic.Repository;
using AirlinesWeb.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace AirlinesWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IAirportService _airportService;
        private readonly IAirlineService _airlineService;
        private readonly IFlightService _flightService;

       
        public HomeController
            (ILogger<HomeController> logger, IAirportService airportService, IAirlineService airlineService, IFlightService flightService)
        {
            _logger = logger;
            _airportService = airportService;
            _airlineService = airlineService;
            _flightService = flightService;
        }

        public IActionResult Index()
        {
            int airpCount = _airportService.GetCount();
            int airlCount = _airlineService.GetCount();
            int flightCount = _flightService.GetCount();
            CountDto countDto = new CountDto(airpCount, airlCount, flightCount);
            return View(countDto);
        }

        


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
