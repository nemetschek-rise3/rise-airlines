﻿using Airlines.Bussines.Dto;
using Airlines.Bussines.Service;
using Airlines.Persistance.Basic.Entities;
using Airlines.Persistance.Basic.Repository;
using Microsoft.AspNetCore.Mvc;
using X.PagedList;

namespace AirlinesWeb.Controllers
{
    public class AirlinesController : Controller
    {
        private readonly IAirlineService _airlineService;

        public AirlinesController(IAirlineService airlineService)
        {
            _airlineService = airlineService;
        }
        public IActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            List<AirlineDto> airlines = _airlineService.GetAirlines();
            return View(airlines.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public IActionResult AddNewAirline(AirlineDto airline, int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            if (ModelState.IsValid)
            {
                TempData["Successful"] = "Airline has been added successfully!";
                _airlineService.AddAirline(airline);
                return RedirectToAction("Index");
            }
            else
            {
                TempData["Unsuccessful"] = "Unvalid airline details!";
                List<AirlineDto> airlines = _airlineService.GetAirlines();
                return View("Index", airlines.ToPagedList(pageNumber, pageSize));
            }
        }


        [HttpPost]
        public IActionResult SearchAirline(SearchDto searchDto, int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            if (ModelState.IsValid)
            {
                List<AirlineDto> airlines = _airlineService.GetAirlineFilter(searchDto.Filter, searchDto.SearchInput);
                return View("Index", airlines.ToPagedList(pageNumber, pageSize));
            }
            else
            {
                
                List<AirlineDto> airlines = _airlineService.GetAirlines();
                return View("Index", airlines.ToPagedList(pageNumber, pageSize));
            }
        }
    }
}
