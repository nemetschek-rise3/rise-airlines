﻿using Airlines.Bussines.Dto;
using Airlines.Bussines.Service;
using Airlines.Persistance.Basic.Entities;
using Airlines.Persistance.Basic.EntitiesService;
using Airlines.Persistance.Basic.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Drawing.Printing;
using X.PagedList;

namespace AirlinesWeb.Controllers
{
    public class AirportsController : Controller
    {
        private readonly IAirportService _airportService;

        public AirportsController(IAirportService airportService)
        {
            _airportService = airportService;
        }
        public IActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            List<AirportDto> airports = _airportService.GetAirports();
            return View(airports.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public IActionResult AddNewAirport(AirportDto airportDto, int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            if (ModelState.IsValid)
            {
                _airportService.AddAirport(airportDto);
                TempData["Successful"] = "Airport has been added successfully!";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["Unsuccessful"] = "Unvalid airport details!";
                List<AirportDto> airports = _airportService.GetAirports();
                return View("Index", airports.ToPagedList(pageNumber, pageSize));
            }
        }

        [HttpPost]
        public IActionResult SearchAirport(SearchDto searchDto, int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            if (ModelState.IsValid)
            {
                List<AirportDto> airports = _airportService.GetAirportFilter(searchDto.Filter, searchDto.SearchInput);
                return View("Index", airports.ToPagedList(pageNumber, pageSize));
            }
            else
            {
                List<AirportDto> airports = _airportService.GetAirports();
                return View("Index", airports.ToPagedList(pageNumber, pageSize));
            }
        }
    }
}
