﻿using Airlines.Bussines.Dto;
using Airlines.Bussines.Service;
using Airlines.Persistance.Basic.Entities;
using Airlines.Persistance.Basic.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Drawing.Printing;
using X.PagedList;

namespace AirlinesWeb.Controllers
{
    public class FlightsController : Controller
    {
        private readonly IFlightService _flightService;
        public FlightsController(IFlightService flightService)
        {
            _flightService = flightService;
        }

        public IActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            List<FlightDto> flights = _flightService.GetFlights();
            return View(flights.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public IActionResult AddNewFlight(FlightDto flight, int? page)
        {
            if(ModelState.IsValid)
            {
                TempData["Successful"] = "Flight has been added successfully!";
                _flightService.AddFlight(flight);
                return RedirectToAction("Index");
            }
            else
            {
                TempData["Unsuccessful"] = "Unvalid flight details!";
                int pageSize = 10;
                int pageNumber = (page ?? 1);
                List<FlightDto> flights = _flightService.GetFlights();
                return View("Index",flights.ToPagedList(pageNumber, pageSize));
            }
            
        }

        [HttpPost]
        public IActionResult SearchFlight(SearchDto searchDto, int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            if (ModelState.IsValid)
            {
                List<FlightDto> flights = _flightService.GetFlightByFilter(searchDto.Filter, searchDto.SearchInput);
                return View("Index", flights.ToPagedList(pageNumber, pageSize));
            }
            else
            {
                List<FlightDto> flights = _flightService.GetFlights();
                return View("Index", flights.ToPagedList(pageNumber, pageSize));
            }
        }
    }
}
