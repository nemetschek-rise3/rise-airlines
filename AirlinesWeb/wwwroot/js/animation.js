document.addEventListener('DOMContentLoaded', function () {
    let successMessage = document.getElementById("success");
    if (successMessage) {
        addConfetti();
    }
   
    function addConfetti() {
        let end = Date.now() + 5 * 1000;

        (function frame() {
            confetti({
                particleCount: 5,
                spread: 70,
                origin: { x: 0 },
            });
            confetti({
                particleCount: 5,
                spread: 70,
                origin: { x: 1 },
            });

            if (Date.now() < end) {
                requestAnimationFrame(frame);
            }
        }());
    }
});