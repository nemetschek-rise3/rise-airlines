document.addEventListener("DOMContentLoaded", function () {

    const form = document.getElementById("form");
    const code = document.getElementById("code");
    const departureId = document.getElementById("from");
    const arrivalId = document.getElementById("to");
    const departureDateTime = document.getElementById("departure");
    const arrivalDateTime = document.getElementById("arrival");
    const submitButton = document.getElementById("submitBut")
 
    const codeErrorMessage = document.getElementById("valid-code")
    const departureIdErrorMessage = document.getElementById("valid-depId")
    const arrivalIdErrorMessage = document.getElementById("valid-arrId")
    const departureDtErrorMessage = document.getElementById("valid-depDt")
    const arrivalDtErrorMessage = document.getElementById("valid-arrDt")


    const onlyLettersRegex = /^[a-zA-Z]+$/;
    const empty = "";

    submitButton.disabled = true;
    code.addEventListener("blur", validate);
    departureId.addEventListener("blur", validate);
    arrivalId.addEventListener("blur", validate);
    departureDateTime.addEventListener("blur", validate);
    arrivalDateTime.addEventListener("blur", validate);



    form.querySelectorAll('input').forEach(input => {
        input.addEventListener("input", function () {
            formValidation(form);
        });
    });

    function formValidation(form) {
        submitButton.disabled = false;

        form.querySelectorAll('input').forEach(input => {
            const value = input.value.trim();
            const inputName = input.name;

            if (
                (inputName === "code" && !validateCode(value)) ||
                (inputName === "from" && !validateDepartureId(value)) ||
                (inputName === "to" && !validateArrivalId(value)) ||
                (inputName === "departure" && !validateDepartureDt(value)) ||
                (inputName === "arrival" && !validateArrivalDt(value)))
             {
                submitButton.disabled = true;

            }
        });
    }

    function validate(e) {

        if (e.target.name === "code") {
            if (!validateCode(e.target.value)) {
                codeErrorMessage.textContent = "Field is empty or invalid!"
            } else {
                nameErrorMessage.textContent = ""
            }
        }
        if (e.target.name === "from") {
            if (!validateDepartureId(e.target.value)) {
                departureIdErrorMessage.textContent = "Field is empty or invalid!"
            } else {
                departureIdErrorMessage.textContent = ""
            }
        }
        if (e.target.name === "to") {
            if (!validateArrivalId(e.target.value)) {
                arrivalIdErrorMessage.textContent = "Field is empty or invalid!"
            } else {
                arrivalIdErrorMessage.textContent = ""
            }
        }
        if (e.target.name === "departure") {
            if (!validateDepartureDt(e.target.value)) {
                departureDtErrorMessage.textContent = "Field is empty or invalid!"
            } else {
                departureDtErrorMessage.textContent = ""
            }
        }
        if (e.target.name === "arrival") {
            if (!validateArrivalDt(e.target.value)) {
                arrivalDtErrorMessage.textContent = "Field is empty or invalid!"
            } else {
                arrivalDtErrorMessage.textContent = ""
            }
        }
    }

    function validateCode(input) {
        if (input === empty || !onlyLettersRegex.test(input)) {
            return false
        }
        return true;
    }

    function validateDepartureId(input) {
        if (input === empty) {

            return false
        } else {

            return true
        }
    }
    function validateArrivalId(input) {
        if (input === empty) {
            return false
        } else {
            return true
        }
    }
    function validateDepartureDt(input) {
        if (input === empty) {
            return false
        } else {
            return true
        }
    }
    function validateArrivalDt(input) {
        if (input === "") {
            return false
        }
        return true;
    }
});