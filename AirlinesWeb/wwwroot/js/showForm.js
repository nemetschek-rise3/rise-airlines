document.addEventListener("DOMContentLoaded", function () {
    const table = document.getElementById("table-topline");
    const form = document.getElementById("form-container");
    const showButton = document.getElementById("show-button");

    table.style.display = "none";
    form.style.display = "";
    showButton.addEventListener("click", toggle)

    function toggle() {
        if (table.style.display === "") {
            table.style.display = "none";
            form.style.display = "";
            showButton.textContent = "Show Table";
        } else {
            table.style.display = "";
            form.style.display = "none";
            showButton.textContent = "Show Form";
        }
    }
});