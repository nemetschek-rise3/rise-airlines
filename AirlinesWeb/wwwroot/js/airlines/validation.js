document.addEventListener("DOMContentLoaded", function () {

    console.log("hello")

    const name = document.getElementById("name");
    const founded = document.getElementById("founded");
    const fleetSize = document.getElementById("fleetsize");
    const description = document.getElementById("description");
    const submitButton = document.getElementById("submitB")
    
    const nameErrorMessage = document.getElementById("valid-name")
    const fleetSizeErrorMessage = document.getElementById("valid-fleetsize")
    const descriptionErrorMessage = document.getElementById("valid-description")
    const foundedErrorMessage = document.getElementById("valid-founded")


    //submitButton.disabled = true;
    name.addEventListener("blur", validate);
    founded.addEventListener("blur", validate);
    fleetSize.addEventListener("blur", validate);
    description.addEventListener("blur", validate);

    form.querySelectorAll('input').forEach(input => {
        input.addEventListener("input", function () {
            formValidation(form);
        });
    });

    function formValidation(form) {
        submitButton.disabled = false;

        form.querySelectorAll('input').forEach(input => {
            const value = input.value.trim();
            const inputName = input.name;

            if (
                (inputName === "name" && !validateName(value)) ||
                (inputName === "fleetsize" && !validateFleetSize(value)) ||
                (inputName === "founded" && !validateFounded(value)) ||
                (inputName === "description" && !validateDescription(value))
            ) {
                submitButton.disabled = true;
            }
        });
    }


    function validate(e) {

        if (e.target.name === "name") {
            if (!validateName(e.target.value)) {
                nameErrorMessage.textContent = "Field is empty or invalid!"
            } else {
                nameErrorMessage.textContent = ""
            }
        }
        if (e.target.name === "fleetsize") {
            if (!validateFleetSize(e.target.value)) {
                fleetSizeErrorMessage.textContent = "Field is empty or invalid!"
            } else {
                fleetSizeErrorMessage.textContent = ""
            }
        }
        if (e.target.name === "founded") {
            if (!validateFounded(e.target.value)) {
                foundedErrorMessage.textContent = "Field is empty or invalid!"
            } else {
                foundedErrorMessage.textContent = ""
            }
        }
        if (e.target.name === "description") {
            if (!validateDescription(e.target.value)) {
                descriptionErrorMessage.textContent = "Field is empty or invalid!"
            } else {
                descriptionErrorMessage.textContent = ""
            }
        }
    }


    function validateName(nameInput) {
        return nameInput !== ""
    }

    function validateFleetSize(fleetSizeInput) {
        return fleetSizeInput !== ""
    }
    function validateFounded(foundedInput) {
        return foundedInput !== ""
    }
    function validateDescription(descriptionInput) {
        return descriptionInput !== ""
    }
});