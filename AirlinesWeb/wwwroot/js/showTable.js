document.addEventListener("DOMContentLoaded", function () {
    const table = document.getElementById("table");
    const rows = table.querySelectorAll("tbody tr");
    const button = document.getElementById("toggle");

    const rowCount = rows.length;
    const defaultRowCount = 3;

    for (let i = defaultRowCount + 1; i < rowCount; i++) {
        rows[i].style.display = "none";
    }

    if (rowCount > defaultRowCount) {
        button.style.display = "block";
    } else {
        button.style.display = "none";
    }

    button.textContent = "Show More";

    //just swaping the functionalities
    button.addEventListener("click", function () {
        for (let i = defaultRowCount + 1; i < rowCount; i++) {
            if (rows[i].style.display === "none") {
                rows[i].style.display = "table-row";
            } else {
                rows[i].style.display = "none";
            }
        }

        if (button.textContent === "Show More") {
            button.textContent = "Show Less";
        } else {
            button.textContent = "Show More";
        }
    });
});