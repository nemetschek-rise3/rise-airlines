document.addEventListener("DOMContentLoaded", function () {

    const form = document.getElementById("form");
    const name = document.getElementById("name");
    const country = document.getElementById("country");
    const city = document.getElementById("city");
    const code = document.getElementById("code");
    const runwaysCount = document.getElementById("runwaysCount");
    const founded = document.getElementById("founded");
    const submitButton = document.getElementById("submit")

    const nameErrorMessage = document.getElementById("valid-name")
    const countryErrorMessage = document.getElementById("valid-country")
    const cityErrorMessage = document.getElementById("valid-city")
    const codeErrorMessage = document.getElementById("valid-code")
    const runwaysErrorMessage = document.getElementById("valid-runways")
    const foundedErrorMessage = document.getElementById("valid-founded")

    const onlyLettersRegex = /^[a-zA-Z]+$/;
    const empty = "";

    submitButton.disabled = true;
    name.addEventListener("blur", validate);
    country.addEventListener("blur", validate);
    city.addEventListener("blur", validate);
    code.addEventListener("blur", validate);
    runwaysCount.addEventListener("blur", validate);
    founded.addEventListener("blur", validate);


    form.querySelectorAll('input').forEach(input => {
        input.addEventListener("input", function () {
            formValidation(form);
        });
    });

    function formValidation(form) {
        submitButton.disabled = false;

        form.querySelectorAll('input').forEach(input => {
            const value = input.value.trim();
            const inputName = input.name;

            if (
                (inputName === "name" && !validateName(value)) ||
                (inputName === "code" && !validateCode(value)) ||
                (inputName === "country" && !validateCountry(value)) ||
                (inputName === "city" && !validateCity(value)) ||
                (inputName === "runwaysCount" && !validateRunways(value)) ||
                (inputName === "founded" && !validateFounded(value))
            ) {
                submitButton.disabled = true;

            }
        });
    }

    function validate(e) {

        if (e.target.name === "name") {
            if (!validateName(e.target.value)) {
                nameErrorMessage.textContent = "Field is empty or invalid!"
            } else {
                nameErrorMessage.textContent = ""
            }
        }
        if (e.target.name === "city") {
            if (!validateCity(e.target.value)) {
                cityErrorMessage.textContent = "Field is empty or invalid!"
            } else {
                cityErrorMessage.textContent = ""
            }
        }
        if (e.target.name === "country") {
            if (!validateCountry(e.target.value)) {
                countryErrorMessage.textContent = "Field is empty or invalid!"
            } else {
                countryErrorMessage.textContent = ""
            }
        }
        if (e.target.name === "code") {
            if (!validateCode(e.target.value)) {
                codeErrorMessage.textContent = "Field is empty or invalid!"
            } else {
                codeErrorMessage.textContent = ""
            }
        }
        if (e.target.name === "runwaysCount") {
            if (!validateRunways(e.target.value)) {
                runwaysErrorMessage.textContent = "Field is empty or invalid!"
            } else {
                runwaysErrorMessage.textContent = ""
            }
        }
        if (e.target.name === "founded") {
            if (!validateFounded(e.target.value)) {
                foundedErrorMessage.textContent = "Field is empty or invalid!"
            } else {
                foundedErrorMessage.textContent = ""
            }
        }
    }

    function validateName(input) {
        if (input === empty || !onlyLettersRegex.test(input)) {
            return false
        }
        return true;
    }

    function validateCountry(input) {
        if (input === empty || !onlyLettersRegex.test(input)) {

            return false
        } else {

            return true
        }
    }
    function validateCity(input) {
        if (input === empty || !onlyLettersRegex.test(input)) {
            return false
        } else {
            return true
        }
    }
    function validateCode(input) {
        if (input === empty || !onlyLettersRegex.test(input)) {
            return false
        } else {
            return true
        }
    }
    function validateRunways(runwaysCountInput) {
        if (runwaysCountInput === "") {
            return false
        }
        let runwaysCount = parseInt(runwaysCountInput);
        if (runwaysCount < 1 || runwaysCount > 10) {
            return false;
        }
        return true;
    }
    function validateFounded(foundedInput) {
        if (foundedInput === "") {
            return false
        }
        let founded = new Date(foundedInput);
        let now = new Date();
        if (founded >= now) {
            return false;
        }
        return true;
    }
});