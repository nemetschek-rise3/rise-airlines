﻿namespace AirlinesWeb.Models
{
    public class AirportModel
    {
        public AirportModel()
        {
        }
        public AirportModel(int id, string name, string country, string city, string code, int runwaysCount, DateOnly founded)
        {
            FlightDepartureAirports = new List<FlightModel>();
            FlightArrivalAirports = new List<FlightModel>();
            Id = id;
            Name = name;
            Country = country;
            City = city;
            Code = code;
            RunwaysCount = runwaysCount;
            Founded = founded;
        }
        public int? Id { get; set; }
        public string? Name { get; set; }
        public string? Country { get; set; }
        public string? City { get; set; }
        public string? Code { get; set; }
        public int? RunwaysCount { get; set; }
        public DateOnly? Founded { get; set; }

        public virtual ICollection<FlightModel>? FlightArrivalAirports { get; set; }
        public virtual ICollection<FlightModel>? FlightDepartureAirports { get; set; }
    }
}
