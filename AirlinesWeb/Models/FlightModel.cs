﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AirlinesWeb.Models
{
    public class FlightModel
    {
        public FlightModel() { }
        public FlightModel(int id, string code, int departureAirportId, int arrivalAirportId,
            DateTime departureDateTime, DateTime arrivalDateTime, int price, int timeHours)
        {
            DepartureAirport = new AirportModel();
            ArrivalAirport = new AirportModel();
            Id = id;
            Code = code;
            DepartureAirportId = departureAirportId;
            ArrivalAirportId = arrivalAirportId;
            DepartureDateTime = departureDateTime;
            ArrivalDateTime = arrivalDateTime;
            Price = price;
            TimeHours = timeHours;
        }
        public int? Id { get; set; }

        public string?  Code { get; set; }

        public int? DepartureAirportId { get; set; }

        public int? ArrivalAirportId { get; set; }

        public DateTime? DepartureDateTime { get; set; }

        public DateTime? ArrivalDateTime { get; set; }

        public int? Price { get; set; }

        public int? TimeHours { get; set; }

        public virtual AirportModel? ArrivalAirport { get; set; }

        public virtual AirportModel? DepartureAirport { get; set; }
    }
}
