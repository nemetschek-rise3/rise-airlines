﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AirlinesWeb.Models
{
    public class AirlineModel
    {
        public AirlineModel(int id, string name, DateOnly founded, int fleetSize, string description)
        {
            Id = id;
            Name = name;
            Founded = founded;
            FleetSize = fleetSize;
            Description = description;
        }
        public int? Id { get; set; }
        public string? Name { get; set; }
        public DateOnly? Founded { get; set; }
        public int? FleetSize { get; set; }
        public string? Description { get; set; }
    }
}
